
var app = {
    init: function () {
        if (!$('.select2').hasClass("select2-hidden-accessible")) {
            app.customSelect();
        }
       
        app.maximumSelection();
        app.formValidation();
        app.submitForm();
        app.datePicker();

        app.ajaxRoute = $('meta[name="front.ajax.route"]').attr('content');

        app.dateRange();
        app.formWizard();

       //app.fileUploader();
       //app.uppyUploader()

       app.dataChain();
       app.interPhoneCode();

       app.editable();
        
    },

    ajaxRoute : null,

    loader : '<center><div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div></center>',

    customSelect:function (){
        if($(".select2").length){
            $(".select2").select2({
                dropdownAutoWidth: true,
                width: '100%'
            });

            $('.select2-container--focus').hide();
        }
    },
    formValidation : function(){
        jQuery.validator.addMethod("uploaded", function(value, element) {
            if(!value){
                return false;
            }
            var files = JSON.parse(value);

            if((files.added).length <= (files.removed).length)
            {
                return false;
            }
            return true;
          }, "Please, uploade your Emirates ID");
          
    },
    maximumSelection : function(){
        $(".max-length").each(function(){
            var max = $(this).data('max');
            $(this).select2({
                dropdownAutoWidth: true,
                width: '100%',
                maximumSelectionLength: max,
                placeholder: "Select maximum "+ max +" items"
            });
        })
    },
    dataChain: function(){
        $('.data-chain').each(function(){
            var chain = $(this).data('chain');
            var main = $(this).attr('id');
            $('#'+chain).chained('#' + main);
        });
    },
    interPhoneCode: function (){
        $('.inter-phone-code').each(function(){
            $(this).intercode({
                country: 'UAE',
                default: $(this).val(),
                selectClass: 'countryCodeselect left',
                intercodeFile: '/front/countrycodes.json'
            });
        })
    },

    editable : function(){
        
        $.fn.editable.defaults.ajaxOptions = {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-front"]').attr('content')
            },
        };

        $('.editable').editable({
            placement: 'right',
           /*  toggle: 'dblclick', */
            pk: 1,
            params: function(params) {
                return params;
            }

        });
    },
    dateRange: function(){
        if($('input.date-range').length){
            var start = moment();
            var end = moment().add(7 ,'days');

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            $('input.date-range').daterangepicker({
                startDate: start,
                endDate: end,
                locale : 'MMMM D, YYYY',
                opens: 'center',
                locale :{
                    format: "YYYY-MM-DD",
                },
                ranges: {
                   'Today': [moment(), moment()],
                   'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],

                   'This Week': [moment().startOf('week'), moment().endOf('week')],
                   
                   'Next Week': [moment().add(6, 'days').startOf('week'), moment().add(6, 'days').endOf('week')],
                   
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);

        }
    },
    datePicker: function(){
        $("input.date-picker").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1920,
            maxYear: parseInt(moment().add(1 , 'year').format('YYYY'),10),
            locale :{
                format: "YYYY-MM-DD",
            }
        }).on('apply.daterangepicker', function(ev, picker) {

        });
        
    },
    /** Delete Entity from DB */
    deleteItem : function(el){
        var route = $(el).data('route');

        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
            confirmButtonClass: "btn btn-outline-success",
            cancelButtonClass: "btn btn-outline-danger ml-1",
            buttonsStyling: !1
        }).then(function(t) {
            if(t.value){
                app.appAjax('DELETE' , {} , route , 'object')
                .then(function(response){
                    if(response.status == 'ok'){                        
                        toastr.success(response.message , "Deleted Successfully!" );
                        $('#' + response.deleted ).remove();
                    }
                })
                .fail(function(jqXHR, textStatus, error){
                    toastr.error('Please try again later!','Something went wrong');
                });
            }
        })
        
    },

    startPorfileUpload: function(){
        $('#account-upload').click();
    },

    fileSelected(autoUpload){
        var reader,
        files = document.getElementById("account-upload").files;

        var type = (files[0].type).split("/");
        if(type[0] != 'image'){
            toastr.error('Please select an image','Incorrect file type');
            return false;
        }
        
        reader = new FileReader();
        reader.onload = e => {
            $('#profile-thumb').attr('src' , e.target.result);
        };
        reader.readAsDataURL(files[0]);
        if(autoUpload){
            $('.loader').html(app.loader);
            var form = $('#update_profile_image');

            var data = new FormData($("#update_profile_image")[0])
            data.append('action' , 'member.updateProfileImage');

            app.appAjax('POST' , data , app.ajaxRoute , 'file' )
            .then(function(resposne){
                $('.loader').html("");
                if(resposne.status == 'ok'){
                    toastr.success(resposne.message , "Updated Successfully!" )
                }
            })
            .fail(function(jqXHR, textStatus, error){
                $('.loader').html("");
                var message = 'Please try again later!';
                if(typeof jqXHR.responseJSON.errors.profile_image[0] !== 'undefined'){
                    message = jqXHR.responseJSON.errors.profile_image[0];
                }
                toastr.error(message,'Something went wrong');
            });
        }
    },
    uppyUploader: function(){

        var message = $('#drag-drop-area').data('message');
        
        var filesList = {
            added : [],
            removed : [],
        };
        
        var uppy = Uppy.Core({
            autoProceed: true,
            allowMultipleUploads: true,

            restrictions: {
                maxNumberOfFiles: 2,
                minNumberOfFiles: 1,
                allowedFileTypes: ['image/*', '.pdf']
            },
        })
   
        .use(Uppy.Dashboard, {
          inline: true,
          target: '#drag-drop-area',
          width:800,
          height:300,
          showRemoveButtonAfterComplete: true,
          thumbnailWidth:280,
          proudlyDisplayPoweredByUppy: false,
          locale: {
                strings: {
                    dropPasteImport: message,
                    dropHint: message,
                }
            },
            metaFields: [
                { id: 'name', name: 'Name', placeholder: 'file name' },
                { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
            ],
            note: 'Images and video only, 2–3 files, up to 1 MB',
        })
        .use(Uppy.Webcam , {
            onBeforeSnapshot: () => Promise.resolve(),
            countdown: false,
            target: Uppy.Dashboard,
            modes: [
              'video-audio',
              'video-only',
              'audio-only',
              'picture'
            ],
            mirror: false,
            facingMode: 'user',
            showRecordingLength: false,
            preferredVideoMimeType: null,
            preferredImageMimeType: null,
            
          })
        .use(Uppy.XHRUpload, {
                endpoint: '/upload/upload',
                formData: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token-front"]').attr('content')
                },
                formData: true,
                fieldName: 'files[]',
            })
        .use(Uppy.Form, {
                target: "#createMember",
                getMetaFromForm: false,
                addResultToForm: true,
        });

        uppy.on('upload', (data) => {
            $('a[href="#next"]').attr('disabled' , 'disabled');
            $('a[href="#next"]').addClass('disabled_btn');
        })
        
        uppy.on('complete', (result) => {
            
            $('a[href="#next"]').removeClass('disabled_btn');
            $('a[href="#next"]').removeAttr('disabled');

            result.successful.forEach(function(file){
                filesList.added.push({
                    originalName: file.data.name,
                    path: file.response.body[0],
                });
            })

            app.saveFilesList(filesList);
        })

        uppy.on('upload-success', (file, response) => {
            console.log("uploaded")
            //console.log(response.status); // HTTP status code
            //console.log(response.body); // HTTP status code
        })
        uppy.on('file-removed', (file) => {
            filesList.removed.push({
                originalName: file.data.name,
                path: file.response.body[0],
            });
            app.saveFilesList(filesList);
        })
    },
    saveFilesList: function(filesList){
          $('#Files_List').val( JSON.stringify(filesList) );  
    },
    fileUploader: function(){
  
        // Turn input element into a pond with configuration options
        $('input.filepond').each(function(){
            var uploadeURL = $(this).data('upload-url');
            var container = $(this).data('container-id');
            $(this).filepond({
                allowMultiple: false,
                server :{
                    url:'/upload',
                    /* process:function (fieldName, file,metadata , load, error, progress, abort, transfer, options){

                        console.log(fieldName);
                        console.log(file);
                        console.log(metadata);
                        console.log(error);
                        console.log(progress);
                        console.log(abort);
                        console.log(transfer);
                        console.log(options);

                    }, */
                    process:{
                        url: '/upload',
                        onload : function(file){
                            $('#'+ container).val(file);
                        },
                    },
                    /* revert: {
                        url: '/destroy',
                        ondata: function(formData){
                            console.log(formData);
                            return formData;
                        }
                    }, */
                    revert: function(uniqueFileId, load, error) {
                        console.log("uniqueFileId");
                        console.log(uniqueFileId);
                        // Should remove the earlier created temp file here
                        // ...
            
                        // Can call the error method if something is wrong, should exit after
                        error('oh my goodness');
            
                        // Should call the load method when done, no parameters required
                        load();
                    },
                    remove: function(source, load, error) {
                        console.log("source");
                        console.log(source);
                        // Should somehow send `source` to server so server can remove the file with this source
            
                        // Can call the error method if something is wrong, should exit after
                        error('oh my goodness');
            
                        // Should call the load method when done, no parameters required
                        load();
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token-front"]').attr('content')
                    },
                    onerror: null,
                    ondata: null
                } ,
               
            });
        })



       
        
    },
    formWizard : function(){

        if($(".steps-validation").length){
        // Show form
        var form = $(".steps-validation").show();

        $(".steps-validation").steps({
            headerTag: "h6",
            bodyTag: "fieldset",
            transitionEffect: "fade",
            titleTemplate: '<span class="step">#index#</span> #title#',
            labels: {
                finish: 'Add Member'
            },
            onInit :function (event, currentIndex) { 
                app.customSelect();
                /* if (!$('.select2').hasClass("select2-hidden-accessible")) {
                } */
                //app.fileUploader();
                app.datePicker();
                app.uppyUploader();
            },
            onStepChanging: function (event, currentIndex, newIndex) {
               
                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {
                    // To remove error styles
                    form.find(".body:eq(" + newIndex + ") label.error").remove();
                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                }
                form.validate().settings.ignore = ":disabled,:hidden";
                if(form.valid()){
                   // app.syncData(event);
                }
                
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                //submit Application
                var form = event.target;
                form.submit();
            }
        });
        // Initialize validation
        $(".steps-validation").validate(app.validation());
        } 
    },
    
    submitForm : function(form){
        if($('.form-validate').length){
            $('.form-validate').validate(app.validation()).settings.ignore = ":disabled,:hidden";
        }
    },

    validation :function(){
        return {
            ignore: 'input[type=hidden]', // ignore hidden fields
            errorClass: 'danger',
            successClass: 'success',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                phone:{
                    minlength: 10,
                   /*  remote: app.remoteValidation('member.uniquePhone','family_phone') */
                },
                id_number_1:{
                    minlength: 14,
                    maxlength: 14,
                    remote: app.remoteValidation('member.isUniqueGlobally' ,'id_number_1')
                },
                id_number_2: {
                    minlength: 15,
                    maxlength: 15,
                    remote: app.remoteValidation('member.isUniqueGlobally' ,'id_number_2')
                },
                files_list:{
                    uploaded:true
                }
            },
            messages: {
                phone: {
                    minlength: jQuery.validator.format("Please, enter a valid Phone Number"),
                    remote:jQuery.validator.format("This phone number is already used for another family")
                },
                id_number_1: {
                    minlength: jQuery.validator.format("Please, enter a valid Egyptian National ID"),
                    maxlength: jQuery.validator.format("Please, enter a valid Egyptian National ID"),
                    remote: jQuery.validator.format("This Number is already registered"),
                },
                id_number_2: {
                    minlength: jQuery.validator.format("Please, enter a valid Emirates ID"),
                    maxlength: jQuery.validator.format("Please, enter a valid Emirates ID"),
                    remote: jQuery.validator.format("This Number is already registered"),
                }
              }
        }
    },

    remoteValidation: function(action, attr){
        return {
            url: app.ajaxRoute,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token-front"]').attr('content')
            },
            data: {
                action: action,
                attr: attr,
                value: function() {
                    return $('[name="'+attr+'"]').val();
                }
            }
        }
    },
    syncData : function(event){
        var form = event.target;
        var route = $(form).attr('action');
        formData = new FormData($(form)[0]);
        app.appAjax('POST', formData, route, 'file');
    },
    toggleElement : function(el){
        var val         = $(el).val();
        var conditions  = $(el).data('conditions');
        conditions = conditions.split(",");
        
        conditions.forEach(function(condition){
            var condition   = condition.split("|");
            var ref = condition[2].replace(/_/g , " ");
            switch (condition[1]) {
                case "eq":
                    app.toggleElementIfEqual(val , condition[0] , ref)
                    break;
            }
        })

    },

    toggleElementIfEqual: function(val , show , condition){
        var elements = show.split("+");
        if(val == condition){
            elements.forEach(function(element){
                $("#" + element).removeClass('hidden');
                $("#" + element).find('input').removeAttr('disabled');
                $("#" + element).find('select').removeAttr('disabled');
            })
        }else{
            elements.forEach(function(element){
                $("#" + element).addClass('hidden');
                $("#" + element).find('input').attr('disabled', 'disabled');
                $("#" + element).find('select').attr('disabled', 'disabled');
            })
        }      
    },

    syncElement: function(el){
        var sync = $(el).data('sync');
        var value = $(el).val();
        sync = sync.split("|");
        var data = {
            action : sync[1],
            value: value
        }

        this.appAjax('POST' , data , app.ajaxRoute , 'object' )
        .then(function(resposne){
            console.log(resposne);
        });
        
    },

    filterLiturgies : function(el , template){
        var filters = [];

        $('[data-filter]').each(function(){
            filters.push({
                filter : $(this).data('filter'),
                value : $(this).val(),
            });
        })
        var data = {
            action : 'liturgies.index',
            filters: filters,
            template: template
        }
        $('#liturgies-list').html(app.loader);
        app.appAjax('POST' , data , app.ajaxRoute , 'object')
        .then(function(resposne){
            if(resposne.status == 'ok'){
                $('#liturgies-list').html(resposne.html);
            }
        });


    },
    appAjax : function(method , data , route , dataType ){
        // Default DataType is Data
        dataType = dataType || 'data';
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token-front"]').attr('content')
                }
        });
    
        var args = {
              url           : route,
              method        : method,
              data          : data,
              dataType      : "json",
           };
    
    
        // For Ajax File Upload
        if(dataType == 'file'){
           args.processData = false; 
           args.contentType = false; 
        }
        return $.ajax(args);
    }

}


$(document).ready(function () {
    app.init();
});
