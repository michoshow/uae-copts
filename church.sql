-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2020 at 06:51 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `church`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `active`, `role`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'Michel Melad', 'michelmelad@hotmail.com', '2020-05-07 04:00:00', '$2y$10$LpTEVvxMOB/yQlU1HHMkze4DPpc/x5MEjm4Dx8oHiFQn8H85a.l72', NULL, '2020-05-07 04:00:00', '2020-05-07 04:00:00'),
(8, 1, 'admin', 'Michel Attallah', 'mattalah@higher-education-marketing.com', NULL, '$2y$10$87Utvn.8ox3O/ZzplQYVEuPejDsnXVFB5jPvHTSAqWn2Gno1Rs222', NULL, '2020-05-08 09:04:55', '2020-05-11 06:57:13'),
(9, 0, 'admin', 'Michel Melad', 'michelmald@hotmail.com', NULL, '$2y$10$3A3mEoKlZGg5P12GCMWboe8SUtERta7ZaxtkHU3PAhBl.b6lXOVU2', NULL, '2020-05-22 07:03:59', '2020-05-22 07:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `admins_password_resets`
--

CREATE TABLE `admins_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `liturgy_id` bigint(20) UNSIGNED NOT NULL,
  `church_id` bigint(20) UNSIGNED NOT NULL,
  `family_id` bigint(20) UNSIGNED NOT NULL,
  `member_id` bigint(20) UNSIGNED NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `attended` tinyint(1) NOT NULL DEFAULT '0',
  `canceled` tinyint(1) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `token`, `liturgy_id`, `church_id`, `family_id`, `member_id`, `confirmed`, `attended`, `canceled`, `date`, `start`, `end`, `created_at`, `updated_at`) VALUES
(289, '6ec448badb49893c3a389f0ef8350e19', 2, 4, 17, 25, 0, 0, 0, '2020-06-06', '08:00:00', '11:00:00', '2020-06-04 07:47:37', '2020-06-04 07:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `churches`
--

CREATE TABLE `churches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emirate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UAE',
  `contacts` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `churches`
--

INSERT INTO `churches` (`id`, `name`, `address`, `area`, `city`, `emirate`, `country`, `contacts`) VALUES
(1, 'St. Antony Coptic Orthodox Church', '17th St - Al Mushrif W24-02', 'Abu Dhabi', 'Abu Dhabi', 'Abu Dhabi', 'UAE', '{\r\n    \"communication\": {\r\n        \"website\": \"http:\\/\\/www.website.com\",\r\n        \"facebook\": \"http:\\/\\/www.facebook.com\\/page\",\r\n        \"email\": \"email@church.com\"\r\n    },\r\n    \"support\": {\r\n        \"1\": {\r\n            \"name\": \"Michel Melad\",\r\n            \"phone\": \"+51425093333\",\r\n            \"email\": \"michelmelad@email.com\"\r\n        }\r\n    }\r\n}'),
(2, 'St. Mary & St. Shenoudah Coptic Orthodox Church', 'Companies Campus,Al Ain - Abu Dhabi - United Arab Emirates', 'Al Ain', 'Al Ain', 'Abu Dhabi', 'UAE', '{\r\n    \"communication\": {\r\n        \"website\": \"http:\\/\\/www.website.com\",\r\n        \"facebook\": \"http:\\/\\/www.facebook.com\\/page\",\r\n        \"email\": \"email@church.com\"\r\n    },\r\n    \"support\": {\r\n        \"1\": {\r\n            \"name\": \"Michel Melad\",\r\n            \"phone\": \"+51425093333\",\r\n            \"email\": \"michelmelad@email.com\"\r\n        }\r\n    }\r\n}'),
(3, 'St. Mina Coptic Orthodox Church', ' A Road - Dubai - United Arab Emirates', 'Jebel Ali', 'Jebel Ali', 'Dubai', 'UAE', '{\r\n    \"communication\": {\r\n        \"website\": \"http:\\/\\/www.website.com\",\r\n        \"facebook\": \"http:\\/\\/www.facebook.com\\/page\",\r\n        \"email\": \"email@church.com\"\r\n    },\r\n    \"support\": {\r\n        \"1\": {\r\n            \"name\": \"Michel Melad\",\r\n            \"phone\": \"+51425093333\",\r\n            \"email\": \"michelmelad@email.com\"\r\n        }\r\n    }\r\n}'),
(4, 'St. Mark & St. Bishoy Coptic Orthodox Church', 'Inside Holy Trinity Church - Oud Metha Rd - Dubai - United Arab Emirates', 'Dubai', 'Dubai', 'Dubai', 'UAE', '{\r\n    \"communication\": {\r\n        \"website\": \"http:\\/\\/www.website.com\",\r\n        \"facebook\": \"http:\\/\\/www.facebook.com\\/page\",\r\n        \"email\": \"email@church.com\"\r\n    },\r\n    \"support\": {\r\n        \"1\": {\r\n            \"name\": \"Michel Melad\",\r\n            \"phone\": \"+51425093333\",\r\n            \"email\": \"michelmelad@email.com\"\r\n        }\r\n    }\r\n}'),
(5, 'St. Mary & St. Abou Sefein Coptic Orthodox Church', 'Al Yarmook, Halwan Suburb - Sharjah - United Arab Emirates', 'Sharjah', 'Sharjah', 'Sharjah', 'UAE', '{\r\n    \"communication\": {\r\n        \"website\": \"http:\\/\\/www.website.com\",\r\n        \"facebook\": \"http:\\/\\/www.facebook.com\\/page\",\r\n        \"email\": \"email@church.com\"\r\n    },\r\n    \"support\": {\r\n        \"1\": {\r\n            \"name\": \"Michel Melad\",\r\n            \"phone\": \"+51425093333\",\r\n            \"email\": \"michelmelad@email.com\"\r\n        }\r\n    }\r\n}'),
(6, 'St. George and St. Anthony Coptic Orthodox Church', 'Al Faseel, Fujairah, United Arab Emirates', 'Fujairah', 'Fujairah', 'Fujairah', 'UAE', '{\r\n    \"communication\": {\r\n        \"website\": \"http:\\/\\/www.website.com\",\r\n        \"facebook\": \"http:\\/\\/www.facebook.com\\/page\",\r\n        \"email\": \"email@church.com\"\r\n    },\r\n    \"support\": {\r\n        \"1\": {\r\n            \"name\": \"Michel Melad\",\r\n            \"phone\": \"+51425093333\",\r\n            \"email\": \"michelmelad@email.com\"\r\n        }\r\n    }\r\n}'),
(7, 'St. Mary and Archangel Michael Coptic Orthodox Church', 'Line 9 - Ras al Khaimah - United Arab Emirates', 'Ras al Khaimah', 'Ras al Khaimah', 'Ras al Khaimah', 'UAE', '{\r\n    \"communication\": {\r\n        \"website\": \"http:\\/\\/www.website.com\",\r\n        \"facebook\": \"http:\\/\\/www.facebook.com\\/page\",\r\n        \"email\": \"email@church.com\"\r\n    },\r\n    \"support\": {\r\n        \"1\": {\r\n            \"name\": \"Michel Melad\",\r\n            \"phone\": \"+51425093333\",\r\n            \"email\": \"michelmelad@email.com\"\r\n        }\r\n    }\r\n}');

-- --------------------------------------------------------

--
-- Table structure for table `families`
--

CREATE TABLE `families` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` bigint(20) UNSIGNED NOT NULL,
  `church_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emirate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UAE',
  `landmarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergerncy_contacts_in_country` longtext COLLATE utf8mb4_unicode_ci,
  `emergerncy_contacts_out_country` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Draft',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `families`
--

INSERT INTO `families` (`id`, `uid`, `church_id`, `user_id`, `email`, `phone`, `address`, `area`, `city`, `emirate`, `country`, `landmarks`, `lat`, `long`, `emergerncy_contacts_in_country`, `emergerncy_contacts_out_country`, `status`, `created_at`, `updated_at`) VALUES
(7, 55423, 1, 2, NULL, NULL, 'شارع المينا', 'النادي السياحي', 'Abu Dhabi', 'Dubai', 'UAE', '[\"\\u0627\\u0644\\u0646\\u0627\\u062f\\u064a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\"]', NULL, NULL, NULL, NULL, 'Draft', '2020-05-22 21:25:11', '2020-05-22 21:25:11'),
(8, 98431, 1, 3, NULL, NULL, 'شارع المينا', 'النادي السياحي', 'Abu Dhabi', 'Abu Dhabi', 'UAE', '[\"\\u0627\\u0644\\u0646\\u0627\\u062f\\u064a \\u0627\\u0644\\u0633\\u064a\\u0627\\u062d\\u064a\"]', NULL, NULL, NULL, NULL, 'Draft', '2020-05-27 20:54:12', '2020-05-27 20:54:12'),
(9, 45802, 1, 4, NULL, NULL, 'شارع المينا', 'النادي السياحي', 'Abu Dhabi', 'Dubai', 'UAE', '[\"\"]', NULL, NULL, NULL, NULL, 'Draft', '2020-05-27 23:30:45', '2020-05-27 23:30:45'),
(10, 43327, 1, 5, NULL, NULL, 'my street', 'النادي السياحي', 'Abu Dhabi', 'Abu Dhabi', 'UAE', '[\"\"]', NULL, NULL, NULL, NULL, 'Draft', '2020-05-29 06:29:33', '2020-05-29 06:29:33'),
(11, 49823, 1, 5, NULL, NULL, 'my street', 'النادي السياحي', 'Abu Dhabi', 'Abu Dhabi', 'UAE', '[\"\"]', NULL, NULL, NULL, NULL, 'Draft', '2020-05-29 06:30:00', '2020-05-29 06:30:00'),
(12, 98384, 1, 7, NULL, '1234567891', 'شارع المينا', 'غليلة', NULL, 'Ras Al Khaimah', 'UAE', NULL, NULL, NULL, NULL, NULL, 'Draft', '2020-05-31 21:35:45', '2020-05-31 21:35:45'),
(13, 74877, 1, 9, NULL, NULL, 'شارع المينا', 'Al Karamah', NULL, 'Abu Dhabi', 'UAE', NULL, NULL, NULL, NULL, NULL, 'Draft', '2020-06-01 00:06:19', '2020-06-01 00:06:19'),
(14, 75847, 1, 10, 'family@michel.ca', '5142223355', 'شارع المينا', 'Al Karamah', NULL, 'Abu Dhabi', 'UAE', NULL, NULL, NULL, NULL, NULL, 'Draft', '2020-06-01 00:08:03', '2020-06-01 00:08:03'),
(15, 24904, 1, 11, 'family58@michel.ca', '+15145669079', 'شارع المينا', 'Al Karamah', NULL, 'Abu Dhabi', 'UAE', NULL, NULL, NULL, NULL, NULL, 'Draft', '2020-06-01 00:10:42', '2020-06-01 00:10:42'),
(16, 35135, 1, 14, 'michel@test.ca', '51423982225', 'streetaddress', 'Al Karamah', NULL, 'Abu Dhabi', 'UAE', NULL, NULL, NULL, NULL, NULL, 'Draft', '2020-06-03 06:08:09', '2020-06-03 06:08:09'),
(17, 85606, 1, 15, 'test@email333.com', '51423332225', 'streetaddress', 'Al Karamah', NULL, 'Abu Dhabi', 'UAE', NULL, NULL, NULL, NULL, NULL, 'Draft', '2020-06-03 15:18:30', '2020-06-03 15:18:30');

-- --------------------------------------------------------

--
-- Table structure for table `liturgies`
--

CREATE TABLE `liturgies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `church_id` bigint(20) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `seats` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `liturgies`
--

INSERT INTO `liturgies` (`id`, `church_id`, `active`, `title`, `description`, `date`, `start`, `end`, `seats`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Holy Liturgy', NULL, '2020-06-01', '07:00:00', '10:00:00', 500, NULL, NULL),
(2, 4, 1, 'Holy Liturgy', NULL, '2020-06-06', '08:00:00', '11:00:00', 2, NULL, NULL),
(3, 5, 1, 'Holy Liturgy', NULL, '2020-06-27', '08:00:00', '11:00:00', 0, NULL, NULL),
(4, 7, 1, 'Holy Liturgy', NULL, '2020-06-16', '10:00:00', '12:00:00', 500, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` bigint(20) UNSIGNED NOT NULL,
  `family_uid` bigint(20) UNSIGNED NOT NULL,
  `church_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number_1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number_3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` text COLLATE utf8mb4_unicode_ci,
  `phone2` text COLLATE utf8mb4_unicode_ci,
  `whatsapp_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp_group_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `family_role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marriage_date` datetime DEFAULT NULL,
  `uae_status` longtext COLLATE utf8mb4_unicode_ci,
  `egypt_status` longtext COLLATE utf8mb4_unicode_ci,
  `deacon` tinyint(1) DEFAULT NULL,
  `deacon_degree` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deacon_in_aue` tinyint(1) DEFAULT NULL,
  `servant_in` text COLLATE utf8mb4_unicode_ci,
  `served_by` text COLLATE utf8mb4_unicode_ci,
  `father_of_confession` text COLLATE utf8mb4_unicode_ci,
  `regsiterd_in_youth_meeting` tinyint(1) DEFAULT NULL,
  `sunday_school_details` longtext COLLATE utf8mb4_unicode_ci,
  `school` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specialization` text COLLATE utf8mb4_unicode_ci,
  `education_details` text COLLATE utf8mb4_unicode_ci,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `talents_hobbies` text COLLATE utf8mb4_unicode_ci,
  `previous_services` text COLLATE utf8mb4_unicode_ci,
  `services_want_to_join` text COLLATE utf8mb4_unicode_ci,
  `phone_refers` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edit_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `uid`, `family_uid`, `church_id`, `first_name`, `middle_name`, `last_name`, `additional_name`, `full_name`, `date_of_birth`, `gender`, `email`, `nationality`, `id_number_1`, `id_number_2`, `id_number_3`, `phone1`, `phone2`, `whatsapp_number`, `whatsapp_group_number`, `marital_status`, `family_role`, `marriage_date`, `uae_status`, `egypt_status`, `deacon`, `deacon_degree`, `deacon_in_aue`, `servant_in`, `served_by`, `father_of_confession`, `regsiterd_in_youth_meeting`, `sunday_school_details`, `school`, `qualification`, `qualification_description`, `specialization`, `education_details`, `company`, `domain`, `occupation`, `job_title`, `talents_hobbies`, `previous_services`, `services_want_to_join`, `phone_refers`, `status`, `edit_status`, `properties`, `created_at`, `updated_at`) VALUES
(5, 37115, 55423, 1, 'Michel', 'Melad', 'Attallah', '2020-05-09', 'Michel Melad Gouda Attallah', '2020-05-05', 'Male', NULL, NULL, '123456789123456', '123456789123456', '123456789123456', '{\"number\":null}', '{\"number\":null}', NULL, NULL, 'false', 'Father', NULL, '{\"first_entery_date\":null,\"status_in_uae\":\"false\",\"uae_id_number\":null,\"uae_id_expiry_date\":null}', '{\"egypt_id_number\":null,\"passpot_number\":null,\"another_id_number\":null,\"country_name\":null,\"governorate\":null,\"city\":null,\"church_name\":null,\"ref_priest\":null,\"priest_number\":null}', NULL, 'false', NULL, '\"false\"', '\"false\"', NULL, NULL, '{\"current_class\":null,\"school_name\":null,\"sunday_school_class_name\":null,\"bible_study_class_name\":null}', NULL, NULL, NULL, NULL, NULL, NULL, 'false', NULL, NULL, '[null]', '[null]', '[null]', NULL, NULL, NULL, NULL, '2020-05-13 09:13:51', '2020-05-13 09:13:51'),
(8, 22644, 98431, 1, 'Michel', 'Melad', 'Gouda', 'Attallah', NULL, '2020-05-27', 'Male', NULL, NULL, '12345678912345', '12345678912345', NULL, '\"123456788\"', '\"45465465465\"', NULL, NULL, 'Married', 'Husband', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, '\"sdsadas\"', NULL, NULL, NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'dsds Employer | جهة العمل', 'Medical | طب', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-27 22:44:33', '2020-05-27 22:44:33'),
(9, 34128, 43327, 1, 'Michel', 'Melad', 'Gouda', 'Attallah', NULL, '1989-05-03', 'Male', NULL, NULL, '12345698756555', '12345678912347', NULL, '\"123456789\"', '\"1324689789\"', NULL, NULL, 'Married', 'Husband', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, '\"sdsadas\"', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-29 06:44:17', '2020-05-29 06:44:17'),
(12, 96142, 43327, 1, 'Anna Maria', 'Melad', 'test', 'Attallah', NULL, '2020-05-28', 'Male', NULL, NULL, '12345678912315', '454545454544545', NULL, '\"123456789\"', NULL, NULL, NULL, 'Single', 'Wife', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, NULL, NULL, '44444', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Medical | طب', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-29 07:10:59', '2020-05-29 07:10:59'),
(14, 25543, 43327, 2, 'Anna Maria', 'Melad', 'test', 'Attallah', NULL, '2020-05-02', 'Male', NULL, NULL, '12345678912344', '123456789123455', NULL, NULL, NULL, NULL, NULL, NULL, 'Child', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"class_name\":\"St Mina\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-29 07:22:09', '2020-05-29 07:22:09'),
(15, 98890, 98431, 1, 'Foregn', 'Foregn', 'Foregn', 'Foregn', NULL, '2020-05-06', 'Male', NULL, NULL, NULL, '565487987887896', 'A3346565465', '\"123456788\"', '\"45465465465\"', NULL, NULL, 'Single', 'Father', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, '\"sdsadas\"', NULL, '44444', NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'dsds Employer | جهة العمل', 'Medical | طب', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-31 20:08:08', '2020-05-31 20:08:08'),
(16, 17470, 98384, 2, 'Michel', 'Melad', 'Attallah', 'Attallah', NULL, '2020-05-31', 'Male', NULL, NULL, '12345678912385', '123456789123365', NULL, '\"123456789\"', '\"45465465465\"', NULL, NULL, 'Single', 'Husband', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, '\"If serving in Church | \\u0625\\u0646 \\u0643\\u0646\\u062a \\u062e\\u0627\\u062f\\u0645 \\u0628\\u0627\\u0644\\u0643\\u0646\\u064a\\u0633\\u0629\"', NULL, '1', NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'Employer | جهة العمل', 'Medical | طب', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-31 21:38:11', '2020-05-31 21:38:11'),
(17, 72571, 98384, 1, 'Anna Maria', 'Melad', 'test', 'Attallah', NULL, '2020-05-31', 'Male', NULL, NULL, '12345678912378', '123456789126575', NULL, NULL, NULL, NULL, NULL, NULL, 'Child', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"class_name\":\"St Mina\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-31 21:39:52', '2020-05-31 21:39:52'),
(18, 19894, 98384, 2, 'مشيل', 'ميلاد', 'حخحجخحج', 'غكملببهب', NULL, '2020-05-31', 'Male', NULL, NULL, '12345678912346', '123456789123447', NULL, '\"123456788\"', '\"1324689789\"', NULL, NULL, 'Married', 'Wife', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, '\"If serving in Church | \\u0625\\u0646 \\u0643\\u0646\\u062a \\u062e\\u0627\\u062f\\u0645 \\u0628\\u0627\\u0644\\u0643\\u0646\\u064a\\u0633\\u0629\"', NULL, '1', NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'dsds Employer | جهة العمل', 'Medical | طب', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-31 21:51:03', '2020-05-31 21:51:03'),
(19, 11292, 98431, 1, 'Anna Maria', 'Melad', 'test', 'Attallah', NULL, '2020-01-30', 'Male', NULL, NULL, '12345678912339', NULL, NULL, '\"123456788\"', '\"45465465465\"', NULL, NULL, 'Single', 'Mother', NULL, '{\"visat_type\":\"Visit\"}', NULL, NULL, NULL, NULL, '\"If serving in Church | \\u0625\\u0646 \\u0643\\u0646\\u062a \\u062e\\u0627\\u062f\\u0645 \\u0628\\u0627\\u0644\\u0643\\u0646\\u064a\\u0633\\u0629\"', NULL, '1', NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'dsds Employer | جهة العمل', 'Engineering | هندسة', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-31 22:54:29', '2020-05-31 22:54:29'),
(20, 63015, 24904, 1, 'Anna Maria', 'Melad', 'test', 'Attallah', NULL, '2020-05-15', 'Male', NULL, NULL, '12345678912347', '123456789123348', NULL, '\"123456788\"', '\"45465465465\"', NULL, NULL, 'Single', 'Husband', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, '\"If serving in Church | \\u0625\\u0646 \\u0643\\u0646\\u062a \\u062e\\u0627\\u062f\\u0645 \\u0628\\u0627\\u0644\\u0643\\u0646\\u064a\\u0633\\u0629\"', NULL, '1', NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'dsds Employer | جهة العمل', 'Medical | طب', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-01 00:12:42', '2020-06-01 00:12:42'),
(21, 79903, 35135, 1, 'test', 'test', 'tes', NULL, NULL, '2020-06-02', 'Male', NULL, NULL, '41236598756214', NULL, NULL, '\"51422233333\"', NULL, NULL, NULL, 'Married', 'Husband', NULL, '{\"visat_type\":\"Visit\"}', NULL, NULL, NULL, NULL, '\"test\"', NULL, '1', NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'Employer | جهة العمل', 'Medical | طب', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 06:09:11', '2020-06-03 06:09:11'),
(22, 70637, 35135, 2, 'Child', 'Child', 'Child', NULL, NULL, '2020-06-19', 'Female', NULL, NULL, NULL, NULL, '12354478898', NULL, NULL, NULL, NULL, NULL, 'Child', NULL, '{\"visat_type\":\"Visit\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{\"class_name\":\"test\"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 06:20:28', '2020-06-03 06:20:28'),
(25, 31612, 85606, 1, 'Michel', 'Melad', 'Gouda', NULL, NULL, '2020-06-05', 'Male', NULL, NULL, '41236598756217', '123456789123453', NULL, '\"51422233333\"', NULL, NULL, NULL, 'Single', 'Brother', NULL, '{\"visat_type\":\"Residence\"}', NULL, NULL, NULL, NULL, '\"test\"', NULL, '1', NULL, NULL, NULL, 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية', NULL, NULL, NULL, 'Employer | جهة العمل', 'Medical | طب', NULL, 'Job | الوظيفة', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 05:51:35', '2020-06-04 05:51:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2020_02_22_074913_create_permission_tables', 1),
(9, '2020_05_08_030332_create_admins_table', 1),
(10, '2020_05_08_030557_create_admins_password_resets', 1),
(37, '2020_05_07_031202_create_churches_table', 2),
(38, '2020_05_08_031015_create_families_table', 2),
(39, '2020_05_09_070711_create_members_table', 2),
(40, '2020_05_22_190345_create_liturgies_table', 3),
(41, '2020_06_01_165321_create_bookings_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\Admin', 1),
(1, 'App\\Models\\Admin', 8),
(2, 'App\\Models\\Admin', 8),
(3, 'App\\Models\\Admin', 8);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'APPROVE_ADMINS', 'admin', '2020-05-08 04:00:00', '2020-05-08 04:00:00'),
(2, 'ADD_FAMILY', 'admin', '2020-05-08 04:00:00', '2020-05-08 04:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'admin', '2020-05-08 04:00:00', '2020-05-08 04:00:00'),
(2, 'admin', 'admin', '2020-05-08 04:00:00', '2020-05-08 04:00:00'),
(3, 'church-admin', 'admin', '2020-05-08 04:00:00', '2020-05-08 04:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Michel Attallah', 'michelmelad@hotmail.com', NULL, '$2y$10$aRIrnpKIoTBN4.e2ZT8vqOLHaMmhxO9yBfbrbAgHH7oj0XD5ELZMG', NULL, '2020-05-22 21:24:01', '2020-05-22 21:24:01'),
(3, 'Michel Melad', 'michelmelad@gmail.com', NULL, '$2y$10$9d8IVEao1Jcu6vOPcCJSBeVhgtlwYXRPgS6Yk/F8jfh0/0v0yofjm', NULL, '2020-05-27 20:53:53', '2020-05-27 20:53:53'),
(4, 'Michel Attallah', 'michelmelad@email.com', NULL, '$2y$10$D1niXib.xHpyt1obtrD01eKQurpljxf6Hp2Gqln5PJyaV16p2o5SS', NULL, '2020-05-27 23:29:40', '2020-05-27 23:29:40'),
(5, NULL, 'michelmelad55@hotmail.com', NULL, '$2y$10$e3tw3PGyQUlBROaCGZ3Dnu79P9jcDwFmIt4dLhizosKxf7BuZbQla', NULL, '2020-05-29 06:25:49', '2020-05-29 06:25:49'),
(6, NULL, 'agent@email.ca', NULL, '$2y$10$8iSDscoXvLDQF0mosGp97.j85RVMyehw3RNQCl3/p6Fc2Hs.1r79W', NULL, '2020-05-29 07:42:08', '2020-05-29 07:42:08'),
(7, NULL, 'new@wmail.ca', NULL, '$2y$10$5PO.rKoZYhHNvMeLFgJmfOz9GZ4gTa6j0QCZdgEqsUoZQ1HNE0wcW', NULL, '2020-05-31 20:35:11', '2020-05-31 20:35:11'),
(8, NULL, 'test2222@email.ca', NULL, '$2y$10$GHfMzHa1LnR.cK19i77kau8deYyctPPqMXvLvifqXGofMFjmlwTKW', NULL, '2020-05-31 22:05:35', '2020-05-31 22:05:35'),
(9, NULL, 'new@family.ca', NULL, '$2y$10$xCEUnfR6CdNsEFoMNqQNDef2WMVCFw1/EKFFcZZPnF3/0wcsPd/rq', NULL, '2020-05-31 22:54:57', '2020-05-31 22:54:57'),
(10, NULL, 'family@michel.ca', NULL, '$2y$10$MB84W7PaCa45QQPSciTcKORfa39wRoz2bszTjUqRCdJGL0IoBnvS6', NULL, '2020-06-01 00:07:40', '2020-06-01 00:07:40'),
(11, NULL, 'family2@michel.ca', NULL, '$2y$10$1K2F0eVokQNAm4qoMZV3p.1tFvJPgbz9Mb/DwvFGiA0KF8fJapFqG', NULL, '2020-06-01 00:09:20', '2020-06-01 00:09:20'),
(12, NULL, 'testuser@email.ca', NULL, '$2y$10$wqibIkQ/7EjXfqbF8v4KZe2aPUcj0VTd0WcMHwLzZcC9Eba/lbYwW', NULL, '2020-06-03 02:59:22', '2020-06-03 02:59:22'),
(13, NULL, 'michel@goda.ca', NULL, '$2y$10$UJDzvQrkreF752jYmP.Okei5CqrdhiKy5d5BNCRyC742tO6dI3H3G', NULL, '2020-06-03 05:22:04', '2020-06-03 05:22:04'),
(14, NULL, 'michel@test.ca', NULL, '$2y$10$DZQ6x4.ylJAShtAM9e8oOuHSrJ/GYtdh4kaS3nzrmMJcgAU6WpGia', NULL, '2020-06-03 06:07:41', '2020-06-03 06:07:41'),
(15, NULL, 'test@email333.com', NULL, '$2y$10$L7mBrCYk0lt33.XnHu476.LQ/gNK2Fy5uE0pgV1Obqe92t3U4YqmW', NULL, '2020-06-03 15:17:37', '2020-06-03 15:17:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admins_password_resets`
--
ALTER TABLE `admins_password_resets`
  ADD KEY `admins_password_resets_email_index` (`email`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_liturgy_id_index` (`liturgy_id`),
  ADD KEY `bookings_church_id_index` (`church_id`),
  ADD KEY `bookings_family_id_index` (`family_id`),
  ADD KEY `bookings_member_id_index` (`member_id`);

--
-- Indexes for table `churches`
--
ALTER TABLE `churches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `families`
--
ALTER TABLE `families`
  ADD PRIMARY KEY (`id`),
  ADD KEY `families_church_id_index` (`church_id`);

--
-- Indexes for table `liturgies`
--
ALTER TABLE `liturgies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `liturgies_church_id_index` (`church_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_family_uid_index` (`family_uid`),
  ADD KEY `members_church_id_index` (`church_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT for table `churches`
--
ALTER TABLE `churches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `families`
--
ALTER TABLE `families`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `liturgies`
--
ALTER TABLE `liturgies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_church_id_foreign` FOREIGN KEY (`church_id`) REFERENCES `churches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_family_id_foreign` FOREIGN KEY (`family_id`) REFERENCES `families` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_liturgy_id_foreign` FOREIGN KEY (`liturgy_id`) REFERENCES `liturgies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `families`
--
ALTER TABLE `families`
  ADD CONSTRAINT `families_church_id_foreign` FOREIGN KEY (`church_id`) REFERENCES `churches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `liturgies`
--
ALTER TABLE `liturgies`
  ADD CONSTRAINT `liturgies_church_id_foreign` FOREIGN KEY (`church_id`) REFERENCES `churches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_church_id_foreign` FOREIGN KEY (`church_id`) REFERENCES `churches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
