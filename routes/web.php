<?php

// Front User
Route::get('/auth/login', 'Front\\Auth\\LoginController@showLoginForm')->name('user.login');

Route::post('/auth/login', 'Front\\Auth\\LoginController@login')->name('user.login');


Route::get('/auth/register', 'Front\\Auth\\RegisterController@showRegistrationForm')->name('user.register');
Route::post('/auth/register', 'Front\\Auth\\RegisterController@register')->name('user.register');



Route::middleware(['auth:user' , 'has_family'])->group(function () {
  Route::get('/', 'Front\\HomeController@show')->name('app.home');

  
 
  
  Route::get('/{family}/members/{member}/edit/{type}', 'Front\\MemberController@edit')->name('user.members.edit');

  Route::post('/members/{member}/update/{field}', 'Front\\MemberController@updateField')->name('user.members.update');

  
  Route::delete('/{family}/members/{member}/destroy', 'Front\\MemberController@destroy')->name('user.members.destroy');


  Route::get('/liturgies/', 'Front\\LiturgiesController@index')->name('litugries.index');

  Route::get('/churches/{church}', 'Front\\ChurchesController@show')->name('churches.show');
  
  
  Route::get('/liturgies/{liturgy}/bookings', 'Front\\BookingsController@index')->name('user.booking.index');
  
  Route::get('/liturgies/{liturgy}/bookings/new', 'Front\\BookingsController@create')->name('user.booking.create');

  Route::post('/liturgies/{liturgy}/bookings/new', 'Front\\BookingsController@store')->name('user.booking.store');

  Route::delete('/liturgies/{liturgy}/bookings/{booking}/destroy', 'Front\\BookingsController@destroy')->name('user.booking.destroy');



  Route::get('/liturgies/{liturgy}/booking/{token}', 'Front\\BookingsController@show')->name('user.booking.show');

  
  
  Route::get('/bookings', 'Front\\BookingsController@index')->name('user.booking.index');


  
});


Route::middleware(['auth:user'])->group(function () {

  Route::get('/account', 'Front\\AccountController@show')->name('show.account');

  Route::put('/account/change-password', 'Front\\AccountController@changePassword')->name('account.change.password');

  
  Route::get('/{family}/members/create', 'Front\\MemberController@create')->name('create.family.member');

  Route::post('/{family}/members/create', 'Front\\MemberController@store')->name('user.members.add');
  

  Route::post('/ajax', 'Front\\AjaxController@excute')->name('front.ajax.route');
  
  Route::post('/upload/upload', 'Front\\UploadController@upload')->name('front.upload.url');

  Route::delete('/upload/destroy', 'Front\\UploadController@destroy')->name('front.upload.destroy');

  
  Route::post('/auth/logout', 'Front\\Auth\LoginController@logout')->name('user.logout');

  Route::get('/{user}/family/create', 'Front\\FamilyController@create')->name('create.user.family');

  Route::get('/{user}/family/{family}', 'Front\\FamilyController@show')->name('show.user.family');

  Route::post('/{user}/family/create', 'Front\\FamilyController@store')->name('store.user.family');
  
  Route::put('/{user}/family/{family}/update', 'Front\\FamilyController@update')->name('update.user.family');


});


Route::get('/public/liturgies/{liturgy}/booking/{token}', 'Front\\BookingsController@showForPublic')->name('public.user.booking.show');
