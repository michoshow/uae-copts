<?php 
use App\Http\Controllers\Backoffice\LanguageController;
 // locale Route
Route::get('lang/{locale}',[LanguageController::class,'swap']);

  


// Guest Admin
Route::prefix('backoffice/auth')->group(function () {

    Route::get('/login', 'Backoffice\\Auth\\LoginController@showLoginForm')->name('admin.login');
  
    Route::post('/login', 'Backoffice\\Auth\\LoginController@login')->name('admin.login');
  
    Route::post('/register', 'Backoffice\\Auth\\RegisterController@register')->name('admin.register');
  
    Route::get('/register', 'Backoffice\\Auth\\RegisterController@showRegistrationForm')->name('admin.register');
  
  });
  // Auth Admin  
  Route::middleware(['auth:admin'])->prefix('backoffice')->group(function () {
  
  Route::post('/auth/logout', 'Backoffice\\Auth\LoginController@logout')->name('admin.logout');
  Route::get('/dashboard', 'Backoffice\\DashboardController@dashboardAnalytics');
  
    
  // Families Resource
  Route::get('/families/{family}/members/add', 'Backoffice\\FamiliesController@addMembers')->name('families.add.members');
  Route::get('/families/data', 'Backoffice\\FamiliesController@getFamiliesData')->name('families.data');
  Route::get('/families/import', 'Backoffice\\FamiliesController@import');
  Route::post('/families/import', 'Backoffice\\FamiliesController@upload')->name('families.upload');
  Route::resource('/families', 'Backoffice\\FamiliesController')->middleware('role:admin|church-admin|super-admin');
    
  
  // Adminsitrators
  Route::get('/admins/data', 'Backoffice\\AdminsController@getAdminsData')->name('admins.data');
  Route::resource('/admins', 'Backoffice\\AdminsController')->middleware('role:super-admin');
  
  
  // Members
  Route::resource('/members', 'Backoffice\\MembersController')->middleware('role:admin|church-admin|super-admin');
  

  //Churches
  Route::get('/churches/data', 'Backoffice\\ChurchesController@getChurchesData')->name('churches.data');
  Route::resource('/churches', 'Backoffice\\ChurchesController');
  

  //liturgies
  Route::resource('/liturgies', 'Backoffice\\LiturgiesController');

  Route::get('/liturgies/data/{filter}', 'Backoffice\\LiturgiesController@getLiturgiesData')->name('liturgies.data');
  
  
  //Bookings
  Route::get('/bookings/data', 'Backoffice\\BookingsController@getBookingsData')->name('bookings.data');
  Route::resource('/bookings', 'Backoffice\\BookingsController');
  
  
  // Ajax Route
  Route::post('/ajax', 'Backoffice\\AjaxController@excute')->name('ajax.route');
   
});
?>