<?php

return [
    "Dashboard" => "لوحة الإعدادت",
    "Families" => "الأسر",
    "All Families" => "جميع الأسر",
    "Add New Family" => "أضف أسرة جديدة",
    "Import Data" => "رفع البيانات",
    "Settings" => "الإعدادت",
    "Administrators" => "المديرين",
    "login"          => 'تسجيل الدخول'   
];