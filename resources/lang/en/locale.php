<?php

return [
    "Dashboard"             => "Dashboard",
    "Families"              => "Families",
    "All Families"          => "All Families",
    "Add New Family"        => "Add New Family",
    "Import Data"           => "Import Data",
    "Settings"              => "Settings",
    "Administrators"        => "Administrators",
    "login"                 => 'Login',
    "Churches"              => 'Churches',
    "All Churches"          => 'All Churches',
    "Add New Church"        => 'Add New Church',
    "Liturgies"             => "Liturgies",
    "Coming Liturgies"      => "Coming Liturgies",
    "Archived Liturgies"    => "Archived Liturgies",
    "Add New Liturgy"       => "Add New Liturgy",
    "Bookings"              => "Bookings",
    "Coming Bookings"       => "Coming Bookings",
    "Archived Bookings"     => "Archived Bookings",
];