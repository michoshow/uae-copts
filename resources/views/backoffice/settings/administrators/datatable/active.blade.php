@if($admin->active)
    <div class="chip chip-success">
        <div class="chip-body">
          <span class="chip-text">{{ __('Active') }}</span>
        </div>
    </div>
@else
    <div class="chip chip-danger">
        <div class="chip-body">
        <span class="chip-text">{{ __('Inactive') }}</span>
        </div>
    </div>
@endif