@foreach ( $roles as $role)
    <div class="chip chip-primary">
        <div class="chip-body">
          <span class="chip-text">{{ ucwords(str_replace("-" , " " , $role)) }}</span>
        </div>
    </div>
@endforeach