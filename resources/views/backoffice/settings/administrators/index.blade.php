@extends('layouts/contentLayoutMaster')

@section('title', __('Administrators'))

@section('vendor-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('content')
<!-- Description -->
<section id="description" class="card">
  <div class="card-content">
      <div class="card-body">
          <div class="card-text">
            <div class="table-responsive">
                    <table class="table table-bordered" id="admins-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Roles</th>
                                <th>Account Status</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
            </div>
          </div>
      </div>
  </div>
</section>
<!--/ Description -->
@endsection


@section('vendor-script')
{{-- vendor files --}}
    <script src="{{ asset('js/libraries.js') }}"></script>
@endsection


@push('scripts')
<script>
$(function() {
    $('#admins-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admins.data') !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'role', name: 'role' },
            { data: 'active', name: 'active' },
            { data: 'action', name: 'action'}
        ]
    });
});
</script>
@endpush
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection