@extends('layouts/contentLayoutMaster')

@section('title', __('Administrators'))

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection


@section('content')
<!-- Description -->
<div class="card">
    <div class="card-header">
    <h4 class="card-title">Edit - {{ $admin->name }}</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
          

        <form class="form" novalidate method="POST" action="{{route('admins.update' , $admin)}}">
                
                @method('PUT')
                @csrf
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            @include('backoffice.partials.forms.text' , [
                                'label'     => 'Name',
                                'name'      => 'name',
                                'value'     => $admin->name,
                                'attr'      => false,
                                'class'     => false,
                                'required'  => true,
                                'disabled'  => false,
                                'helper'    => false,
                            ])
                        </div>
                        <div class="col-md-6 col-12">
                            @include('backoffice.partials.forms.email' , [
                                'label'     => 'Email',
                                'name'      => 'email',
                                'value'     => $admin->email,
                                'attr'      => false,
                                'class'     => false,
                                'required'  => true,
                                'disabled'  => false,
                                'helper'    => false,
                            ])
                        </div>
                        <div class="col-md-6 col-12">
                            @include('backoffice.partials.forms.password' , [
                                'label'         => 'Password',
                                'name'          => 'password',
                                'value'         => null,
                                'attr'          => false,
                                'class'         => false,
                                'required'      => false,
                                'disabled'      => false,
                                'placeholder'   => 'Leave blank to keep the old password',
                            ])
                        </div>
                        @hasanyrole('super-admin')
                        <div class="col-md-6 col-12">
                            @include('backoffice.partials.forms.select' , [
                                'label'         => 'Role',
                                'name'          => 'role[]',
                                'attr'          => false,
                                'class'         => false,
                                'multiple'      => true,
                                'max'           => false,
                                'value'         => $admin->roles->pluck('name')->toArray() ,
                                'data'          => [
                                        // Value        => Label
                                        'super-admin'   => 'Super Admin',
                                        'admin'         => 'Admin',
                                        'church-admin'  => 'Church Admin',
                                ],
                                'required'      => true,
                                'disabled'      => false,
                                'placeholder'   => '',
                            ])
                        </div>
                        
                        <div class="col-md-6 col-12">
                            @include('backoffice.partials.forms.switch' , [
                                'label'         => 'Is Active',
                                'name'          => 'active',
                                'value'         => $admin->active,
                                'default'       => 1,
                                'attr'          => false,
                                'class'         => false,
                                'required'      => false,
                                'disabled'      => false,
                            ])
                        </div>

                        @endhasanyrole
                        <div class="col-12">
                        
                        <a href="{{route('admins.index')}}" class="btn btn-outline-danger ml-1 mb-1 waves-effect waves-light pull-right">{{__('Cancel')}}</a>

                        <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Update')}}</button>

                       
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<!--/ Description -->
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection