@extends('layouts/contentLayoutMaster')

@section('title', __('Administrators'))

@section('vendor-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('content')
<!-- Description -->
<section id="description" class="card">
  {{--   <div class="card-header">
        <h4 class="card-title">Description</h4>
    </div> --}}
  <div class="card-content">
      <div class="card-body">
          <div class="card-text">
            <div class="table-responsive">
                <table class="table zero-configuration">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Active</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($admins as $admin)
                            @php
                                $roles = $admin->roles->pluck('name');
                            @endphp
                        <tr>
                            <td>
                            <a href="{{route('admins.edit' , $admin)}}">
                                    {{$admin->name}}
                                </a>
                            </td>
                            <td>{{$admin->email}}</td>
                            <td>
                                @foreach ( $roles as $role)
                                <div class="badge badge-primary badge-sm mr-1 mb-1">{{ ucwords(str_replace("-" , " " , $role)) }}</div>    
                                @endforeach
                            </td>
                            <td>
                                @if($admin->active)
                                <div class="badge badge-success badge-sm mr-1 mb-1">Active</div>
                                @else
                                <div class="badge badge-danger badge-sm mr-1 mb-1">Inactive</div>
                                @endif
                                
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Active</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
          </div>
      </div>
  </div>
</section>
<!--/ Description -->
@endsection


@section('vendor-script')
{{-- vendor files --}}
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ elixir('js/main.js') }}"></script>
@endsection