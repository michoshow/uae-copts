<div class="form-group form-label-group @if($errors->has($name)) error @endif">
    <div class="controls">
      
        <label for="{{$name}}-column">{{__($label)}}  @if($required) <span class="text-danger">*</span>  @endif</label>

        <input type="email" 
            name="{{$name}}" 
            class="form-control @if($class) {{ $class }}" @endif" 
            
            placeholder="{{(isset($placeholder)) ? __($placeholder)   : __($label)}}" 
            
            @if($value) value="{{ $value }}" @endif

            @if($required)  
                required="required"  
                data-validation-required-message="{{$label}} is required"
            @endif

            @if($attr)  {{ $attr }} @endif

            @if($disabled)  disabled  @endif
        >
        <div class="help-block">
            @if($errors->has($name))
                <ul role="alert"><li>{{$errors->first($name)}}</li></ul>
            @endif
        </div>
        @if (isset($helper) && !empty($helper))
            <p><small class="text-helper">{{__($helper)}}</small></p>
        @endif
       
        
    </div>
</div>