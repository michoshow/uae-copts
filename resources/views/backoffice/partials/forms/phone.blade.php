<div class="form-group form-label-group @if($errors->has($name)) error @endif">
    <div class="controls">
      
        <label for="{{$name}}-column">{{__($label)}}  @if($required) <span class="text-danger">*</span>  @endif</label>


        @if (isset($helper) && !empty($helper))
       {{--  <span class="helper-toggle" data-toggle="popover"
        data-content="{{__($helper)}}"
        data-trigger="hover" data-original-title="{{__($label)}}">
            ?
        </span> --}}
        @endif

        <input type="phone" 
            name="{{$name}}" 
            class="form-control @if($class) {{ $class }} phone" @endif" 
            
            placeholder="{{(isset($placeholder)) ? __($placeholder)   : __($label)}}" 
            
            @if($value) value="{{ $value }}" @endif


            @if($required)  
                required="required"  
                data-validation-required-message="{{$label}} is required"
            @endif

            @if(isset($min))  
                minlength={{$min}}  
                data-validation-minlength-message="Please enter a valid phone number"
            @endif

            data-validation-containsnumber-regex="([^0-9]*[0-9]+)+"

            @if($attr)  {{ $attr }} @endif

            @if($disabled)  disabled  @endif
        >
        <div class="clear clearfix"></div> 
        <div class="help-block">
            @if($errors->has($name))
                <ul role="alert"><li>{{$errors->first($name)}}</li></ul>
            @endif

        </div>
        @if (isset($helper) && !empty($helper))
            <p class="d-block"><small class="text-helper">{{__($helper)}}</small></p>
        @endif
       
        <div class="clear clearfix"></div>   
        
    </div>
</div>