<div class="form-group form-label-group @if($errors->has($name)) error @endif">
    <div class="controls">
      
        <label for="{{$name}}-column">{{__($label)}}  @if($required) <span class="text-danger">*</span>  @endif</label>

        <select 
    class="form-control  {{($max) ? 'max-length' : ''}} {{$class}}" 
            
            id="{{$name}}"

            @if($multiple)
                multiple="multiple" 
            @endif

            @if($max) ? {{"data-max=$max"}} @endif

            @if($required)  
                required="required"  
                data-validation-required-message="{{$label}} is required"
            @endif

            @if($disabled)  disabled  @endif
            name ="{{$name}}"

            @if($attr)  {{ $attr }} @endif

            >
            <option value="">{{__('Select')}}</option>
            @foreach ($data as $val => $label)
                @if (is_array($label))
                        @foreach ($label as $v=>$l)
                            <option data-chained="{{$val}}" value="{{$v}}" 
                            {{ (in_array($v , $value)) ? 'selected' : '' }}
                            >{{$l}}
                            </option>
                        @endforeach
                    @else
                        <option value="{{$val}}" 
                        {{ (in_array($val , $value)) ? 'selected' : '' }}
                        >{{$label}}</option>
                @endif
            @endforeach

            
        </select>
        <div class="help-block">
            @if($errors->has($name))
                <ul role="alert"><li>{{$errors->first($name)}}</li></ul>
            @endif
        </div>
        @if (isset($helper) && !empty($helper))
            <p><small class="text-helper">{{__($helper)}}</small></p>
        @endif       
        
    </div>
</div>