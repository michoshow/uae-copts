<div class="profile text-center">
    <a href="javascript: void(0);" onclick="app.startPorfileUpload()">
    <img src='{{(isset($profile_image)) ?$profile_image : 'http://www.michelmelad.com/avatar.png'}}' class="mr-75"
        alt="profile image" id="profile-thumb" width="100%">
    </a>

    @if (!isset($editable))
    <p class="text-center mt-2">Click here to upload your personal photo</p>
    <input type="file" id="account-upload"  name="profile_image" onchange="app.fileSelected(false)" required accept="image/*">
 
    @else
    
    <form enctype="multipart/form-data" id="update_profile_image" role="form" method="POST"> 
    <p class="text-center mt-2"><div class="loader"></div>Click here to change your personal photo</p>
    
    <input type="hidden" name="member" value="{{$member->uid}}"  />
    
    <input type="file"id="account-upload"  name="profile_image" onchange="app.fileSelected(true)" required  accept="image/*">
    </form>
    @endif
    
   
</div>