<div class="form-group form-label-group @if($errors->has($name)) error @endif">
    <div class="controls">

        
        <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
            <label class="mb-0 d-block" style="margin-top: 5px">{{__($label)}}  @if($required) <span class="text-danger">*</span>  @endif</label>


            <input 
            type="checkbox" 
            class="custom-control-input @if($class) {{ $class }}" @endif"
             id="{{$name}}"
             name="{{$name}}" 
             @if($required)  
                required="required"  
                data-validation-required-message="{{$label}} is required"
            @endif
            
            @if($attr)  {{ $attr }} @endif

            @if($disabled)  disabled  @endif

            @if($value)  checked  @endif

            value="{{$default}}"
            >
            
            <label class="custom-control-label" style="margin-top:8px" for="{{$name}}"></label>
            
            <div class="help-block">
                @if($errors->has($name))
                    <ul role="alert"><li>{{$errors->first($name)}}</li></ul>
                @endif
            </div>

            @if (isset($helper) && !empty($helper))
                <p><small class="text-helper">{{__($helper)}}</small></p>
            @endif       


            
        </div>
    </div>
</div>

