<div class="media"  x-data="imageData()">
    <template x-if="previewUrl !== ''">
        <a href="javascript: void(0);">
            <img :src="previewUrl" class="rounded mr-75"
            alt="profile image" height="64" width="64">
        </a>
    </template>

    <template x-if="previewUrl == ''">
        <a href="javascript: void(0);">
            <img src="http://www.michelmelad.com/avatar.png" class="rounded mr-75"
            alt="profile image" height="64" width="64">
        </a>
    </template>
    
    <div class="media-body mt-75">
      <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
        
        <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer"
      for="account-upload">{{__('Upload Personal Photo')}}</label>
          
        <input type="file" id="account-upload" hidden name="profile_image" @change="updatePreview()">


      <a  href="#" @click.prevent="clearPreview()" class="btn btn-sm btn-outline-warning ml-50">{{__('Reset')}}</a>

      </div>
      
        <p class="text-muted ml-75 mt-50"><small>{{__('Allowed JPG, GIF or PNG.')}}</small></p>
    </div>
    
  </div>
<script>
    function imageData() {
        
        return {
            previewUrl: "",
            updatePreview() {
            var reader,
                files = document.getElementById("account-upload").files;
                reader = new FileReader();
                reader.onload = e => {
                    this.previewUrl = e.target.result;
                };
                reader.readAsDataURL(files[0]);
            },
            clearPreview() {
            document.getElementById("account-upload").value = null;
            this.previewUrl = "";
            }
        };
    }
</script>