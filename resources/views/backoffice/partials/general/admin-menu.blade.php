<div class="dropdown-menu dropdown-menu-right">
  <a class="dropdown-item" href="javascript:void(0)"><i
    class="feather icon-user"></i> Edit Profile</a>
    
    {{-- <a class="dropdown-item" href="javascript:void(0)"><i
    class="feather icon-mail"></i> My
  Inbox</a>
   --}}
  {{-- <a class="dropdown-item" href="javascript:void(0)"><i class="feather icon-check-square"></i>
  Task</a>
  
  <a class="dropdown-item" href="javascript:void(0)"><i class="feather icon-message-square"></i>
  Chats</a> --}}

<div class="dropdown-divider"></div>
  <a class="dropdown-item" href="{{  route('admin.logout') }}"
  onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
  <i class="feather icon-power"></i> Logout
</a>

<form id="logout-form" action="{{  route('admin.logout') }}" method="POST" style="display: none;">
  {{ csrf_field() }}
</form>

  
</div>