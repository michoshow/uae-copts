<div class="card">
    <div class="card-header" style="padding-bottom: 1.5rem;">
        <h4 class="card-title">{{__("Family UID")}} - {{$family->uid}}</h4>
      <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
      <div class="heading-elements">
        <ul class="list-inline mb-0">
          <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="card-content collapse show" style="">
      <div class="card-body">
        <div class="card-text">
            <div class="table-responsive">
                <table class="table table-stripped">
                    <tr>
                     <td><strong>{{__('Address')}}</strong></td>
                     <td>{{$family->address}}</td>
                     <td><strong>{{__('Emirate')}}</strong></td>
                     <td>{{$family->area}} - {{$family->emirate}}</td>
                    </tr>

                    <tr>
                     <td><strong>{{__('Church')}}</strong></td>
                     <td colspan="3">{{$family->church->name}} - {{$family->church->emirate}}</td>
                    </tr>
                    
                    <tr>
                      <td><strong>{{__('Email')}}</strong></td>
                      <td>{{$family->email}}</td>
                      <td><strong>{{__('Phone')}}</strong></td>
                      <td>{{$family->phone}}</td>
                     </tr>

                </table>
              </div>
        </div>
      </div>
    </div>
  </div>