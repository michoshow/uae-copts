<div class="card">
<div class="card-header" style="padding-bottom: 1.5rem;">
    <h4 class="card-title">{{__("Family Members")}} ({{$family->members->count()}})</h4>
    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
    <div class="heading-elements">
    <ul class="list-inline mb-0">
        <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
    </ul>
    </div>
</div>

    

<div class="card-content collapse show" style="">
    <div class="card-body">
    <div class="card-text">
        @if ($family->members->count())

        <div class="table-responsive">
            <table class="table data-list-view">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Date of birth</th>
                    <th>Gender</th>
                    <th>Family role</th>
                    <th style="width:10%;">Actions</th>
                </tr>
                </thead>
                <tbody>
                
                    @foreach ($family->members as $member)
                        <tr id="{{$member->id}}">
                        <td></td>
                        <td class="d-flex">
                            
                            <div>
                                <div class="avatar mr-1 avatar-sm">
                                    <img src="{{$member->avatar}}" alt="avtar img holder">
                                </div>
                            </div>

                            <div>
                                {{$member->name}}
                            </div>
                            
                        </td>

                        <td>
                        
                            @if (isset($member->phone1))
                                {{$member->phone1}}
                            @endif
                        </td>
                        
                        <td class="product-price">{{$member->date_of_birth}}</td>
                        <td>
                            <div class="chip chip-primary">
                            <div class="chip-body">
                                <div class="chip-text">{{$member->gender}}</div>
                            </div>
                            </div>
                        </td>
                        
                        <td>
                            <div class="chip chip-primary">
                            <div class="chip-body">
                                <div class="chip-text">{{$member->family_role}}</div>
                            </div>
                            </div>
                        </td>
                        
                        <td><a href="#" class="btn btn-icon btn-flat-success waves-effect waves-light d-inline"><span class="action-edit"><i class="feather icon-edit"></i></span></a>

                        <a href="javascript:void(0)" data-route="{{route('members.destroy' , $member)}}" onclick="app.deleteItem(this)" class="btn btn-icon btn-flat-danger waves-effect waves-light d-inline"><i class="feather icon-trash"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div>
            <a href="{{route('families.add.members' , $family)}}" class="btn btn-primary mt-1 mb-2 pull-right">{{__('Add Family Member')}}</a>
        </div>
        
        @else

        <center>
            <div class="alert alert-warning col-md-6 col-md-offset-3 mt-md" style="white-space: normal;">
                <h4>{{__('There are no family members yet!')}}</h4>
                    <p>{{__('Start adding family members..')}}</p>
            </div>
            <a href="{{route('families.add.members' , $family)}}" class="btn btn-primary mt-1">{{__('Add Family Member')}}</a>
        </center>

        @endif
    </div>
    </div>
</div>


</div>