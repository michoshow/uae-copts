@extends('layouts/contentLayoutMaster')

@section('title', __('All Families'))

@section('vendor-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('content')

<section id="description" class="card">
    <div class="card-header">
        <div class="container p-0">
            <h4 class="card-title d-inline">{{__('All Families')}}</h4>
            <a href="{{route('families.create')}}" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">
                <i class="feather icon-plus"></i> {{__('Add New')}}
            </a>

        </div>
    </div>
    <div class="card-content">
        <div class="card-body">
            <div class="card-text">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="families-table">
                        <thead>
                            <tr>
                                <th>{{__('Details')}}</th>
                                <th>{{__('Family ID')}}</th>
                                <th  style="width:50%">{{__('Members')}}</th>
                                <th>{{__('Phone')}}</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Description -->
@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>

    <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@push('scripts')
<script id="family-details" type="text/x-handlebars-template">
    <table class="table table-striped" style="border-bottom:5px solid white">
        <tr>
            <td><strong>Address</strong></td>
            <td colspan="3">@{{address}}, @{{area}} - @{{emirate}}</td>
        </tr>

        <tr>
            <td><strong>Phone</strong></td>
            <td>@{{phone}}</td>
            
            <td><strong>Email</strong></td>
            <td>@{{email}}</td>
        </tr>
    </table>
</script>
<script>
$(function() {
    
    var details = Handlebars.compile($("#family-details").html());

    var table = $('#families-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('families.data') !!}',
        columns: [
            {
                "className":      'details-control',
                "orderable":      false,
                "searchable":     false,
                "data":           null,
                "defaultContent": ''
            },
            { data: 'uid', name: 'uid' },
            { data: 'members', name: 'members.first_name' ,orderable: false},
            { data: 'phone',   name: 'members.phone1' ,orderable: false},
            { data: 'action', name: 'action' ,  orderable: false, searchable: false}
        ],
        order: [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#families-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( details(row.data()) ).show();
            tr.addClass('shown');
        }
    });
});
</script>
@endpush
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection