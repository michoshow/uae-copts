@extends('layouts/contentLayoutMaster')

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('title', __('Add New Family'))

@section('content')
<!-- Description -->
<section id="description" class="card">
  <div class="card-header">
  <h4 class="card-title">{{__("Add Family")}}</h4>
  </div>
  <div class="card-content">
        <div class="card-body">
            <div class="card-text">
                <form class="form" novalidate method="POST" action="{{route('families.store')}}">
                    @csrf
                    <div class="form-body">
                        <input type="hidden" name="church" value="1" />
                        <div class="row">
                            <div class="col-md-6 col-12">
                                @include('backoffice.partials.forms.text' , [
                                    'label'     => 'Street Address',
                                    'name'      => 'address',
                                    'value'     => '',
                                    'attr'      => 'onkeyup=app.activateRetriveBtn()',
                                    'class'     => false,
                                    'required'  => true,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>
                            <div class="col-md-6 col-12">
                                @include('backoffice.partials.forms.text' , [
                                    'label'     => 'Area',
                                    'name'      => 'area',
                                    'value'     => '',
                                    'attr'      => 'onkeyup=app.activateRetriveBtn()',
                                    'class'     => false,
                                    'required'  => true,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>
                            <div class="col-md-6 col-12">
                                @include('backoffice.partials.forms.select' , [
                                    'label'         => 'City',
                                    'name'          => 'city',
                                    'attr'          => false,
                                    'class'         => false,
                                    'multiple'      => false,
                                    'max'           => false,
                                    'value'         => [] ,
                                    'data'          => [
                                            // Value        => Label
                                            'Abu Dhabi'     => 'Abu Dhabi',
                                            'Al Ain'        => 'Al Ain',
                                            'Al Dhafra'     => 'Al Dhafra',
                                    ],
                                    'required'      => true,
                                    'disabled'      => false,
                                    'placeholder'   => '',
                                ])
                            </div>
                            <div class="col-md-6 col-12">
                                @include('backoffice.partials.forms.select' , [
                                    'label'         => 'Emirate',
                                    'name'          => 'emirate',
                                    'attr'          => false,
                                    'class'         => false,
                                    'multiple'      => false,
                                    'max'           => false,
                                    'value'         => [] ,
                                    'data'          => [
                                            // Value        => Label
                                            'Abu Dhabi'     => 'Abu Dhabi',
                                            'Dubai'         => 'Dubai',
                                            'Al Sharja'     => 'Al Sharja',
                                            'Ajman'         => 'Ajman',
                                            'Ajman'         => 'Ajman',
                                            'Umm Al Quwain' => 'Umm Al Quwain',
                                            'Ras Al Khaimah'=> 'Ras Al Khaimah',
                                            'Fujairah'      => 'Fujairah',
                                    ],
                                    'required'      => true,
                                    'disabled'      => false,
                                    'placeholder'   => '',
                                ])
                            </div>
                            <div class="col-md-2 col-12">
                                @include('backoffice.partials.forms.text' , [
                                    'label'     => 'Latitude',
                                    'name'      => 'lat',
                                    'value'     => '',
                                    'attr'      => false,
                                    'class'     => false,
                                    'required'  => false,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>

                            <div class="col-md-2 col-12 mb-2">
                                @include('backoffice.partials.forms.text' , [
                                    'label'     => 'Longitude',
                                    'name'      => 'long',
                                    'value'     => '',
                                    'attr'      => false,
                                    'class'     => false,
                                    'required'  => false,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>
                            <div class="col-md-2 col-12 mb-2 pt-2">
                                <button id="retrive-coordinates" class="btn btn-outline-primary pull-right" onclick="app.retriveCoordinates(this)" disabled>
                                    <span class="btn-content">{{__('Retrive')}}</span>
                                </button>
                            </div>

                            <div class="col-md-6 col-12 mb-2">
                                @include('backoffice.partials.forms.text-repeater' , [
                                    'label'     => 'Landmarks',
                                    'name'      => 'landmarks',
                                    'value'     => '',
                                    'attr'      => false,
                                    'class'     => false,
                                    'required'  => false,
                                    'disabled'  => false,
                                    'helper'    => 'Press Enter to add multiple landmarks',
                                ])
                            </div>
                            <div class="col-12">
                                <a href="{{route('families.index')}}" class="btn btn-outline-danger ml-1 mb-1 waves-effect waves-light pull-right">{{__('Cancel')}}</a>

                                <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Create')}}</button>
    
                            
                            </div>
                        </div>
                    </div>
                </form>        
            </div>
        </div>
  </div>
</section>
<!--/ Description -->
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection