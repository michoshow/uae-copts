@extends('layouts/contentLayoutMaster')

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('title', __('Add Family Member'))

@section('content')
<!-- Description -->

{{--  @dump(Crypt::decryptString($family->address));  --}}

@include('backoffice.families.partials.family-details' , ['family' => $family])

@include('backoffice.families.partials.family-members' , ['family' => $family])


<!--/ Description -->
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
        
@endsection