@foreach ($members as $member)
<div class="chip">
    <a href="{{route('members.edit' , $member)}}" class="chip-body">
        <div class="avatar">
        <img class="img-fluid" src="{{$member->avatar}}" alt="generic img placeholder" width="20" height="20"/>
        </div>
        <span class="chip-text">{{$member->name}}</span>
    </a>
  </div>
@endforeach