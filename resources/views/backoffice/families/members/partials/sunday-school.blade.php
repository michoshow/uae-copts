<div class="col-12">
    <h5 class="mb-1">{{__('Sunday School Data')}}</h5>
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Current Class',
        'name'      => 'sunday_school_details[current_class]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'School Name',
        'name'      => 'sunday_school_details[school_name]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Sunday School class name',
        'name'      => 'sunday_school_details[sunday_school_class_name]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Bible Study class name',
        'name'      => 'sunday_school_details[bible_study_class_name]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>