<div class="col-12">
    <h5 class="mb-1">{{__('Hobbies and Talents')}}</h5>
</div>

<div class="col-md-12 col-12">
    @include('backoffice.partials.forms.text-repeater' , [
        'label'     => 'Hobbies and Talents',
        'name'      => 'talents_hobbies[]',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => 'Press Enter to add multiple',
    ])
</div>