<div class="col-12">
    <h5 class="mb-1">{{__('Family Details')}}</h5>
</div>

<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Family Role',
        'name'          => 'family_role',
        'attr'          => false,
        'class'         => false,
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value        => Label
            'Father'     => 'Father',
            'Wife'       => 'Wife',
            'Child'      => 'Child',
            'Brother'    => 'Brother',
            'Sister'     => 'Sister',
            'Relative'   => 'Relative',
            'Single'     => 'Single',
            'Other'      => 'Other',
        ],
        'required'      => true,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Marital Status',
        'name'          => 'marital_status',
        'attr'          => "onchange=app.toggleElement(this) data-show=marriage_date data-condition=eq|Married",
        'class'         => false,
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value        => Label
            'Married'   => 'Married',
            'Single'    => 'Single',
            'Wido'      => 'Wido',
            'Widower'   => 'Widower',
            'Divorced'  => 'Divorced',
        ],
        'required'      => true,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>

<div class="col-6 toggleable" id="marriage_date">
    @include('backoffice.partials.forms.date' , [
        'label'     => 'Date of marriage',
        'name'      => 'marriage_date',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>