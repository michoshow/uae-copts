<div class="col-12">
    <h5 class="mb-1">{{__('Education Details')}}</h5>
</div>
<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Degree',
        'name'          => 'qualification',
        'attr'          => false,
        'class'         => false,
        'multiple'      => true,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value            => Label
            'PhD'                       => 'PhD',
            "Bachelor's degree"         => "Bachelor's degree",
            'Diploma '                  => 'Diploma',
            'High School'               => 'High School',
            'Industrial High School'    => 'Industrial High School',
            'Prep'                      => 'Prep',
            'primary'                   => 'primary',
            'Without'                   => 'Without'
        ],
        'required'      => false,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Name of degree',
        'name'      => 'qualification_description',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'School',
        'name'      => 'shool',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Specialization',
        'name'      => 'specialization',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-12">
    @include('backoffice.partials.forms.textarea' , [
        'label'     => 'Additional Details',
        'name'      => 'education_details',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>