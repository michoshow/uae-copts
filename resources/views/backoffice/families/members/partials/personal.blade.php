<div class="col-12">
<h5 class="mb-1">{{__('Personal Information')}}</h5>
</div>
<div class="col-12">
    @include('backoffice.partials.forms.photo-uploader')
</div>
<hr>

<input type="hidden" name="family_uid" value="{{$family->uid}}">
<input type="hidden" name="church_id" value="1">

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'First Name',
        'name'      => 'first_name',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => true,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Middle Name',
        'name'      => 'middle_name',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => true,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Last Name',
        'name'      => 'last_name',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => true,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Additional Names',
        'name'      => 'additional_name',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.date' , [
        'label'     => 'Date of birth',
        'name'      => 'date_of_birth',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Gender',
        'name'          => 'gender',
        'attr'          => false,
        'class'         => false,
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value        => Label
            'Male'      => 'Male',
            'Female'    => 'Female'
        ],
        'required'      => true,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>