<div class="col-12">
    <h5 class="mb-1">{{__('Communication Details')}}</h5>
</div>

<div class="col-6">
    @include('backoffice.partials.forms.phone' , [
        'label'     => 'Phone 1',
        'name'      => 'phone1[number]',
        'value'     => '',
        'attr'      => 'onblur=app.checkFamilyProperty(this) data-prop=phone',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-3">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Family\'s primary phone',
        'name'          => 'phone_1[primary]',
        'value'         => '',
        'default'       => 1,
        'attr'          => "",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>
<div class="col-3">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Use for Whatsapp',
        'name'          => 'phone_1[whatsapp]',
        'value'         => '',
        'default'       => 1,
        'attr'          => "",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>


<div class="col-6">
    @include('backoffice.partials.forms.phone' , [
        'label'     => 'Phone 2',
        'name'      => 'phone2[number]',
        'value'     => '',
        'attr'      => 'onblur=app.checkFamilyProperty(this) data-prop=phone',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>


<div class="col-3">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Family\'s primary phone',
        'name'          => 'phone_2[primary]',
        'value'         => '',
        'default'       => 1,
        'attr'          => "data-primary=phone_2",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>
<div class="col-3">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Use for WhatsApp',
        'name'          => 'phone_2[whatsapp]',
        'value'         => '',
        'default'       => 1,
        'attr'          => "data-primary=phone_2",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.email' , [
        'label'     => 'Email',
        'name'      => 'email',
        'value'     => '',
        'attr'      => 'onblur=app.checkFamilyProperty(this) data-prop=email',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Set As Family\'s primary email',
        'name'          => 'email_is_primary',
        'value'         => '',
        'default'       => 1,
        'attr'          => "data-primary=email_1",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>