<div class="col-12">
    <h5 class="mb-1">{{__('Status in Egypt')}}</h5>
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Egyptian ID Number',
        'name'      => 'egypt_status[egypt_id_number]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>


<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Egyptian passport Number',
        'name'      => 'egypt_status[passpot_number]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Number of another ID',
        'name'      => 'egypt_status[another_id_number]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => "In case of don't have an Egyptian ID",
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Country name',
        'name'      => 'egypt_status[country_name]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => "In case of don't have an Egyptian ID",
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Governorate',
        'name'      => 'egypt_status[governorate]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => "",
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'City',
        'name'      => 'egypt_status[city]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => "",
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Name of the Church',
        'name'      => 'egypt_status[church_name]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => "In Egypt",
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Name of reference Priest in Egypt',
        'name'      => 'egypt_status[ref_priest]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => "In Egypt",
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Priest\'s communication number',
        'name'      => 'egypt_status[priest_number]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => "In Egypt",
    ])
</div>