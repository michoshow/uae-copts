<div class="col-12">
    <h5 class="mb-1">{{__('Service Details')}}</h5>
</div>

<div class="col-md-12 col-12">
    @include('backoffice.partials.forms.text-repeater' , [
        'label'     => 'Previous Services',
        'name'      => 'previous_services[]',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => 'Press Enter to add multiple',
    ])
</div>
<div class="col-md-12 col-12">
    @include('backoffice.partials.forms.text-repeater' , [
        'label'     => 'Services Want To Join',
        'name'      => 'services_want_to_join[]',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => 'Press Enter to add multiple',
    ])
</div>