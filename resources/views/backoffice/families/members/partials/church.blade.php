<div class="col-12">
    <h5 class="mb-1">{{__('Ecclesiastical Data')}}</h5>
</div>

<div class="col-12">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Name of the church',
        'name'          => 'church_name',
        'attr'          => false,
        'class'         => false,
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value        => Label
            'St Antony'   => 'St Antony',
            'St Mary'   => 'St Mary'
        ],
        'required'      => false,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>
<div class="col-2">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Deacon',
        'name'          => 'deacon',
        'value'         => '',
        'default'       => 1,
        'attr'          => "",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>
<div class="col-4">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Became a deacon in UAE',
        'name'          => 'deacon_in_uae',
        'value'         => '',
        'default'       => 1,
        'attr'          => "",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Deacon Degree',
        'name'          => 'deacon_degree',
        'attr'          => false,
        'class'         => false,
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value        => Label
            'St Antony'   => 'St Antony',
            'St Mary'   => 'St Mary'
        ],
        'required'      => false,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>


<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Servant In',
        'name'          => 'servant_in',
        'attr'          => false,
        'class'         => false,
        'multiple'      => true,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value            => Label
            'Family Meeting'   => 'Family Meeting',
            'Sunday Schools'   => 'Sunday Schools'
        ],
        'required'      => false,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Served In',
        'name'          => 'served_by',
        'attr'          => false,
        'class'         => false,
        'multiple'      => true,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value            => Label
            'Family Meeting'   => 'Family Meeting',
            'Sunday Schools'   => 'Sunday Schools'
        ],
        'required'      => false,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Father of confession',
        'name'          => 'father_of_confession',
        'attr'          => 'onchange=app.toggleElement(this) data-show=father_of_confession data-condition=eq|Other',
        'class'         => '',
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value            => Label
            'Fr. Antonious'   => 'Fr. Antonious',
            'Fr. Ebram'       => 'Fr. Ebram',
            'Fr. Beshoy'      => 'Fr. Beshoy',
            'Other'           => 'Other'
        ],
        'required'      => false,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>
<div class="col-6 toggleable" id="father_of_confession">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Name of father of confession',
        'name'      => 'father_of_confession',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Regsitered in St. Ebram meeting',
        'name'          => 'regsiterd_in_youth_meeting',
        'value'         => '',
        'default'       => 1,
        'attr'          => "",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>