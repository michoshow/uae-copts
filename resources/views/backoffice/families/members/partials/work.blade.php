<div class="col-12">
    <h5 class="mb-1">{{__('Work Details')}}</h5>
</div>
<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Company',
        'name'      => 'company',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Work Domain',
        'name'          => 'domain',
        'attr'          => false,
        'class'         => false,
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value            => Label
            'Engineering'               => 'Engineering',
            'Other'                     => 'Other'
        ],
        'required'      => false,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Job Title',
        'name'      => 'job_title',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-12">
    @include('backoffice.partials.forms.textarea' , [
        'label'     => 'Additional Details',
        'name'      => 'occupation',
        'value'     => '',
        'attr'      => false,
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>
