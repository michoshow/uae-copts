<div class="col-12">
    <h5 class="mb-1">{{__('Status in UAE')}}</h5>
</div>

<div class="col-6">
    @include('backoffice.partials.forms.switch' , [
        'label'         => 'Living permentally in UAE',
        'name'          => 'uae_status[living_in_uae]',
        'value'         => '',
        'default'       => 1,
        'attr'          => "",
        'class'         => false,
        'required'      => false,
        'disabled'      => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.date' , [
        'label'     => 'Date of first entery',
        'name'      => 'uae_status[first_entery_date]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.select' , [
        'label'         => 'Status in UAE',
        'name'          => 'uae_status[status_in_uae]',
        'attr'          => false,
        'class'         => false,
        'multiple'      => false,
        'max'           => false,
        'value'         => [],
        'data'          => [
            // Value        => Label
            'Permenant'     => 'Permenant',
            'Visitor'       => 'Visitor'
        ],
        'required'      => true,
        'disabled'      => false,
        'placeholder'   => '',
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.text' , [
        'label'     => 'Number of UAE ID',
        'name'      => 'uae_status[uae_id_number]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>

<div class="col-6">
    @include('backoffice.partials.forms.date' , [
        'label'     => 'UAE ID expiry date',
        'name'      => 'uae_status[uae_id_expiry_date]',
        'value'     => '',
        'attr'      => '',
        'class'     => false,
        'required'  => false,
        'disabled'  => false,
        'helper'    => false,
    ])
</div>