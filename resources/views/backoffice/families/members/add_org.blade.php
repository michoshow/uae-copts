@extends('layouts/contentLayoutMaster')

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('title', __('Add Family Member'))

@section('content')
<section id="description" class="card">
  <div class="card-header">
  <h4 class="card-title">{{__("Add Family Member")}} - {{__('Family ID:')}} {{$family->uid}}</h4>
  </div>
  <div class="card-content">
        <div class="card-body">
            <div class="card-text">
                <form class="form" novalidate method="POST" action="{{route('members.store')}}">
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-fluid card-img-top rounded-sm mb-2" src="http://www.michelmelad.com/avatar.png" alt="avtar img holder">

                                @include('backoffice.partials.forms.file' , [
                                    'label'     => 'Personal Photo',
                                    'name'      => 'personal_photo',
                                    'value'     => '',
                                    'attr'      => '',
                                    'class'     => false,
                                    'required'  => false,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])

                                <ul class="list-group list-group-flush">
                                    @foreach ($sections as $key=>$section)
                                        <li class="list-group-item">
                                            <a href="#{{$key}}" class="d-flex">
                                                <p class="float-left mb-0">
                                                <i class="{{$section['icon']}} mr-1"></i>
                                                </p>
                                                <span>{{__($section['title'])}}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                    
                                </ul>
                            </div>
                            <div class="col-md-8">

                                @foreach ($sections as $section=> $details)
                                <section class="card" id="{{$section}}">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="row">
                                                @include('backoffice.families.members.partials.' . $section )
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                @endforeach
                                
                                
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Add Family Member')}}</button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </form>        
            </div>
        </div>
  </div>
</section>
<!--/ Description -->
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
        
@endsection