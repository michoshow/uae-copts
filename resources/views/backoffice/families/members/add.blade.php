@extends('layouts/contentLayoutMaster')

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('title', __('Add Family Member'))

@section('content')
<section id="page-account-settings">
  <div class="row">
    <!-- left menu section -->
    <div class="col-md-3 mb-2 mb-md-0">
      <ul class="nav nav-pills flex-column mt-md-0 mt-1">
        @foreach ($sections as $key=> $section)
          <li class="nav-item {{($loop->index == 0) ? 'active' : ''}}">
            <a class="nav-link d-flex py-75 {{($loop->index == 0) ? 'active' : ''}}" id="account-pill-{{$key}}" data-toggle="pill"
            href="#{{$key}}" aria-expanded="true">
            <i class="{{$section['icon']}} mr-50 font-medium-3"></i>
            {{$section['title']}}
            </a>
        </li>
      @endforeach
        
      </ul>
    </div>
    <!-- right content section -->
    <div class="col-md-9">
      <form class="form" novalidate method="POST" action="{{route('members.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-body">
          <div class="card">
            <div class="card-content">
              <div class="card-body">
                <div class="tab-content">
                  @foreach ($sections as $key=> $section)
                <div role="tabpanel" class="tab-pane {{($loop->index ==0) ? 'active' : ''}}" id="{{$key}}"
                    aria-labelledby="{{$key}}" aria-expanded="true">
                      <div class="row">
                        @include('backoffice.families.members.partials.' . $key )
                      </div>
                  </div>
                  @endforeach
                </div>
          
                <div class="row">
                  <div class="col-12">
                      <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Add Family Member')}}</button>
                  </div>
              </div>

              
            </div>
          </div>
        </div>
      </div>
    </form>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
        
@endsection