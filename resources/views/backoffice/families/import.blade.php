@extends('layouts/contentLayoutMaster')

@section('title', __('Import Data'))

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection


@section('content')
<!-- Description -->
<div class="card">
    <div class="card-header">
    <h4 class="card-title">{{__('Import Data')}}</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
        <div class="container">  
            <div class="row">
                <div class="col-8">
                    <form class="form" novalidate method="POST" action="{{route('families.upload')}}" enctype="multipart/form-data">
                
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                @hasanyrole('super-admin')
                                <div class="col-md-12 col-12">
                                    @include('backoffice.partials.forms.select' , [
                                        'label'         => 'Data Type',
                                        'name'          => 'data_type',
                                        'attr'          => false,
                                        'class'         => false,
                                        'multiple'      => false,
                                        'max'           => false,
                                        'value'         => [] ,
                                        'data'          => [
                                                // Value        => Label
                                                'families'      => 'Families',
                                                'members'       => 'Family Members'
                                        ],
                                        'required'      => true,
                                        'disabled'      => false
                                    ])
                                </div>
                                <div class="col-md-12 col-12">
                                    @include('backoffice.partials.forms.file' , [
                                        'label'         => 'Data File',
                                        'name'          => 'data_file',
                                        'attr'          => false,
                                        'class'         => false,
                                        'value'         => false,
                                        'required'      => true,
                                        'disabled'      => false
                                    ])
                                </div>
        
                                @endhasanyrole
                                <div class="col-12">
                                    <button type="submit" class="btn btn-success mb-1 waves-effect waves-light pull-right">{{__('Upload')}}</button>
        
                                <a href="{{route('admins.index')}}" class="btn btn-danger mr-1 mb-1 waves-effect waves-light pull-right">Cancle</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        </div>
    </div>
</div>
<!--/ Description -->
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection