@extends('layouts/contentLayoutMaster')

@section('title', __($pageTitle))

@section('vendor-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('content')
<!-- Description -->
<section id="description" class="card">
    <div class="card-content">
      <div class="card-body">
          <div class="card-text">
            <div class="table-responsive">
                    <table class="table table-bordered" id="booking-table">
                        <thead>
                            <tr>
                                <th>Member</th>
                                <th>Liturgy</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
            </div>
          </div>
      </div>
  </div>
</section>
<!--/ Description -->
@endsection


@section('vendor-script')
{{-- vendor files --}}
    <script src="{{ asset('js/libraries.js') }}"></script>
@endsection


@push('scripts')
<script>
$(function() {
    $('#booking-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('bookings.data' , ['filter' => $filter , 'liturgy' => $liturgy])!!}',
        columns: [
            { data: 'member', name: 'id' , searchable: false},
            { data: 'title', name: 'id' , searchable: false},
            { data: 'date', name: 'date' },
            { data: 'action', name: 'action'}
        ]
    });
});
</script>
@endpush
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection