<div class="card">
    <div class="card-header" style="padding-bottom: 1.5rem;">
        <h4 class="card-title">{{__("Support")}}</h4>
        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
        <ul class="list-inline mb-0">
            <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
        </ul>
        </div>
    </div>
    <div class="card-content collapse show" style="">
        <div class="card-body">
            <div class="card-text">
                @foreach ($church->contacts['support'] as $person)
                
                    @include('backoffice.churches.partials.support-person' , ['person' => $person , 'index' => $loop->index])

                @endforeach
            </div>
        </div>
    </div>
</div>
