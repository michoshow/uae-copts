<div class="row">
    <div class="col-md-4 col-12">
        @include('backoffice.partials.forms.text' , [
            'label'     => 'Name',
            'name'      => "contacts[support][$index][name]",
            'value'     => $person['name'],
            'attr'      => '',
            'class'     => false,
            'required'  => false,
            'disabled'  => false,
            'helper'    => false,
        ])
    </div>
    <div class="col-md-4 col-12">
        @include('backoffice.partials.forms.text' , [
            'label'     => 'Phone',
            'name'      => "contacts[support][$index][phone]",
            'value'     => $person['phone'],
            'attr'      => '',
            'class'     => false,
            'required'  => false,
            'disabled'  => false,
            'helper'    => false,
        ])
    </div>
    <div class="col-md-4 col-12">
        @include('backoffice.partials.forms.text' , [
            'label'     => 'Email',
            'name'      => "contacts[support][$index][email]",
            'value'     => $person['email'],
            'attr'      => '',
            'class'     => false,
            'required'  => false,
            'disabled'  => false,
            'helper'    => false,
        ])
    </div>
</div>