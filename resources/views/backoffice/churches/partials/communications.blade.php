<div class="card">
    <div class="card-header" style="padding-bottom: 1.5rem;">
        <h4 class="card-title">{{__("Communication Details")}}</h4>
        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
        <ul class="list-inline mb-0">
            <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
        </ul>
        </div>
    </div>
    <div class="card-content collapse show" style="">
        <div class="card-body">
            <div class="card-text">
                <div class="row">
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.text' , [
                            'label'     => 'Website',
                            'name'      => 'contacts[communication][website]',
                            'value'     => $church->contacts['communication']['website'],
                            'attr'      => '',
                            'class'     => false,
                            'required'  => false,
                            'disabled'  => false,
                            'helper'    => false,
                        ])
                    </div>
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.text' , [
                            'label'     => 'facebook',
                            'name'      => 'contacts[communication][facebook]',
                            'value'     => $church->contacts['communication']['facebook'],
                            'attr'      => '',
                            'class'     => false,
                            'required'  => false,
                            'disabled'  => false,
                            'helper'    => false,
                        ])
                    </div>
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.text' , [
                            'label'     => 'Email',
                            'name'      => 'contacts[communication][email]',
                            'value'     => $church->contacts['communication']['email'],
                            'attr'      => '',
                            'class'     => false,
                            'required'  => false,
                            'disabled'  => false,
                            'helper'    => false,
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>