@extends('layouts/contentLayoutMaster')

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('title', __('Edit -' . $church->name ))

@section('content')
<!-- Description -->
<!-- Description -->
<section id="description" class="card">
    <div class="card-header">
    <h4 class="card-title">{{__("Edit -" . $church->name)}}</h4>
    </div>
    <div class="card-content">
          <div class="card-body">
              <div class="card-text">
                  <form class="form" novalidate method="POST" action="{{route('churches.update' , $church)}}">
                      @csrf
                      @method('PUT')
                      <div class="form-body">
                          <input type="hidden" name="church" value="1" />
                          <div class="row">
                              <div class="col-md-12 col-12">
                                  @include('backoffice.partials.forms.textarea' , [
                                      'label'     => 'Street Address',
                                      'name'      => 'address',
                                      'value'     => $church->address,
                                      'attr'      => '',
                                      'class'     => false,
                                      'required'  => true,
                                      'disabled'  => false,
                                      'helper'    => false,
                                  ])
                              </div>
                            
                                <div class="col-md-6 col-12">
                                  @include('backoffice.partials.forms.select' , [
                                      'label'         => 'Emirate',
                                      'name'          => 'emirate',
                                      'attr'          => false,
                                      'class'         => 'select2',
                                      'multiple'      => false,
                                      'max'           => false,
                                      'value'         => [$church->emirate] ,
                                      'data'          => [
                                              // Value        => Label
                                              "Abu Dhabi"         =>"Abu Dhabi",
                                              "Al Ain"            =>"Al Ain",
                                              "Dubai"             =>"Dubai",
                                              "Sharjah"           =>"Sharjah",
                                              "Ajman"             =>"Ajman",
                                              "Umm Al Quwain"     =>"Umm Al Quwain",
                                              "Fujairah"          =>"Fujairah",
                                              "Ras Al Khaimah"    =>"Ras Al Khaimah",
                                      ],
                                      'required'      => true,
                                      'disabled'      => false,
                                      'placeholder'   => '',
                                  ])
                              </div>
                              <div class="col-md-6 col-12">
                                  @include('backoffice.partials.forms.text' , [
                                      'label'     => 'Area',
                                      'name'      => 'area',
                                      'value'     => $church->area,
                                      'attr'      => '',
                                      'class'     => false,
                                      'required'  => true,
                                      'disabled'  => false,
                                      'helper'    => false,
                                  ])
                              </div>
                              
                          </div>
                      </div>
                       
              </div>
          </div>
    </div>
</section>
@include('backoffice.churches.partials.communications')
@include('backoffice.churches.partials.support')

<div class="wor">
    <div class="col-12">
        <a href="{{route('churches.index')}}" class="btn btn-outline-danger ml-1 mb-1 waves-effect waves-light pull-right">{{__('Cancel')}}</a>

        <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Edit')}}</button>
    </div>
    </form>   
</div>

<!--/ Description -->
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
        
@endsection

