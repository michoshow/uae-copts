@extends('layouts/contentLayoutMaster')

@section('title', __('Churches'))

@section('vendor-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('content')
<!-- Description -->
<section id="description" class="card">
  <div class="card-content">
      <div class="card-body">
          <div class="card-text">
            <div class="table-responsive">
                    <table class="table table-bordered" id="churches-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Emirate</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
            </div>
          </div>
      </div>
  </div>
</section>
<!--/ Description -->
@endsection


@section('vendor-script')
{{-- vendor files --}}
    <script src="{{ asset('js/libraries.js') }}"></script>
@endsection


@push('scripts')
<script>
$(function() {
    $('#churches-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('churches.data') !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'emirate', name: 'emirate' },
            { data: 'action', name: 'action'}
        ]
    });
});
</script>
@endpush
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection