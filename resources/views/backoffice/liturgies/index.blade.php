@extends('layouts/contentLayoutMaster')

@section('title', __($pageTitle))

@section('vendor-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('content')

<!-- Description -->
<section id="description" class="card">
    <div class="card-header">

    <a href="{{route('liturgies.create')}}" class="btn btn-success d-inline float-right right" style="float:right"><i class="feather icon-plus-square"></i> {{__('Add New Liturgy')}}</a>
      
    </div>
    <div class="card-content">
      <div class="card-body">
          <div class="card-text">
            <div class="table-responsive">
                    <table class="table table-bordered" id="liturgies-table">
                        <thead>
                            <tr>
                                <th>Liturgy</th>
                                <th>Date</th>
                                <th>Seats</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
            </div>
          </div>
      </div>
  </div>
</section>
<!--/ Description -->
@endsection


@section('vendor-script')
{{-- vendor files --}}
    <script src="{{ asset('js/libraries.js') }}"></script>
@endsection


@push('scripts')
<script>
$(function() {
    $('#liturgies-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('liturgies.data' , ['filter' => $filter])!!}',
        columns: [
            { data: 'title', name: 'title'},
            { data: 'date', name: 'date' },
            { data: 'seats', name: 'seats' },
            { data: 'action', name: 'action'}
        ]
    });
});
</script>
@endpush
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection