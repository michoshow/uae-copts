@php
    $stats = BackofficeHelper::seatsStats($liturgy->seats , $liturgy->allowedSeats)
@endphp

<p class="mb-0">Seats: {{$liturgy->allowedSeats}}/{{$liturgy->seats}}</p>
<div class="progress progress-bar-{{$stats['color']}} mt-25">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$stats['percentage']}}" aria-valuemin="{{$stats['percentage']}}" aria-valuemax="100" style="width:{{$stats['percentage']}}%"></div>
</div>