<a href="{{route('bookings.index' , ['filter' => request()->filter , 'liturgy' => $model->id])}}" class="btn btn-icon btn-flat-info waves-effect waves-light d-inline"><span class="action-edit"><i class="feather icon-tag"></i></span> {{__('Bookings')}}</a>

<a href="{{route( $modelName . '.edit' , $model)}}" class="btn btn-icon btn-flat-success waves-effect waves-light d-inline"><span class="action-edit"><i class="feather icon-edit"></i></span></a>

<a href="javascript:void(0)" 
    data-route="{{route($modelName . '.destroy' , $model)}}" 
    onclick="app.deleteItem(this)" 
    class="btn btn-icon btn-flat-danger waves-effect waves-light d-inline"><i class="feather icon-trash"></i></span></a>