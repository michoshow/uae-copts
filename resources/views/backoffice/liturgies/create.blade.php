@extends('layouts/contentLayoutMaster')

@section('page-style')
        <link rel="stylesheet" href="{{ asset('css/libraries.css') }}">
@endsection

@section('title', __('Add New Liturgy'))

@section('content')
<!-- Description -->
<section id="description" class="card">
  <div class="card-header">
  <h4 class="card-title">{{__("Add Liturgy")}}</h4>
  </div>
  <div class="card-content">
        <div class="card-body">
            <div class="card-text">
                <form class="form" novalidate method="POST" action="{{route('liturgies.store')}}">
                    @csrf
                    <div class="form-body">
                        <input type="hidden" name="church" value="1" />
                        <div class="row">
                            <div class="col-md-6 col-12">
                                @include('backoffice.partials.forms.select' , [
                                    'label'         => 'Church',
                                    'name'          => 'church',
                                    'attr'          => false,
                                    'class'         => false,
                                    'multiple'      => false,
                                    'max'           => false,
                                    'required'      => true,
                                    'value'         => [] ,
                                    'data'          => Front::churchesList(),
                                    'disabled'      => false,
                                    'placeholder'   => '',
                                ])
                            </div>

                            <div class="col-md-6 col-12">
                                @include('backoffice.partials.forms.text' , [
                                    'label'     => 'Title',
                                    'name'      => 'title',
                                    'value'     => 'Holy Liturgy',
                                    'attr'      => '',
                                    'class'     => false,
                                    'required'  => true,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>
                          
                            
                            <div class="col-md-3 col-12">
                                @include('backoffice.partials.forms.date' , [
                                    'label'     => 'Date',
                                    'name'      => 'date',
                                    'value'     => '',
                                    'attr'      => '',
                                    'class'     => false,
                                    'required'  => true,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>
                            <div class="col-md-3 col-12">
                                @include('backoffice.partials.forms.time' , [
                                    'label'     => 'Start Time',
                                    'name'      => 'start',
                                    'value'     => '',
                                    'attr'      => '',
                                    'class'     => false,
                                    'required'  => true,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>
                            <div class="col-md-3 col-12">
                                @include('backoffice.partials.forms.time' , [
                                    'label'     => 'End Time',
                                    'name'      => 'end',
                                    'value'     => '',
                                    'attr'      => '',
                                    'class'     => false,
                                    'required'  => true,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>

                            <div class="col-md-3 col-12">
                                @include('backoffice.partials.forms.text' , [
                                    'label'     => 'Seats',
                                    'name'      => 'seats',
                                    'value'     => '',
                                    'attr'      => '',
                                    'class'     => false,
                                    'required'  => true,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>

                            <div class="col-md-12 col-12">
                                @include('backoffice.partials.forms.textarea' , [
                                    'label'     => 'Note',
                                    'name'      => 'description',
                                    'value'     => '',
                                    'attr'      => '',
                                    'class'     => false,
                                    'required'  => false,
                                    'disabled'  => false,
                                    'helper'    => false,
                                ])
                            </div>

                          
                            <div class="col-12">
                                <a href="{{route('families.index')}}" class="btn btn-outline-danger ml-1 mb-1 waves-effect waves-light pull-right">{{__('Cancel')}}</a>

                                <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Create')}}</button>
    
                            
                            </div>
                        </div>
                    </div>
                </form>        
            </div>
        </div>
  </div>
</section>
<!--/ Description -->
@endsection

@section('vendor-script')
        <script src="{{ asset('js/libraries.js') }}"></script>
@endsection
@section('page-script')
        <script src="{{ asset('js/main.js') }}"></script>
@endsection