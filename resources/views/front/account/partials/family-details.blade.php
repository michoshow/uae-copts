<div role="tabpanel" class="tab-pane active p-2" id="family"
    aria-labelledby="family" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Edit your family details')}}</h4>

<form class="form form-validate" novalidate method="POST" action="{{route('update.user.family' , [
    'user' => Auth::guard('user')->user(),
    'family' => $family,
])}}">
        @csrf
        @method('PUT')
        
        <div class="form-body">
            <input type="hidden" name="church" value="1" />
            <div class="row">
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'Street Address | العنوان',
                        'name'      => 'address',
                        'value'     => $family->address,
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>

                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.select' , [
                        'label'         => 'Emirate | الإمارة',
                        'name'          => 'emirate',
                        'attr'          => 'data-chain=area',
                        'class'         => 'data-chain',
                        'multiple'      => false,
                        'max'           => false,
                        'value'         => [$family->emirate] ,
                        'data'          => [
                                "Abu_Dhabi"         =>"Abu Dhabi",
                                "Al_Ain"            =>"Al Ain",
                                "Dubai"             =>"Dubai",
                                "Sharjah"           =>"Sharjah",
                                "Ajman"             =>"Ajman",
                                "Umm_Al_Quwain"     =>"Umm Al Quwain",
                                "Fujairah"          =>"Fujairah",
                                "Ras_Al_Khaimah"    =>"Ras Al Khaimah",
                        ],
                        'required'      => true,
                        'disabled'      => false,
                        'placeholder'   => '',
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.chained' , [
                        'label'         => 'Area | المنطقة',
                        'name'          => 'area',
                        'attr'          => false,
                        'class'         => false,
                        'multiple'      => false,
                        'max'           => false,
                        'value'         => [$family->area] ,
                        'data'          => Front::locations(),
                        'required'      => true,
                        'disabled'      => false,
                        'placeholder'   => '',
                    ])
                </div>
              

                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.select' , [
                        'label'         => 'Church | الكنيسة',
                        'name'          => 'church',
                        'attr'          => false,
                        'class'         => false,
                        'multiple'      => false,
                        'max'           => false,
                        'value'         => [$family->church->id] ,
                        'data'          => Front::churchesList(),
                        'required'      => true,
                        'disabled'      => false,
                        'placeholder'   => '',
                    ])
                </div>

            </div>

            <div class="row">
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.phone' , [
                        'label'     => 'Primary Phone Number | رقم التليفون الرئيسي',
                        'name'      => 'phone',
                        'value'     => $family->phone,
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => "Used for communications and receiving booking confirmations  | يستخدم للاتصالات وتلقي تأكيدات الحجز",
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.email' , [
                        'label'     => 'Primary Email | البريد الإلكتروني الرئيسي',
                        'name'      => 'email',
                        'value'     => $family->email,
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => "Used for communications and receiving booking confirmations | يستخدم للاتصالات وتلقي تأكيدات الحجز",
                    ])
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Edit')}}</button>
                </div>
            </div>
        </div>
    </form> 
</div>