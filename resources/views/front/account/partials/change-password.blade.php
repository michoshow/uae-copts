<div role="tabpanel" class="tab-pane  p-2" id="password"
    aria-labelledby="password" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Change Password')}}</h4>
  
<form class="form form-validate" novalidate method="POST" action="{{route('account.change.password')}}">
        @csrf
        @method('PUT')
        
        <div class="form-body">
            <input type="hidden" name="church" value="1" />
            <div class="row">
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.password' , [
                        'label'     => 'New Password',
                        'name'      => 'password',
                        'value'     => '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.password' , [
                        'label'     => 'Confirm Password',
                        'name'      => 'password_confirmation',
                        'value'     => '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Change Password')}}</button>
                </div>
            </div>
        </div>
    </form> 
</div>