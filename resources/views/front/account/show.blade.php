
@extends('front.layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section id="page-account-settings">
                        <div class="row">
                          <!-- left menu section -->
                          <div class="col-md-3 mb-2 mb-md-0">
                            <ul class="nav nav-pills flex-column mt-md-0 mt-1">

                                <li class="nav-item active">
                                  <a class="nav-link d-flex py-75 active" id="account-pill-family" data-toggle="pill"
                                  href="#family" aria-expanded="true">
                                  <i class="feather icon-settings mr-50 font-medium-3"></i>
                                    {{__('Family Details')}}
                                  </a>
                                </li>

                                <li class="nav-item">
                                  <a class="nav-link d-flex py-75" id="account-pill-password" data-toggle="pill"
                                  href="#password" aria-expanded="true">
                                  <i class="feather icon-lock mr-50 font-medium-3"></i>
                                    {{__('Change Password')}}
                                  </a>
                              </li>
                            </ul>
                          </div>
                          <!-- right content section -->
                          <div class="col-md-9">
                            
                              <div class="form-body">
                                <div class="card">
                                  <div class="card-content">
                                    <div class="card-body">
                                       @if (session()->has('success'))
                                            <div class="alert alert-success">
                                               {{session('success')}}
                                            </div>
                                       @endif
                                        @if(count($errors))
                                            <div class="alert alert-danger">
                                                <h4 class="alert-heading">{{
                                                    __('Something went wrong!') }}</h4>
                                                <p>
                                                <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                      <div class="tab-content">
                                        
                                            @include('front.account.partials.family-details' , ['family' => $family])

                                            @include('front.account.partials.change-password')
                                        
                                        </div>
                                
                                      {{-- <div class="row">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Edit Account')}}</button>
                                        </div>
                                    </div> --}}
                      
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                    
                          </div>
                        </div>
                      </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection