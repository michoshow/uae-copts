<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    
    <meta name="description" content="UAE Copts">
    <meta name="keywords" content="UAE Copts">
    <meta name="author" content="UAE Copts">

    <title>UAE Copts</title>
    
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">

    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Cairo&family=Markazi+Text:wght@400;500;600;700&display=swap" rel="stylesheet"> 

        <!-- BEGIN: Vendor CSS-->
        {{-- @include('front.layout.css-rtl') --}}
        @include('front.layout.css-ltr')
        <!-- END: Vendor CSS-->
    </head>

    <body class="horizontal-layout horizontal-menu 1-column navbar-floating footer-static bg-full-screen-image blank-page blank-page" data-open="hover" data-menu="horizontal-menu" data-col="1-column">

        <div class="app-content content">
                <div class="content-overlay"></div>
                <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    @yield('content')
                </div>
            </div>
        </div>

    <!-- END: Body-->
     <!-- END: Content-->
{!! NoCaptcha::renderJs() !!}
<script src="{{asset('front/js/libraries.js')}}"></script>
</body>
<!-- END: Content-->    
</html>