<footer class="footer footer-static footer-light navbar-shadow">
    <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; @php date('Y') @endphp<a class="text-bold-800 grey darken-2" href="" target="_blank">UAE IT Team,</a>All rights Reserved</span>
    </p>
</footer>