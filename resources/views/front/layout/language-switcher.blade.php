<li class="dropdown dropdown-language nav-item">
                
                <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flag-icon flag-icon-eg"></i><span class="selected-language">العربية</span>
                </a>
                
                <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                    
                    <a class="dropdown-item" href="#" data-language="en"><i class="flag-icon flag-icon-us"></i> الإنجليزية</a>
                    
                    <a class="dropdown-item" href="#" data-language="ar"><i class="flag-icon flag-icon-eg"></i> العربية</a>

                </div>
            </li>