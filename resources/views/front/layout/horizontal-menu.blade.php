 <!-- BEGIN: Main Menu-->
 <div class="horizontal-menu-wrapper">
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-without-dd-arrow navbar-shadow menu-border navbar-brand-center" role="navigation" data-menu="menu-wrapper" data-nav="brand-center">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{route('app.home')}}">
                        <div class="brand-logo"></div>
                        <h2 class="brand-text mb-0">{{env('APP_NAME')}}</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
            </ul>
        </div>
        <!-- Horizontal menu content-->
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                
                <li class="nav-item">
                    <a class="nav-link" href="{{route('app.home')}}">
                        <i class="feather icon-home"></i>
                        <span>{{__('Home')}}</span>
                    </a>
                </li>
                @if (isset(Auth::guard('user')->user()->family))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('show.user.family' , ['family' => Auth::guard('user')->user()->family, 'user' => Auth::guard('user')->user()])}}">
                        <i class="feather icon-users"></i>
                        <span>{{__('My Family')}}</span>
                    </a>
                </li>
                
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="{{route('litugries.index')}}" data-toggle="dropdown">
                        <i class="feather icon-bell"></i><span>{{__('Liturgies')}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        
                       
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown" data-i18n="Charts">{{__('My Bookings')}}</a>
                            <ul class="dropdown-menu">

                                <li data-menu=""><a class="dropdown-item" href="{{route('user.booking.index' , ['filter'=>'coming'])}}" data-toggle="dropdown" data-i18n="Apex"><i class="feather icon-circle"></i>{{__('Coming Bookings')}}</a>
                                </li>
                                
                                <li data-menu=""><a class="dropdown-item" href="{{route('user.booking.index' , ['filter'=>'archived'])}}" data-toggle="dropdown" data-i18n="Chartjs"><i class="feather icon-circle"></i>{{__('Archived Bookings')}}</a>
                                </li>
                           
                            </ul>
                        </li>

                        <li class="" data-menu=""><a class="dropdown-item" href="{{route('churches.show' , ['church' => Auth::guard('user')->user()->family->church_id])}}" data-toggle="dropdown" data-i18n="Fixed navbar">{{__('My Church')}}</a>
                        </li>
                    </ul>
                </li>
                @endif
                
            </ul>
        </div>
        <!-- /horizontal menu content-->
    </div>
</div>