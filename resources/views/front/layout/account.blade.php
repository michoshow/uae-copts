<li class="dropdown dropdown-user nav-item">
    <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">

    <span><img class="round" src="{{auth()->user()->gravatar}}" alt="avatar" height="40" width="40"></span>

        <div class="user-nav d-sm-flex d-none ml-1">
            <span class="user-name text-bold-600">{{auth()->user()->name}}</span>
        </div>
        
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="{{route('show.account')}}">
        <i class="feather icon-user"></i>{{__('Edit your account')}}</a>
        <div class="dropdown-divider"></div>

        <a class="dropdown-item" href="{{  route('user.logout') }}"
        onclick="event.preventDefault();
                document.getElementById('logout-form').submit();"><i class="feather icon-power"></i>{{__('Logout')}}</a>

<form id="logout-form" action="{{  route('user.logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
    </form>
    </div>
</li>