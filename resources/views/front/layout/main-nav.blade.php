<!-- BEGIN: Header-->
<div class="content-overlay"></div>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed navbar-brand-center">
    
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                
                <ul class="nav navbar-nav float-right">
                    {{-- @include('front.layout.language-switcher') --}}
                    @include('front.layout.account')
                    {{-- @include('front.layout.notifications') --}}
                </ul>

                <div class="ml-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                    </ul>

                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item">
                            <a class="navbar-brand" href="{{route('app.home')}}">
                                <div class="brand-logo">UAE Copts</div>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
@include('front.layout.horizontal-menu')