<!DOCTYPE html>
<html class="loading" lang="ar" data-textdirection="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    
    <meta name="description" content="UAE Copts">
    <meta name="keywords" content="UAE Copts">
    <meta name="author" content="UAE Copts">

    <meta name="csrf-token-front" content="{{ csrf_token() }}">
    <meta name="front.ajax.route" content="{{ route('front.ajax.route') }}">

<title>{{env('UAE Copts')}}</title>
    
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">

    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Cairo&family=Markazi+Text:wght@400;500;600;700&display=swap" rel="stylesheet"> 

    <link href="https://transloadit.edgly.net/releases/uppy/v1.15.0/uppy.min.css" rel="stylesheet">


        <!-- BEGIN: Vendor CSS-->
        {{-- @include('front.layout.css-rtl') --}}
        @include('front.layout.css-ltr')
        <!-- END: Vendor CSS-->
    </head>

    <body class="horizontal-layout 2-columns ecommerce-application navbar-floating footer-static pace-done menu-hide vertical-layout vertical-overlay-menu fixed-navbar" data-open="hover" data-menu="horizontal-menu" data-col="2-columns" data-gr-c-s-loaded="true">
        @include('front.layout.main-nav-ltr')
        @include('front.layout.horizontal-menu')
        
    <!-- END: Main Menu-->
        @yield('content')
    <!-- END: Body-->

 <!-- END: Content-->

 <div class="sidenav-overlay"></div>
 <div class="drag-target"></div>

 @include('front.layout.footer')
 
<script src="{{asset('front/js/libraries.js')}}"></script>

<script src="https://transloadit.edgly.net/releases/uppy/v1.15.0/uppy.min.js"></script>

<script src="{{asset('front/js/app.js')}}"></script>
</body>
<!-- END: Content-->    
</html>