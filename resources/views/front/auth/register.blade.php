@extends('front.layout.auth')

@section('content')
<section class="row flexbox-container">
    <div class="col-xl-8 col-10 d-flex justify-content-center">
        <div class="card rounded-0 mb-0">
            <div class="row m-0">
                <div class="col-lg-5 d-lg-block d-none text-center align-self-center p-0">
                    <img style="width: 100%" src="/images/pages/login.png" alt="branding logo">
                </div>
              <div class="col-lg-7 col-12 p-0">
                  <div class="card rounded-0 mb-0 p-2">
                      <div class="card-header pt-50 pb-1">
                          <div class="card-title">
                          <h4 class="mb-0">{{__("CREATE ACCOUNT")}}</h4>
                          </div>
                      </div>
                    <p class="px-2">{{__('Fill the below form to create a new account.')}}</p>
                      <div class="card-content">
                          <div class="card-body pt-0">
                            <form method="POST" action="{{ route('user.register') }}">
                              @csrf
                                  <div class="form-label-group">
                                      <!-- <input type="email" id="inputEmail" class="form-control" placeholder="Email" required> -->
                                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email">
                                      

                                  <label for="email">{{__('Email')}}</label>
                                      @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                      @enderror
                                  </div>
                                  <div class="form-label-group">
                                      <!-- <input type="password" id="inputPassword" class="form-control" placeholder="Password" required> -->
                                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">
                                      
                                      <p><small class="text-muted">{{__("Minmum 6 characters")}}</small></p>

                                  <label for="password">{{__('Password')}}</label>
                                      @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                      @enderror
                                  </div>
                                  <div class="form-label-group">
                                      <!-- <input type="password" id="inputConfPassword" class="form-control" placeholder="Confirm Password" required> -->
                                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
                                  <label for="password-confirm">{{__('Confirm Password')}}</label>
                                  </div>
                                 {{--  <div class="form-group row">
                                      <div class="col-12">
                                          <fieldset class="checkbox">
                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                              <input type="checkbox" checked>
                                              <span class="vs-checkbox">
                                                <span class="vs-checkbox--check">
                                                  <i class="vs-icon feather icon-check"></i>
                                                </span>
                                              </span>
                                            <span class="">{{__(' I accept the terms & conditions.')}}</span>
                                            </div>
                                          </fieldset>
                                      </div>
                                  </div> --}}
                                   {{--    <a href="{{route('user.login')}}" class="btn btn-outline-primary float-left btn-inline mb-50">Login</a> --}}
                                    <div class="form-group">
                                        {!! NoCaptcha::display() !!}
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="invalid-feedback" style="display: block" role="alert">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                  <button type="submit" class="btn btn-primary float-right btn-inline mb-50">Register</a>

                              </form>
                          </div>
                      </div>
                        <div class="login-footer pb-3 pt-2">
                            <div class="row">
                                <div class="col-12">
                                <div class="text-center">
                                    <p>Already have an account?</p>
                                    <a href="{{route('user.login')}}" class="btn btn-outline-primary btn-outline">Login</a>
                                </div>
                                </div>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection
