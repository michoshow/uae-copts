@extends('front.layout.auth')

@section('content')
<section class="row flexbox-container">
  <div class="col-xl-8 col-10 d-flex justify-content-center">
      <div class="card rounded-0 mb-0">
          <div class="row m-0">
              <div class="col-lg-5 d-lg-block d-none text-center align-self-center p-0">
                  <img style="width: 100%" src="/images/pages/login.png" alt="branding logo">
              </div>
              <div class="col-lg-7 col-12 p-0">
                  <div class="card rounded-0 mb-0 px-2">
                      <div class="card-header pb-1  text-center">
                        <h4 class="mb-0 d-block">{{__('LOGIN')}}</h4>
                      </div>
                    <p class="px-2">First time, please <a href="{{route('user.register')}}"><strong>register</strong></a>. Otherwise login.</p>
                      <div class="card-content">
                          <div class="card-body pt-1">
                            <form method="POST" action="{{ route('user.login') }}">
                                @csrf
                                <fieldset class="form-label-group form-group position-relative has-icon-left">
              
                                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" placeholder="E-Mail Address" value="{{ old('email') }}" required autocomplete="email"
                                    autofocus>
              
                                   <div class="form-control-position">
                                    <i class="feather icon-user"></i>
                                  </div> 
              
                                  <label for="email">E-Mail Address</label>
                                  @error('email')
                                  <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                  @enderror
                                </fieldset>
              
                                <fieldset class="form-label-group position-relative has-icon-left">
              
                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                    name="password" placeholder="Password" required autocomplete="current-password">
              
                                   <div class="form-control-position">
                                    <i class="feather icon-lock"></i>
                                  </div> 
                                  
                                  <label for="password">Password</label>
                                  @error('password')
                                  <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                  </span>
                                  @enderror
                                </fieldset>
                                <div class="form-group d-flex justify-content-between align-items-center">
                                  <div class="text-left">
                                    <fieldset class="checkbox">
                                      <div class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                        <span class="vs-checkbox">
                                          <span class="vs-checkbox--check">
                                            <i class="vs-icon feather icon-check"></i>
                                          </span>
                                        </span>
                                        <span class="">Remember me</span>
                                      </div>
                                    </fieldset>
                                  </div>
                                  @if (Route::has('password.request'))
                                  <div class="text-right"><a class="card-link" href="{{ route('password.request') }}">
                                      Forgot Password?
                                    </a></div>
                                  @endif
                                </div>
                                <div class="form-group">
                                {!! NoCaptcha::display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="invalid-feedback" style="display: block" role="alert">
                                      <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <button type="submit" class="btn btn-primary float-right btn-inline">Login</button>
                              </form>
                          </div>
                      </div>
                      <div class="login-footer pb-3 pt-2">
                        <div class="row">
                            <div class="col-12">
                              <div class="text-center">
                                <p>Don't have an account?</p>
                                <a href="{{route('user.register')}}" class="btn btn-outline-primary btn-outline">Register</a>
                             </div>
                            </div>
                        </div>

                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection