<div class="alert alert-warning" role="alert">
<h4 class="alert-heading">{{__('No Family Members')}}</h4>
    <p>
        {{__('It seems that you didn\'t add your family members yet!')}} <br/>
        {{__('Please, add your details and your family members details to be able to book for liturgies')}}
    </p>
    <p>
        @include('front.dashboard.add-family-member') 
    </p>
    <div class="clear clearfix"></div>
</div>