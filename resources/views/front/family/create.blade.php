@extends('front.layout.layout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper">

<div class="content-body">
<!-- Description -->
<div class="row">
<div class="col-md-8 offset-md-2 col-12">
<section id="description" class="card">
<div class="card-header">
<h4 class="card-title">{{__('Add your family details')}}</h4>
</div>
<div class="card-content">
<div class="card-body">
    <div class="card-text">
        
        <form class="form form-validate" novalidate method="POST" action="{{route('store.user.family' , $user)}}">
            @csrf
            <div class="form-body">
                <input type="hidden" name="church" value="1" />
                <div class="row">
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.text' , [
                            'label'     => 'Street Address | العنوان',
                            'name'      => 'address',
                            'value'     => old('address'),
                            'attr'      => '',
                            'class'     => false,
                            'required'  => true,
                            'disabled'  => false,
                            'helper'    => false,
                        ])
                    </div>

                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.select' , [
                            'label'         => 'Emirate | الإمارة',
                            'name'          => 'emirate',
                            'attr'          => 'data-chain=area',
                            'class'         => 'data-chain',
                            'multiple'      => false,
                            'max'           => false,
                            'value'         => [old('emirate')] ,
                            'data'          => [
                                    "Abu_Dhabi"         =>"Abu Dhabi",
                                    "Al_Ain"            =>"Al Ain",
                                    "Dubai"             =>"Dubai",
                                    "Sharjah"           =>"Sharjah",
                                    "Ajman"             =>"Ajman",
                                    "Umm_Al_Quwain"     =>"Umm Al Quwain",
                                    "Fujairah"          =>"Fujairah",
                                    "Ras_Al_Khaimah"    =>"Ras Al Khaimah",
                            ],
                            'required'      => true,
                            'disabled'      => false,
                            'placeholder'   => '',
                        ])
                    </div>
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.chained' , [
                            'label'         => 'Area | المنطقة',
                            'name'          => 'area',
                            'attr'          => false,
                            'class'         => false,
                            'multiple'      => false,
                            'max'           => false,
                            'value'         => [old('area')] ,
                            'data'          => Front::locations(),
                            'required'      => true,
                            'disabled'      => false,
                            'placeholder'   => '',
                        ])
                    </div>
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.select' , [
                            'label'         => 'Church | الكنيسة',
                            'name'          => 'church',
                            'attr'          => false,
                            'class'         => false,
                            'multiple'      => false,
                            'max'           => false,
                            'value'         => [old('church')] ,
                            'data'          => Front::churchesList(),
                            'required'      => true,
                            'disabled'      => false,
                            'placeholder'   => '',
                        ])
                    </div>

                   {{--   <div class="col-md-6 col-12 mb-2">
                        @include('backoffice.partials.forms.text-repeater' , [
                            'label'     => 'Landmarks',
                            'name'      => 'landmarks',
                            'value'     => '',
                            'attr'      => false,
                            'class'     => false,
                            'required'  => false,
                            'disabled'  => false,
                            'helper'    => 'Press Enter to add multiple landmarks',
                        ])
                    </div>  --}}
                </div>

                <div class="row">
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.phone' , [
                            'label'     => 'Primary Phone Number | رقم التليفون الرئيسي',
                            'name'      => 'phone',
                            'value'     => old('phone'),
                            'attr'      => '',
                            'class'     => 'inter-phone-code',
                            'required'  => true,
                            'disabled'  => false,
                            'helper'    => "Used for communications and receiving booking confirmations  | يستخدم للاتصالات وتلقي تأكيدات الحجز",
                        ])
                    </div>
                    <div class="col-md-6 col-12">
                        @include('backoffice.partials.forms.email' , [
                            'label'     => 'Primary Email | البريد الإلكتروني الرئيسي',
                            'name'      => 'email',
                            'value'     => null !== old('email') ? old('email') : Auth::guard('user')->user()->email,
                            'attr'      => '',
                            'class'     => false,
                            'required'  => true,
                            'disabled'  => false,
                            'helper'    => "Used for communications and receiving booking confirmations | يستخدم للاتصالات وتلقي تأكيدات الحجز",
                        ])
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Save')}}</button>
                    </div>
                </div>
            </div>
        </form>    
    </div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
@endsection