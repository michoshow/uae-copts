@extends('front.layout.layout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
    <div class="content-wrapper">

        <div class="content-body">
            <div class="row">
                <div class="col-md-8 col-12">
                    @include('front.dashboard.my-family')
                
                </div>
                <div class="col-md-4 col-12">
                    @include('front.dashboard.my-liturgies')
                    @include('front.dashboard.churches')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection