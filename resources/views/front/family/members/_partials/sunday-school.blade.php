<h6>{{__('Sunday School')}}</h6>
<fieldset>
    <div class="row">
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Which church you attend the Liturgies in | أي كنيسة تحضر القداسات بها',
                'name'          => 'church',
                'attr'          => '',
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [],
                'data'          => Front::churchesList(),
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>

        <div class="col-md-6 col-12" >
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Sunday school Grade\Class Name',
                'name'      => 'sunday_school_details',
                'value'     => '',
                'attr'      => '',
                'class'     => false,
                'required'  => false,
                'disabled'  => false,
                'helper'    => "",
            ])
        </div>

    </div>

  
</fieldset>