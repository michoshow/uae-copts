<h6>{{__('Emergency')}}</h6>
<fieldset>
    
    @include('front.family.members._partials.emergency-person-uae')
    @include('front.family.members._partials.emergency-person-egypt')
    
</fieldset>