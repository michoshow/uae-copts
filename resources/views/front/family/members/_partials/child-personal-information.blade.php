<h6>{{__('Personal Information')}}</h6>
<fieldset>
    <div class="row">
        <input type="hidden" value="Child" name="family_role">
        <input type="hidden" name="type" value="child" />

        <div class="col-md-4 col-12">
            @include('backoffice.partials.forms.profile-photo') 
        </div>

        <div class="col-md-8 col-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'First Name',
                        'name'      => 'first_name',
                        'value'     => '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
        
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'Father Name',
                        'name'      => 'middle_name',
                        'value'     => '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'Last Name',
                        'name'      => 'last_name',
                        'value'     => '',
                        'attr'      => false,
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'Additional Names',
                        'name'      => 'additional_name',
                        'value'     => '',
                        'attr'      => false,
                        'class'     => false,
                        'required'  => false,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
        
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.date' , [
                        'label'     => 'Date of birth',
                        'name'      => 'date_of_birth',
                        'value'     => '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => false,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.select' , [
                        'label'         => 'Gender',
                        'name'          => 'gender',
                        'attr'          => false,
                        'class'         => '',
                        'multiple'      => false,
                        'max'           => false,
                        'value'         => [],
                        'data'          => [
                            // Value        => Label
                            'Male'      => 'Male',
                            'Female'    => 'Female'
                        ],
                        'required'      => true,
                        'disabled'      => false,
                        'placeholder'   => '',
                    ])
                </div>
            </div>
        </div>
       
    </div>
</fieldset>