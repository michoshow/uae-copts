<h6>{{__('Job and Profession')}}</h6>
<fieldset>
    <div class="row">
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية',
                'name'      => 'qualification',
                'value'     => isset($member) ? $member->qualification : '',
                'attr'      => '',
                'class'     => false,
                'required'  => false,
                'disabled'  => false,
                'helper'    => "",
            ])
        </div>

        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Field | مجال العمل',
                'name'          => 'domain',
                'attr'          => '',
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [isset($member) ? $member->domain : ''],
                'data'          => Front::workDomainList(),
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Employer | جهة العمل',
                'name'      => 'company',
                'value'     => isset($member) ? $member->company : '',
                'attr'      => '',
                'class'     => false,
                'required'  => false,
                'disabled'  => false,
                'helper'    => "",
            ])
        </div>
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Job | الوظيفة',
                'name'      => 'job_title',
                'value'     => isset($member) ? $member->job_title : '',
                'attr'      => '',
                'class'     => false,
                'required'  => false,
                'disabled'  => false,
                'helper'    => "",
            ])
        </div>
    </div>
</fieldset>