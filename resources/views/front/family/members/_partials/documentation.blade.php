<h6>{{__('Documentation')}}</h6>
<fieldset>
    <div class="row">
        
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Nationality | الجنسية',
                'name'          => 'nationality',
                'attr'          => 'onchange=app.toggleElement(this) data-conditions=id_number_1_wrapper|eq|Egyptian,id_number_3_wrapper|eq|Other',
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [isset($member) ? $member->nationality : ''],
                'data'          => [
                    // Value        => Label
                    'Egyptian'      => 'Egyptian | مصري',
                    'Other'          => 'Other | أخرى'
                ],
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>

        <div class="col-md-6 col-12 hidden" id="id_number_1_wrapper">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Egypt National ID | الرقم القومي المصري',
                'name'      => 'id_number_1',
                'value'     => isset($member) ? $member->id_number_1 : '',
                'attr'      => '',
                'class'     => false,
                'required'  => true,
                'disabled'  => false,
                'helper'    => "لمن معهم جواز مصري - الرقم القومي موجود في الجواز المصري",
            ])
        </div>

        <div class="col-md-6 col-12 hidden" id="id_number_3_wrapper">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Passport Number | رقم جواز السفر',
                'name'      => 'id_number_3',
                'value'     => isset($member) ? $member->id_number_3 : '',
                'attr'      => '',
                'class'     => false,
                'required'  => true,
                'disabled'  => false,
                'helper'    => "",
            ])
        </div>

        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Visa Type | نوع الفيزا',
                'name'          => 'uae_status',
                'attr'          => 'onchange=app.toggleElement(this) data-conditions=id_number_2_wrapper+upload_uae_id_wrapper|eq|Residence',
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [isset($member) ? $member->uae_status : ''],
                'data'          => [
                    // Value        => Label
                    'Residence'      => 'Residence | إقامة',
                    'Visit'          => 'Visit | زيارة',
                    'Other'          => 'Other | أخرى'
                ],
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>
        <div class="col-md-6 col-12 hidden" id="id_number_2_wrapper">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'For residents Emirates ID | رقم الهوية الاماراتية للمقيمين',
                'name'      => 'id_number_2',
                'value'     => isset($member) ? $member->id_number_2 : '',
                'attr'      => 'disabled',
                'class'     => false,
                'required'  => true,
                'disabled'  => false,
                'helper'    => "",
            ])
        </div>
        
        <div id="upload_uae_id_wrapper" class=" mb-2 col-md-10 offset-md-1 col-12 hidden">
            <div data-message="Upload All IDs (Front and Back) | برجاء تحميل صورة لوجهي الهوية الإماراتية " id="drag-drop-area" style="margin:0 auto;"></div>
            <input name="files_list" id="Files_List" type="text"   
            style="display: block;width: 100%;height: 1px; border:none;opacity:0"
            >
        </div>
    </div>
</fieldset>