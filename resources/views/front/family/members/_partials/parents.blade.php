<h6>{{__('Parents')}}</h6>
<fieldset>
    <div class="alert alert-info">
        Please fill their names in case parents are living with you في حالة اقامة الوالدين معك في الامارات برجاء إضافة اسمائهم 
    </div>
    @include('front.family.members._partials.parent')
</fieldset>