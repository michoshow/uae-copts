<h6>{{__('Personal Information')}}</h6>
<fieldset>
    <div class="row">
        
        <div class="col-12">
            <div class="alert alert-info text-center">
                Before filling the form please ensure that you have
Photos, passport copies, Emirates ID copies for all family members.
                <br />
                قبل ملء النموذج يرجى التأكد من أن لديك
صور ونسخ جوازات السفر ونسخ الهوية الإماراتية لجميع أفراد الأسرة
            </div>
        </div>
            
        <div class="col-12">
            
            <h6 class="border-bottom py-1 my-2 mb-0 font-medium-2">Personal Information | البيانات الشخصية
            </h6>

        </div>
        
        <input type="hidden" name="type" value="adult" />

        <div class="col-md-4 col-12">
            @include('backoffice.partials.forms.profile-photo') 
        </div>

        <div class="col-md-8 col-12">
            <div class="row">
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'First Name | الإسم الاول',
                        'name'      => 'first_name',
                        'value'     => isset($member) ? $member->first_name : '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
        
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'Father Name | إسم الأب',
                        'name'      => 'middle_name',
                        'value'     => isset($member) ? $member->middle_name : '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'Last Name | إسم الجد',
                        'name'      => 'last_name',
                        'value'     => isset($member) ? $member->last_name : '',
                        'attr'      => false,
                        'class'     => false,
                        'required'  => true,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.text' , [
                        'label'     => 'Additional Names | أسماء أخرى',
                        'name'      => 'additional_name',
                        'value'     => isset($member) ? $member->additional_name : '',
                        'attr'      => false,
                        'class'     => false,
                        'required'  => false,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.date' , [
                        'label'     => 'Date of birth | تاريخ الميلاد',
                        'name'      => 'date_of_birth',
                        'value'     => isset($member) ? $member->date_of_birth : '',
                        'attr'      => '',
                        'class'     => false,
                        'required'  => false,
                        'disabled'  => false,
                        'helper'    => false,
                    ])
                </div>
                <div class="col-md-6 col-12">
                    @include('backoffice.partials.forms.select' , [
                        'label'         => 'Gender | الجنس',
                        'name'          => 'gender',
                        'attr'          => false,
                        'class'         => '',
                        'multiple'      => false,
                        'max'           => false,
                        'value'         => [isset($member) ? $member->gender : ''],
                        'data'          => [
                            // Value        => Label
                            'Male'      => 'Male',
                            'Female'    => 'Female'
                        ],
                        'required'      => true,
                        'disabled'      => false,
                        'placeholder'   => '',
                    ])
                </div>
                <input type="hidden" name="family_role" value="" />

        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Marital Status | الحالة الاجتماعية',
                'name'          => 'marital_status',
                'attr'          => "",
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [isset($member) ? $member->marital_status : ''],
                'data'          => [
                    // Value        => Label
                    'Single'      => 'Single | أعزب',
                    'Married'     => 'Married | متزوج',
                    'Widow'       => 'Widow | أرمل'
                ],
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>
        

        <div class="col-md-6 col-12" id="family_role_wrapper">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Family Role',
                'name'          => 'family_role',
                'attr'          => false,
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [isset($member) ? $member->family_role : ''],
                'data'          => [
                    // Value        => Label
                    'Husband'      => 'Husband',
                    'Wife'         => 'Wife',
                    'Son'          => 'Son',
                    'Daughter'     => 'Daughter',
                    'Father'       => 'Father',
                    'Mother'       => 'Mother',
                    'Father in law'=> 'Father in law',
                    'Mother in law'=> 'Mother in law',
                    'Brother'      => 'Brother',
                    'Sister'       => 'Sister',
                    'Other'        => 'Other',
                ],
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>
            </div>
        </div>
    
    <div class="col-12">
        <h6 class="border-bottom py-1 my-2 mb-0 font-medium-2">Communications Information | بيانات التواصل</h6>
    </div>

    <div class="col-md-6 col-12">
        @include('backoffice.partials.forms.phone' , [
            'label'     => 'Mobile # | رقم المحمول',
            'name'      => 'phone1',
            'value'     => isset($member) ? $member->phone1 : '',
            'attr'      => '',
            'class'     => 'inter-phone-code',
            'required'  => true,
            'disabled'  => false,
            'helper'    => "",
        ])
    </div>
    <div class="col-md-6 col-12">
        @include('backoffice.partials.forms.phone' , [
            'label'     => 'WhatsApp # | رقم الواتساب',
            'name'      => 'whatsapp',
            'value'     => isset($member) ? $member->whatsapp : '',
            'attr'      => '',
            'class'     => 'inter-phone-code',
            'required'  => false,
            'disabled'  => false,
            'helper'    => "If Different than your mobile phone number",
        ])
    </div>
    <div class="col-md-6 col-12">
        @include('backoffice.partials.forms.phone' , [
            'label'     => 'Home Telephone # | رقم هاتف المنزل',
            'name'      => 'phone2',
            'value'     => isset($member) ? $member->phone2 : '',
            'attr'      => '',
            'class'     => 'inter-phone-code',
            'required'  => false,
            'disabled'  => false,
            'helper'    => "",
        ])
    </div>

</div>
   
</fieldset>