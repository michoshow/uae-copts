<div x-data="handler()">

    <div class="row">
      <div class="col-12">
        <h6 class="border-bottom py-1 my-2 mb-0 font-medium-2">Emergency contacts in UAE</h6>
      </div>
    </div>
    <template  x-for="(person, index) in persons" :key="index">
        <div class="row">
          <div class="col-md-4 col-12">
            @include('backoffice.partials.forms.text' , [
           'label'     => 'Name',
           'name'      => 'emergency[uae][name][]',
           'value'     => '',
           'attr'      => 'x-model=person.name',
           'class'     => false,
           'required'  => true,
           'disabled'  => false,
           'helper'    => "",
         ]) 
          </div>
          <div class="col-md-3 col-12">
           @include('backoffice.partials.forms.phone' , [
             'label'     => 'Phone',
             'name'      => 'emergency[uae][phone][]',
             'value'     => '',
             'attr'      => 'x-model=person.phone',
             'class'     => false,
             'required'  => true,
             'disabled'  => false,
             'helper'    => "",
           ]) 

          </div>
          <div class="col-md-3 col-12">
      
           @include('backoffice.partials.forms.text' , [
             'label'     => 'Relation',
             'name'      => 'emergency[uae][relation][]',
             'value'     => '',
             'attr'      => 'x-model=person.relation',
             'class'     => false,
             'required'  => true,
             'disabled'  => false,
             'helper'    => "",
           ]) 

          </div>
          <div class="col-md-1 col-12">
            <button type="button" class="btn btn-icon btn-icon rounded-circle btn-danger mt-2 waves-effect waves-light" @click="removeField(index)"><i class="feather icon-trash-2"></i></button>
          </div>
        </div>
      </template>
      <div class="row">
        <div class="col-12">
          <button type="button" class="btn btn-info pull-right float-right mb-3" @click="addNewField()">+ Add Contact</button>
          <div class="clear clearfix"></div>
        </div>
      </div>
    
    </div>

    <script>
        function handler() {
            return {
              persons: [],
            addNewField() {
                this.persons.push({
                    name: '',
                    phone: '',
                    relation: ''
                });
                },
                removeField(index) {
                this.persons.splice(index, 1);
                }
            }
        }
    </script>