<div role="tabpanel" class="tab-pane p-2" id="documentation"
    aria-labelledby="documentation" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Documentation')}}</h4>
    <table class="table table-striped mb-0">
        <tbody>
           
            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Nationality | الجنسية',
                'field'   => 'nationality',
                'value'   => $member->nationality,
                'member'  => $member,
                'source'  => [
                    'Egyptian'      => 'Egyptian | مصري',
                    'Other'          => 'Other | أخرى'
                ],  
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Egypt National ID | الرقم القومي المصري',
                'field'  => 'id_number_1',
                'value'  => $member->id_number_1,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Passport Number | رقم جواز السفر',
                'field'  => 'id_number_3',
                'value'  => $member->id_number_3,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.select', [
                'label'  => 'Visa Type | نوع الفيزا',
                'field'  => 'uae_status',
                'value'  =>  $member->uae_status,
                'member'  => $member,
                'source'  => [
                    'Residence'      => 'Residence | إقامة',
                    'Visit'          => 'Visit | زيارة',
                    'Other'          => 'Other | أخرى'
                ],  
            ])

            
            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'For residents Emirates ID | رقم الهوية الاماراتية للمقيمين',
                'field'  => 'id_number_2',
                'value'  => $member->id_number_2,
                'member'  => $member,
            ])
         


        </tbody>

    </table>
    
</div>