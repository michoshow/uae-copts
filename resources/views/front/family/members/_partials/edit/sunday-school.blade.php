<div role="tabpanel" class="tab-pane p-2" id="sunday-school"
    aria-labelledby="sunday-school" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Sunday School')}}</h4>
    <table class="table table-striped mb-0">
        <tbody>
           
            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Which church you attend the Liturgies in | أي كنيسة تحضر القداسات بها',
                'field'   => 'church_id',
                'value'   => $member->church->id,
                'member'  => $member,
                'source'  => Front::churchesList(),
                'text'    => $member->church->name  
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Sunday school Grade\Class Name',
                'field'  => 'sunday_school_details',
                'value'  => $member->sunday_school_details,
                'member' => $member,
            ])

            
         


        </tbody>

    </table>
    
</div>