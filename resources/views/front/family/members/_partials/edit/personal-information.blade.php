<div role="tabpanel" class="tab-pane active p-2" id="personal"
    aria-labelledby="personal" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Personal Details')}}</h4>
    <table class="table table-striped mb-0">
        <tbody>
            <tr>
                <td colspan="2">
                    <h6>Personal Information | البيانات الشخصية
                    </h6>
                </td>
            </tr>

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'First Name | الإسم الاول',
                'field'  => 'first_name',
                'value'  => $member->first_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Father Name | إسم الأب',
                'field'  => 'middle_name',
                'value'  => $member->middle_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Last Name | إسم الجد',
                'field'  => 'last_name',
                'value'  => $member->last_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Additional Names | أسماء أخرى',
                'field'  => 'additional_name',
                'value'  => $member->additional_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.date', [
                'label'  => 'Date of birth | تاريخ الميلاد',
                'field'  => 'date_of_birth',
                'value'  => $member->date_of_birth,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Gender | الجنس',
                'field'   => 'gender',
                'value'   => $member->gender,
                'member'  => $member,
                'source'  => [
                    'Male'      => 'Male',
                    'Female'    => 'Female'
                ],  
            ])

            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Marital Status | الحالة الاجتماعية',
                'field'   => 'marital_status',
                'value'   => $member->marital_status,
                'member'  => $member,
                'source'  => [
                    'Single'      => 'Single | أعزب',
                    'Married'     => 'Married | متزوج',
                    'Widow'       => 'Widow | أرمل'
                ],
            ])

            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Family Role',
                'field'   => 'family_role',
                'value'   => $member->family_role,
                'member'  => $member,
                'source'  => [
                    'Husband'      => 'Husband',
                    'Wife'         => 'Wife',
                    'Son'          => 'Son',
                    'Daughter'     => 'Daughter',
                    'Father'       => 'Father',
                    'Mother'       => 'Mother',
                    'Father in law'=> 'Father in law',
                    'Mother in law'=> 'Mother in law',
                    'Brother'      => 'Brother',
                    'Sister'       => 'Sister',
                    'Other'        => 'Other',
                ],
            ])

            <tr>
                <td colspan="2">
                    <h6>Communications Information | بيانات التواصل</h6>
                </td>
            </tr>

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Mobile # | رقم المحمول',
                'field'  => 'phone1',
                'value'  => $member->phone1,
                'member' => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'WhatsApp # | رقم الواتساب',
                'field'  => 'whatsapp',
                'value'  => $member->whatsapp,
                'member' => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Home Telephone # | رقم هاتف المنزل',
                'field'  => 'phone2',
                'value'  => $member->phone2,
                'member' => $member,
            ])


        </tbody>
    </table>
    
</div>