<div role="tabpanel" class="tab-pane active p-2" id="personal-child"
    aria-labelledby="personal-child" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Personal Details')}}</h4>
    <table class="table table-striped mb-0">
        <tbody>
            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'First Name | الإسم الاول',
                'field'  => 'first_name',
                'value'  => $member->first_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Father Name | إسم الأب',
                'field'  => 'middle_name',
                'value'  => $member->middle_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Last Name | إسم الجد',
                'field'  => 'last_name',
                'value'  => $member->last_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Additional Names | أسماء أخرى',
                'field'  => 'additional_name',
                'value'  => $member->additional_name,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.date', [
                'label'  => 'Date of birth | تاريخ الميلاد',
                'field'  => 'date_of_birth',
                'value'  => $member->date_of_birth,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Gender | الجنس',
                'field'   => 'gender',
                'value'   => $member->gender,
                'member'  => $member,
                'source'  => [
                    'Male'      => 'Male',
                    'Female'    => 'Female'
                ],  
            ])
        </tbody>
    </table>
    
</div>