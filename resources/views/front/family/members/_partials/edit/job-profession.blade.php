<div role="tabpanel" class="tab-pane p-2" id="job"
    aria-labelledby="job" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Job and Profession')}}</h4>
    <table class="table table-striped mb-0">
        <tbody>
            
            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Qualification & Degree | المؤهل الدراسي والدرجة العلمية',
                'field'  => 'qualification',
                'value'  => $member->qualification,
                'member'  => $member,
            ])

            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Field | مجال العمل',
                'field'   => 'domain',
                'value'   => $member->domain,
                'member'  => $member,
                'source'  =>  Front::workDomainList()
                
            ])

            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Employer | جهة العمل',
                'field'  => 'company',
                'value'  => $member->comapny,
                'member'  => $member,
            ])
            
            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'Job | الوظيفة',
                'field'  => 'job_title',
                'value'  => $member->job_title,
                'member'  => $member,
            ])
            
            
         


        </tbody>

    </table>
    
</div>