<tr>
    <td class="table-header">
        <strong>{{__($label)}}</strong>
    </td>
    <td>
        <a href="javascript:voide(0)" 
            id="{{$label}}" 
            class="editable" 
            data-type="select" 
            data-pk="{{$member->id}}" 
            data-url="{{route('user.members.update' , ['member' => $member , 'field' => $field])}}" 
            data-title="Enter {{$label}}" 
            data-source="{{json_encode($source)}}"
            data-value="{{$value}}"
            data-name="{{$field}}">
                    {{isset($text) ? $text : $value}}
        </a>
    </td>
</tr>