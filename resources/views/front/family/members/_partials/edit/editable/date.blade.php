<tr>
    <td class="table-header">
        <strong>{{__($label)}}</strong>
    </td>
    <td>
        <a href="javascript:voide(0)" 
            id="{{$label}}" 
            class="editable" 
            data-type="date" 
            data-pk="{{$member->id}}" 
            data-url="{{route('user.members.update' , ['member' => $member , 'field' => $field])}}" 
            data-title="Enter {{$label}}" 
            data-format="yyyy-mm-dd"
            data-name="{{$field}}">
                    {{$value}}
        </a>
    </td>
</tr>