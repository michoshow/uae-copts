<tr>
    <td class="table-header">
        <strong class="d-block">{{__($label)}}</strong>
    </td>
    <td>
        <a href="#" id="{{$label}}" class="editable" data-type="text" data-pk="{{$member->id}}" data-url="{{route('user.members.update' , ['member' => $member , 'field' => $field])}}" data-title="Enter {{$label}}" data-name="{{$field}}">{{$value}}</a>
    </td>
</tr>