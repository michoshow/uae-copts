<div role="tabpanel" class="tab-pane p-2" id="church"
    aria-labelledby="church" aria-expanded="true">
    
    <h4 class="card-title pb-2">{{__('Church')}}</h4>
    <table class="table table-striped mb-0">
        <tbody>
           
            @include('front.family.members._partials.edit.editable.select', [
                'label'   => 'Which church you attend the Liturgies in | أي كنيسة تحضر القداسات بها',
                'field'   => 'church_id',
                'value'   => $member->church->id,
                'member'  => $member,
                'source'  => Front::churchesList(),
                'text'    => $member->church->name  
            ])

            @include('front.family.members._partials.edit.editable.select', [
                'label'  => 'Father of Confession | أب الاعتراف',
                'field'  => 'father_of_confession',
                'value'  => $member->father_of_confession,
                'member' => $member,
                'source' => Front::fathersList(),
                'text'   => Front::fatherName($member->father_of_confession),
            ])


            @include('front.family.members._partials.edit.editable.text', [
                'label'  => 'If serving in Church | إن كنت خادم بالكنيسة',
                'field'  => 'servant_in',
                'value'  => $member->servant_in,
                'member'  => $member,
            ])

            
         


        </tbody>

    </table>
    
</div>