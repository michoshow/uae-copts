<div x-data="parentsHandler()">

    <template  x-for="(parent, index) in parents" :key="index">
        <div class="row">
          <div class="col-md-5 col-12">
            @include('backoffice.partials.forms.text' , [
           'label'     => 'Name',
           'name'      => 'parents[name][]',
           'value'     => '',
           'attr'      => 'x-model=parent.name',
           'class'     => false,
           'required'  => false,
           'disabled'  => false,
           'helper'    => "",
         ]) 
          </div>
          <div class="col-md-5 col-12">

            @include('backoffice.partials.forms.select' , [
                'label'     => 'Relation',
                'name'      => 'parents[relation][]',
                'attr'          => '',
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [],
                'data'          => [
                    'Father'        => 'Father',
                    'Mother'        => 'Mother',
                    'Father In Law' => 'Father In Law',
                    'Mother In Law' => 'Mother In Law',
                    'Other'         => 'Other',
                ],
                'required'      => false,
                'disabled'      => false,
                'placeholder'   => '',
            ])
          </div>
          
          <div class="col-md-2 col-12">
            <button type="button" class="btn btn-icon btn-icon rounded-circle btn-danger mt-2 waves-effect waves-light" @click="removeField(index)"><i class="feather icon-trash-2"></i></button>
          </div>
        </div>
      </template>
      <div class="row">
        <div class="col-12">
          <button type="button" class="btn btn-info pull-right float-right mb-3" @click="addNewField()">+ Add Parent</button>
          <div class="clear clearfix"></div>
        </div>
      </div>
    
    </div>

    <script>
        function parentsHandler() {
            return {
              parents: [],
            addNewField() {
                    this.parents.push({
                        name: '',
                        relation: ''
                    });
                },
                removeField(index) {
                    this.parents.splice(index, 1);
                }
            }
        }
    </script>