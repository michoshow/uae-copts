<h6>{{__('Church')}}</h6>
<fieldset>
    <div class="row">
        
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Which church you attend the Liturgies in | أي كنيسة تحضر القداسات بها',
                'name'          => 'church',
                'attr'          => '',
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [isset($member) ? $member->church->id : ''],
                'data'          => Front::churchesList(),
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Father of Confession | أب الاعتراف',
                'name'          => 'father_of_confession',
                'attr'          => 'onchange=app.toggleElement(this) data-conditions=father_of_confission_wrapper|eq|Other',
                'class'         => '',
                'multiple'      => false,
                'max'           => false,
                'value'         => [isset($member) ? $member->father_of_confession : ''],
                'data'          => Front::fathersList(),
                'required'      => true,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>

        <div class="col-md-6 col-12 hidden" id="father_of_confission_wrapper">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Father of Confession in Egypt | أب الاعتراف بمصر',
                'name'      => 'father_of_confession',
                'value'     => isset($member) ? $member->father_of_confession : '',
                'attr'      => '',
                'class'     => false,
                'required'  => true,
                'disabled'  => false,
                'helper'    => "",
            ])
        </div>
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'If serving in Church | إن كنت خادم بالكنيسة',
                'name'      => 'servant_in',
                'value'     => isset($member) ? $member->servant_in : '',
                'attr'      => '',
                'class'     => false,
                'required'  => false,
                'disabled'  => false,
                'helper'    => "If you're a servant at church, please type in your service. إذا كنت خادم بالكنيسة برجاء كتابة الخدمة",
            ])
        </div>

    </div>

    <div class="row">
       
    </div>
</fieldset>