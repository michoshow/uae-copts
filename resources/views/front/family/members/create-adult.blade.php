@extends('front.layout.layout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper">

<div class="content-body">
<!-- Description -->
<div class="row">
<div class="col-md-8 offset-md-2 col-12">
<section id="description" class="card">
<div class="card-header">
<h4 class="card-title">{{__('Add Family Member')}}</h4>
</div>
<div class="card-content">
<div class="card-body">
    <div class="card-text">
        
    <form action="{{route('user.members.add' , [
        'family' => $family
    ])}}" class="steps-validation wizard-circle" method="POST" enctype="multipart/form-data" id="createMember">
            @csrf
            @include('front.family.members._partials.personal-information')
            @include('front.family.members._partials.documentation')
             @include('front.family.members._partials.church')
            @include('front.family.members._partials.job-profession')
            @include('front.family.members._partials.emergency')
            {{-- @include('front.family.members._partials.parents') --}}

          
        </form> 
        
    </div>
</div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>
@endsection