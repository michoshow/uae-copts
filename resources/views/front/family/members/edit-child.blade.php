
@extends('front.layout.layout')

@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section id="page-account-settings">
                        <div class="row">
                          <!-- left menu section -->
                          <div class="col-md-3 mb-2 mb-md-0">
                            <div class="card">
                              <div class="card-content">
                                <div class="card-body">
                            
                            <div>
                            @include('backoffice.partials.forms.profile-photo' , [
                              'profile_image' => $member->avatar,
                              'editable'      => true
                            ])
                            </div> 

                            <ul class="nav nav-pills flex-column mt-md-0 mt-1">

                                <li class="nav-item active">
                                  <a class="nav-link d-flex py-75 active" id="account-pill-personal-child" data-toggle="pill"
                                  href="#personal-child" aria-expanded="true">
                                  <i class="feather icon-settings mr-50 font-medium-3"></i>
                                    {{__('Personal Information')}}
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link d-flex py-75" id="account-pill-sunday-school" data-toggle="pill"
                                  href="#sunday-school" aria-expanded="true">
                                  <i class="feather icon-settings mr-50 font-medium-3"></i>
                                    {{__('Sunday School')}}
                                  </a>
                                </li>
                            </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- right content section -->
                          <div class="col-md-9">
                            
                              <div class="form-body">
                                <div class="card">
                                  <div class="card-header">
                                    <h4 class="card-title pb-2">{{__('Edit')}} - {{$member->name}}</h4>
                                  </div>
                                  <div class="card-content">
                                    <div class="card-body">
                                       @if (session()->has('success'))
                                            <div class="alert alert-success">
                                               {{session('success')}}
                                            </div>
                                       @endif
                                        @if(count($errors))
                                            <div class="alert alert-danger">
                                                <h4 class="alert-heading">{{
                                                    __('Something went wrong!') }}</h4>
                                                <p>
                                                <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                      <div class="tab-content">
                                        
                                            @include('front.family.members._partials.edit.personal-information-child' , 
                                            [
                                                'family' => $family,
                                                'member' => $member
                                            ])

                                            @include('front.family.members._partials.edit.sunday-school' , 
                                            [
                                                'family' => $family,
                                                'member' => $member
                                            ])
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection