@if($liturgies->count())
    <div class="list-group list-group-flush">
        @foreach ($liturgies as $liturgy)

            <div class="list-group-item list-group-item-action mb-1">
                
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">
                        <a href="{{route('churches.show' , ['church' => $liturgy->church])}}">
                            {{__($liturgy->church->name)}} - {{__($liturgy->church->city)}}
                        </a>
                    </h5>
                </div>
                @if ($liturgy->title)
                    <p>
                        <strong>{{__($liturgy->title)}}</strong> 
                    </p>
                @endif
                @if ($liturgy->discription)
                    <p class="card-text">
                        {{__($liturgy->discription)}} 
                    </p>
                @endif

                <strong>
                    {{date( 'l jS F Y', strtotime($liturgy->date))}} 
                    <span class="d-block text-muted">{{date( 'g:i A', strtotime($liturgy->start))}} - {{date( 'g:i A', strtotime($liturgy->end))}} </span>
                </strong>

                <small class="text-muted right pull-right">{{$liturgy->seats}} Avialable {{ Str::plural('Seat', $liturgy->seats) }}</small>
                
                <div class="clear clearfix"></div>
                
                <div class="mt-1 float-right">
                    @if (BookingHelper::bookable($liturgy))
                    <a href="{{route('user.booking.create' , ['liturgy' => $liturgy])}}" class="btn btn-primary">{{__('Book')}} <i class="fa fa-angle-right"></i></a>
                    @else
                    <button class="btn btn-primary" disabled>{{__('Book')}} <i class="fa fa-angle-right"></i></button>
                    @endif
                </div>

                <div class="clear clearfix"></div>
                
            </div>
            
            @endforeach
        </div>
    @else
    <div class="row">
        <div class="col-12">
            <div class="alert alert-warning" role="alert">
                <h4 class="alert-heading">{{__('No Liturgies')}}</h4>
                    <p>
                        {{__('It seems that there are NO liturgies match your filters criteria!')}} <br/>
                        {{__('Please, search again using other filters')}}
                    </p>
            </div>
        </div>
    </div>
@endif