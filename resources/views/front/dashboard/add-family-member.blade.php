<div class="dropdown float-right">
    <button type="button" class="btn btn-success dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{__('Add Family Member')}}
    </button>
    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(189px, 38px, 0px);">

    <a class="dropdown-item" href="{{route('create.family.member' , [
        'family' => Auth::guard('user')->user()->family,
        'type'  => 'adult',
    ])}}">{{__('Adult')}}</a>
    
    <a class="dropdown-item" href="{{route('create.family.member' , [
        'family' => Auth::guard('user')->user()->family,
        'type'  => 'child',
    ])}}">{{__('Child')}}</a>
        
    </div>
</div>