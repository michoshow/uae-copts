<div class="card" style="border:1px solid gainsboro">
    <div class="card-header mb-1">
    <h4 class="card-title"><a href="{{route('churches.show' , ['church' => $church])}}">{{__($church->name)}} - {{__($church->city)}}</a></h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            @if ($liturgy->title)
            <p class="card-text">
                {{__($liturgy->title)}} 
            </p>
            @endif
            @if ($liturgy->discription)
            <p class="card-text">
                {{__($liturgy->discription)}} 
            </p>
            @endif

            <strong class="card-text">
                {{date( 'l jS F Y', strtotime($liturgy->date))}} 
                <span class="d-block text-muted">{{date( 'g:i A', strtotime($liturgy->start))}} - {{date( 'g:i A', strtotime($liturgy->end))}} </span>
            </strong>
        </div>
    </div>
    <div class="card-footer">
        <span class="float-left">{{$liturgy->allowedSeats}} Avialable {{ Str::plural('Seat', $liturgy->allowedSeats) }}</span>
        
        <span class="float-right">
                @if (BookingHelper::bookable($liturgy))
                    <a href="{{route('user.booking.create' , ['liturgy' => $liturgy])}}" class="btn btn-primary">{{__('Book')}} <i class="fa fa-angle-right"></i></a>
                @else
                <button class="btn btn-primary" disabled>{{__('Book')}} <i class="fa fa-angle-right"></i></button>
                @endif
            </span>
        
    </div>
</div>