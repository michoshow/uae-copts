<div class="media" id="{{$member->id}}">
    
    <a class="media-left" href="{{route('user.members.edit' , [
        'member'    => $member,
        'family'    => $family ,
        'type'      => ($member->type) ? $member->type : 'adult'
    ])}}">
        <img src="{{$member->avatar}}" alt="{{$member->first_name}} " width="50" height="50" class="media-object rounded-circle">
    </a>
    
    <div class="media-body">
        <h5 class="media-heading">
            <a class="media-left" href="{{route('user.members.edit' , [
            'member'    => $member,
            'family'    => $family ,
            'type'      => ($member->type) ? $member->type : 'adult'
        ])}}">
           {{$member->name}}
            </a>        
        </h5>
        <span class="text-muted">{{$member->family_role}}</span>
    </div>

    <div class="media-right">
        <a href="{{route('user.members.edit' , [
            'member'    => $member,
            'family'    => $family ,
            'type'      => ($member->type) ? $member->type : 'adult'
        ])}}" class="btn btn-icon btn-flat-success waves-effect waves-light d-inline"><span class="action-edit"><i class="feather icon-edit"></i></span></a>

        <a href="javascript:void(0)" data-route="{{route('user.members.destroy' , [
            'member'    => $member,
            'family'    => $family    
        ])}}" onclick="app.deleteItem(this)" class="btn btn-icon btn-flat-danger waves-effect waves-light d-inline"><i class="feather icon-trash"></i></a>  
    </div>
    

</div>