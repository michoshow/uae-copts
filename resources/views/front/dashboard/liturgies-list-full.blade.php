<div class="row">

    @if($liturgies->count())
        @foreach ($liturgies as $liturgy)
            <div class="col-md-6 col-12">
                @include('front.dashboard.liturgy' , [
                    'liturgy' => $liturgy,
                    'church' => $liturgy->church,
                ])
            </div>
        @endforeach
    @else
        <div class="col-12">
            <div class="alert alert-warning" role="alert">
                <h4 class="alert-heading">{{__('No Liturgies')}}</h4>
                    <p>
                        {{__('It seems that there are NO liturgies match your filters criteria!')}} <br/>
                        {{__('Please, search again using other filters')}}
                    </p>
            </div>
        </div>
    @endif
</div>