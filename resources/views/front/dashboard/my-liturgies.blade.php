<section id="description" class="card">
    <div class="card-header">
    <h4 class="card-title">{{__('My Coming Bookings')}}</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <div class="card-text">
                
                @if($family->bookings->count())
                    <div class="media-list media-bordered">
                        @foreach (BookingHelper::getBookingsList($family->bookings) as $booking)
                            @include('front.dashboard.liturgy-booking' , ['booking' => $booking])
                        @endforeach
                    </div>

                @else
                    <div class="alert alert-warning" role="alert">
                        <h4 class="alert-heading">{{__('No Coming Bookings')}}</h4>
                            <p>
                                {{__('It seems that you don\'t have any booking!')}} <br/>
                                {{__('You can book for yourself and your family members')}}
                            </p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>