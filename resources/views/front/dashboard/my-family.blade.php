<section id="description" class="card">
    <div class="card-header">
    <h4 class="card-title">{{__('My Family')}}</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <div class="card-text">

                    @if (!$user->family->members->count())
                        @include('front.partials.no-family-members')
                        @else
                        <div class="media-list media-bordered">
                            @foreach ($user->family->members as $member)
                            @include('front.dashboard.family-member' , [
                                'member' => $member,
                                'family' => $user->family   
                                ])
                            @endforeach
                        </div>
                        @include('front.dashboard.add-family-member') 
                        <div class="clear clearfix mb-1"></div>
                    @endif
                

            </div>
        </div>
    </div>
</section>