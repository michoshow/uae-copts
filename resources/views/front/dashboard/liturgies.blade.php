<section id="description" class="card" style="overflow: hidden">
    <div class="card-header">
        <h4 class="card-title">{{__('Liturgies')}}</h4>
    </div>
  
    <div class="row mt-2 mb-1 p-2" style="background-color:rgba(35, 58, 108, 0.1);">
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.select' , [
                'label'         => 'Filter By Church',
                'name'          => 'church_filter',
                'attr'          => 'onchange=app.filterLiturgies(this,\'full\') data-filter=church',
                'class'         => 'select2',
                'multiple'      => true,
                'max'           => false,
                'value'         => isset($filters['church']) ? $filters['church'] : [] ,
                'data'          => Front::churchesList($churches),
                'required'      => false,
                'disabled'      => false,
                'placeholder'   => '',
            ])
        </div>
        <div class="col-md-6 col-12">
            @include('backoffice.partials.forms.text' , [
                'label'     => 'Filter By Date',
                'name'      => 'date_filter',
                'value'     => isset($filters['date']) ? $filters['date'] : '',
                'attr'      => 'onchange=app.filterLiturgies(this,\'full\') data-filter=date',
                'class'     => 'date-range',
                'required'  => false,
                'disabled'  => false,
                'helper'    => false,
            ])
        </div>

    </div>

    <div class="card-content">
        <div class="card-body">
            <div class="card-text" id="liturgies-list">
                @if(isset($liturgiesHTML))
                    {!! $liturgiesHTML !!}
                @else
                <center>
                    <div class="spinner-grow" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </center>
                @endif
               {{--  @include('front.dashboard.liturgies-list') --}}
            </div>
        </div>
    </div>
</section>