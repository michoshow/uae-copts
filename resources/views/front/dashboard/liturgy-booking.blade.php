<div class="media" id="{{$booking->token}}">
    
    <div class="media-body">
        <h6 class="media-heading">
        <a href="{{route('user.booking.show' , [
            'liturgy' => $booking->liturgy,
            'token' => $booking->token,
            ])}}">
                {{$booking->liturgy->church->name}}
                @if ($booking->liturgy->title)
                    - {{$booking->liturgy->title}}
                @endif
           </a>
        </h6>
        <span class="text-muted">{{date( 'l jS F Y', strtotime($booking->date))}}<span class="d-block text-muted">{{date( 'g:i A', strtotime($booking->start))}} - {{date( 'g:i A', strtotime($booking->end))}}</span>
       
    </div>

    <div class="media-right">
        <a href="{{route('user.booking.show' , [
            'liturgy' => $booking->liturgy,
            'token' => $booking->token,
            ])}}" class="btn btn-icon btn-flat-success waves-effect waves-light d-inline"><span class="action-edit"><i class="feather icon-edit"></i></span></a>
    </div>
</div>