<section id="description" class="card">
    <div class="card-header">
    <h4 class="card-title">{{__('Churches')}}</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <div class="card-text">
                <ul class="list-group list-group-flush">
                @foreach ($churches as $church)
                    <li class="list-group-item">
                        <a href="{{route('churches.show' , ['church' => $church])}}">
                            {{$church->name}} - {{$church->emirate}}
                        </a>
                    </li>
                @endforeach
            </ul>
            </div>
        </div>
    </div>
</section>

