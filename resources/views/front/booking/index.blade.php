@extends('front.layout.layout')
@section('content')
<!-- BEGIN: Content-->
<div class="app-content content">
<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
    <div class="content-wrapper">

        <div class="content-body">
            <div class="row">
                <div class="col-md-8 col-12">
                    @if ($bookings->count())
                        
                        @include('front.booking.partials.my-bookings')

                    @else
                    
                    <div class="alert alert-warning" role="alert">
                        <h4 class="alert-heading">{{__('No Bookings')}}</h4>
                            <p>
                                {{__('It seems that you don\'t have any booking!')}} <br/>
                                {{__('You can book for yourself and your family members')}}
                            </p>
                    </div>

                    @endif
                
                </div>
                <div class="col-md-4 col-12">
                    @include('front.church.partials.liturgies'  , [
                        'church' => $family->church
                    ])
                    {{-- @include('front.dashboard.churches') --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection