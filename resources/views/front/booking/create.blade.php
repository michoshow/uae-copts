@extends('front.layout.layout')
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
            <!-- Description -->
                <div class="row">
                    <div class="col-md-8 offset-md-2 col-12">
                        <section id="description" class="card">
                        <div class="card-header">
                        <h4 class="card-title">{{__('Liturgy Booking')}}</h4>
                        </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="card-text">
                                        
                                        <div class="col-12">
                                            <div class="alert alert-info text-center">
                                                For attending the liturgy please dont forget to bring the Emirates IDs for all family members
                                                <br />
                                                لحضور القداسات ، لا تنسَ إحضار بطاقات الهوية الإماراتية لجميع أفراد الأسرة
                                            </div>
                                        </div>

                                        @if ($family->members->count() > $liturgy->allowedSeats)
                                            <div class="alert alert-danger">
                                                <h4 class="alert-heading">{{__('No enough seats')}}</h4>
                                                
                                                {{__('The available seats are less than your family members')}}
                                                
                                                <p>{{__('You can book')}}
                                                {{$liturgy->allowedSeats}} {{Str::plural('Seat', $liturgy->allowedSeats)}} {{__('Only') }} </p>
                                            </div>
                                        @endif

                                        @include('front.booking.partials.liturgy-details')
                                        
                                        
                                        @if ($family->members->count())
                                            
                                        

                                        <form class="form form-validate" novalidate method="POST" action="{{route('user.booking.store' , $liturgy)}}">
                                            @csrf

                                        @foreach ($family->members as $member)
                                            @include('front.booking.partials.member' , [
                                                'allowed' => BookingHelper::allowed($member, $liturgy)
                                            ])
                                        @endforeach

                                        <div class="row">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-outline-success mb-1 waves-effect waves-light pull-right">{{__('Book')}}</button>
                                            </div>
                                        </div>

                                        </form>

                                        @else

                                        @include('front.partials.no-family-members')
                                        
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection