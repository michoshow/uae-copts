<div class="alert alert-success my-2">
    <h4 class="alert-heading">{{ Session::get('success') }}</h4>
    <p>
        You successfully booked {{count($bookings)}} {{Str::plural('seat', count($bookings))}}, <br />
        You should receive a confirmation email shortly!
    </p>
</div>