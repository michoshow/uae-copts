<fieldset class="my-1">
<div class="vs-checkbox-con vs-checkbox-primary member-selection {{!($allowed) ? 'member-selection-disabled' : ''}}">

        <input type="checkbox" 
        name="bookers[]"  
        {{!($allowed) ? "disabled='disabled'" : "checked='checked'"}}
        value="{{$member->id}}">
        
        <span class="vs-checkbox vs-checkbox-lg">
            <span class="vs-checkbox--check">
                <i class="vs-icon feather icon-check"></i>
            </span>
        </span>
        
        <div>
            <strong class="d-block">{{$member->name}}</strong>
            @if ($lastBooking = $member->lastBooking)
                <span class="text-muted">{{$lastBooking->details}}</span>
            @endif
        </div>
    </div>
</fieldset>