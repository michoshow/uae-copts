<div class="visible-print text-center py-2" style="background-color:#f8f8f8">
    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate(route('public.user.booking.show' , [
        'liturgy'     => $liturgy,
        'token'       => $token,
    ]))); !!}">
    <p class="mt-1">
        {{__('Please, save this QR Code and show it to the church servants')}}
    </p>
    <div class="" style="width:100%">
        <a href="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate(route('public.user.booking.show' , [
            'liturgy'     => $liturgy,
            'token'       => $token,
        ]))); !!}" download="{{$token}}.png" class="btn btn-info mx-1">{{__('Download')}}</a>
        {{-- <a href="#" class="btn btn-primary mx-1">{{__('Send by email')}}</a> --}}
    <div>
</div>