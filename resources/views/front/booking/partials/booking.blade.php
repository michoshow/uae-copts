
<div class="booking-details d-flex justify-content-between" id="{{$booking->member_id}}">
    <strong>
        {{$booking->member->name}}
    </strong>

    <div>
        <a href="javascript:void(0)" data-route="{{route('user.booking.destroy' , [
            'booking'    => $booking,
            'liturgy'    => $liturgy,
            'member'     => $booking->member_id
        ])}}" onclick="app.deleteItem(this)" class="btn btn-icon btn-flat-danger waves-effect waves-light d-inline">
            <i class="feather icon-trash"></i>
        </a> 
    </div>
</div>
