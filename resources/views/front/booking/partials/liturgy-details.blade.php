<div class="my-1 liturgy-details">
    <h5>
        {{$liturgy->church->name}}
        @if ($liturgy->title)
            - {{$liturgy->title}}
        @endif
    </h5>
    <p class="text-muted">
        @if ($liturgy->discription)
            {{$liturgy->discription}}
        @endif
    </p>
    <strong>
        {{date( 'l jS F Y', strtotime($liturgy->date))}} 
            <span class="d-block text-muted">
                {{date( 'g:i A', strtotime($liturgy->start))}} - {{date( 'g:i A', strtotime($liturgy->end))}}
            </span>
    </strong>
</div>