<div class="my-1 liturgy-details">
    <h5>
        {{$liturgy->church->name}}
        @if ($liturgy->title)
            - {{$liturgy->title}}
        @endif
    </h5>
    <p class="text-muted">
        @if ($liturgy->discription)
            {{$liturgy->discription}}
        @endif
    </p>
    <p>
        <strong>
            {{date( 'l jS F Y', strtotime($liturgy->date))}} 
            <span class="d-block text-muted">
                {{date( 'g:i A', strtotime($liturgy->start))}} - {{date( 'g:i A', strtotime($liturgy->end))}}
            </span>
        </strong>
    </p>

    <p>
        <strong>
            {{__('Booked Seats')}} : {{count($bookings)}}
        </strong>
    </p>

    <div class="dropdown float-right">
        <button type="button" class="btn btn-info dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{__('Add Calendar')}}
        </button>
        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(189px, 38px, 0px);">
    
            <a class="dropdown-item" target="_blank" href="{{BookingHelper::calendarURL($liturgy ,'google')}}">{{__('Google Calendar')}}</a>
            
            <a class="dropdown-item" target="_blank" href="{{BookingHelper::calendarURL($liturgy,'office')}}">{{__('Office 365 Calendar')}}</a>

        </div>
    </div>

    <div class="clear clearfix my-1"></div>

</div>