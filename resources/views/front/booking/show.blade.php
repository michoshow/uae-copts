@extends('front.layout.layout')
@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
            <!-- Description -->
                <div class="row">
                    <div class="col-12 col-md-8 offset-md-2">
                        <section id="description" class="card">
                        <div class="card-header">
                        <h4 class="card-title">{{__('Liturgy Booking')}}</h4>
                        </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="card-text">
                                        <div class="col-12">
                                            <div class="alert alert-info text-center">
                                                For attending the liturgy please dont forget to bring the Emirates IDs for all family members
                                                <br />
                                                لحضور القداسات ، لا تنسَ إحضار بطاقات الهوية الإماراتية لجميع أفراد الأسرة
                                            </div>
                                        </div>
                                        
                                        @if (Session::has('success'))
                                            
                                            @include('front.booking.partials.success-message' , [
                                                'bookings' => $bookings
                                            ])
                                            
                                        @endif

                                        @include('front.booking.partials.booking-details')
                                        
                                        <div class="row">
                                            <div class="col-md-7 col-12">
                                                <h5 class="my-2">{{__("Booked For")}}</h5>
                                                @if(count($bookings))
                                                    @foreach ($bookings as $booking)
                                                        @include('front.booking.partials.booking' , [
                                                            'booking' => $booking
                                                        ])
                                                    @endforeach
                                                @endif
                                            </div>

                                            <div class="col-md-5 col-12">
                                               @include('front.booking.partials.qr-code' , [
                                                        'token' => $token
                                                    ])
                                            </div>

                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection