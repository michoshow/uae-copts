@extends('front.layout.mail.base')
@section('content')
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td style="padding: 0 2.5em; text-align: center; padding-bottom: 3em;">
    <div class="text">
    <h2>{{__("Your Booking Confirmation")}}</h2>
    </div>
    </td>
    </tr>
    <tr>
    <td style="text-align: center;">
    <div class="text-author">

    <img src="{{$qrCode}}" alt="" style="width: 200px; max-width: 600px; height: auto; margin: auto; display: block;">
    
    <div class="alert alert-info text-center">
        For attending the liturgy please dont forget to bring the Emirates IDs for all family members
        <br />
        لحضور القداسات ، لا تنسَ إحضار بطاقات الهوية الإماراتية لجميع أفراد الأسرة
    </div>
    
    
    <h2>
        {{$liturgy->church->name}}
        @if ($liturgy->title)
            - {{$liturgy->title}}
        @endif
    </h2>
    <p class="text-muted">
        @if ($liturgy->discription)
            {{$liturgy->discription}}
        @endif
    </p>
    <p>
        <strong>
            {{date( 'l jS F Y', strtotime($liturgy->date))}} 
            <span class="d-block text-muted">
                {{date( 'g:i A', strtotime($liturgy->start))}} - {{date( 'g:i A', strtotime($liturgy->end))}}
            </span>
        </strong>
    </p>
    
    <p>
        <strong>
            {{__('Booked Seats')}} : {{count($bookings)}}
        </strong>
    </p>
    
    <p>
        <a href="{{route('user.booking.show' , [
            'liturgy' => $liturgy,
            'token' => $token,
        ])}}" class="btn btn-primary">
            {{__('Edit Yor Booking')}}
        </a>
    </p>
    
    </div>
    </td>
    </tr>
    </table>

    @endsection