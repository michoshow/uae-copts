@extends('front.layout.layout')

@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
           
            <div class="content-body">
                <div class="row">
                    <div class="col-md-8 col-12">
                       
                        <section id="description" class="card" style="overflow: hidden">
                            <div class="card-header">
                            <h4 class="card-title">{{__($church->name)}} - {{$church->emirate}}</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="card-text">
                                        <div class="row mb-1 " >
                                            @include('front.church.partials.map')
                                            @include('front.church.partials.details')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        

                        
                    </div>
                    <div class="col-md-4 col-12">
                        @include('front.church.partials.liturgies')

                        @include('front.dashboard.churches')
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
@endsection