<div class="col-12">
    <div class="accordion" id="accordion_comm_details" data-toggle-hover="true">
        <div class="collapse-margin">
            <div class="card-header collapsed" id="headingOne" data-toggle="collapse" role="button" data-target="#collapse_comm_details" aria-expanded="false" aria-controls="collapse_comm_details">
                <span class="lead collapse-title">
                    {{__('Communication Details')}}
                </span>
            </div>
    
            <div id="collapse_comm_details" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_comm_details" style="">
                <div class="card-body">
                    
                    <div class="list-group list-group-flush">
                        
                        @foreach ($church->contacts['communication'] as $key=>$value)

                        @switch($key)
                            @case('email')
                            
                                <a class="list-group-item list-group-item-action" href="mailto:{{$value}}" class=""><p class="float-left mb-0">
                                    <i class="feather icon-mail mr-1"></i>
                                </p>{{$value}}</a>
                                @break
                            @case('facebook')
                            
                                <a class="list-group-item list-group-item-action" target="_blank" href="{{$value}}" class=""><p class="float-left mb-0">
                                    <i class="feather icon-facebook mr-1"></i>
                                </p>Facebook Page</a>
                                
                                @break
                                
                            @case('website')
                                
                                <a class="list-group-item list-group-item-action" target="_blank" href="{{$value}}" class=""><p class="float-left mb-0">
                                    <i class="feather icon-globe mr-1"></i>
                                </p>Website</a>

                                @break

                            @default
                                
                        @endswitch
                    
                        
                    @endforeach

                       
                    </div>

                   
                    
                </div>
            </div>
        </div>
    </div>

    
    <div class="accordion" id="accordion_support_details" data-toggle-hover="true">
        <div class="collapse-margin">
            <div class="card-header collapsed" id="headingOne" data-toggle="collapse" role="button" data-target="#collapse_support_details" aria-expanded="false" aria-controls="collapse_support_details">
                <span class="lead collapse-title">
                    {{__('Support')}}
                </span>
            </div>
    
            <div id="collapse_support_details" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_support_details" style="">
                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                <th scope="col">{{__('Name')}}</th>
                                <th scope="col">{{__('Phone')}}</th>
                                <th scope="col">{{__('Email')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($church->contacts['support'] as $support)
                                    
                                @endforeach
                                <tr>
                                    <td>{{$support['name']}}</td>
                                    <td><a href="tel:{{$support['phone']}}">{{$support['phone']}}</a></td>
                                    <td><a href="mailto:{{$support['email']}}">{{$support['email']}}</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>