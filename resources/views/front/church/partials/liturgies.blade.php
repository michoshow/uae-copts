<section id="description" class="card">
    <div class="card-header">
        <h4 class="card-title">{{__('Liturgies')}}</h4>
    </div>
    <div class="card-content p-0">
        <div class="card-body">
            <div class="card-text">
                <div class="row mb-1 p-2" style="background-color:rgba(35, 58, 108, 0.1);">
                    <div class="col-12">
                        @include('backoffice.partials.forms.text' , [
                            'label'     => 'Filter By Date',
                            'name'      => 'date_filter',
                            'value'     => '',
                            'attr'      => 'onchange=app.filterLiturgies(this,\'sidebar\') data-filter=date',
                            'class'     => 'date-range',
                            'required'  => false,
                            'disabled'  => false,
                            'helper'    => false,
                        ])
                    </div>

                    <input type="hidden" value="{{$church->id}}" data-filter="church">

                </div>
                <div id="liturgies-list">
                    <center>
                        <div class="spinner-grow" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </center>
                </div>              
            </div>
        </div>
    </div>
</section>