<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('uid');
            $table->unsignedBigInteger('church_id')->index();
            
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->string('area')->nullable();
            $table->string('city')->nullable();
            $table->string('emirate')->nullable();
            $table->string('country')->default('UAE');
            $table->string('landmarks')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->longText('emergerncy_contacts_in_country')->nullable();
            $table->longText('emergerncy_contacts_out_country')->nullable();
            $table->string('status')->default('Draft');
            $table->timestamps();

            $table->foreign('church_id')
                ->references('id')
                ->on('churches')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
