<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('token')->unique();
            $table->unsignedBigInteger('liturgy_id')->index();
            $table->unsignedBigInteger('church_id')->index();
            $table->unsignedBigInteger('family_id')->index();
            $table->unsignedBigInteger('member_id')->index();
            

            $table->boolean('confirmed')->default(false);
            $table->boolean('attended')->default(false);
            $table->boolean('canceled')->default(false);
            
            $table->date('date');
            $table->time('start');
            $table->time('end');

            $table->timestamps();

            $table->foreign('church_id')
            ->references('id')
            ->on('churches')
            ->onDelete('cascade');

            $table->foreign('liturgy_id')
            ->references('id')
            ->on('liturgies')
            ->onDelete('cascade');

            $table->foreign('member_id')
            ->references('id')
            ->on('members')
            ->onDelete('cascade');

            $table->foreign('family_id')
            ->references('id')
            ->on('families')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
