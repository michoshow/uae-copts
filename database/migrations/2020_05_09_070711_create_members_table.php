<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('uid');

            $table->unsignedBigInteger('family_uid')->index();
            $table->unsignedBigInteger('church_id')->index();

            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('additional_name')->nullable();
            $table->string('full_name')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->enum('gender', ['Male' , 'Female'])->nullable();
            
            $table->string('email')->nullable();


            $table->text('phone1')->nullable();
            $table->text('phone2')->nullable();

            $table->string('whatsapp_number')->nullable();
            $table->string('whatsapp_group_number')->nullable();

            $table->string('marital_status')->nullable();
            $table->enum('family_role' , ['Husband', 'Wife' , 'Child' , 'Brother', 'Sister', 'Relative' , 'Single'])->nullable();
            $table->dateTime('marriage_date')->nullable();

            $table->longText('uae_status')->nullable();
            $table->longText('egypt_status')->nullable();
            
            $table->boolean('deacon')->nullable();
            $table->string('deacon_degree')->nullable();
            $table->boolean('deacon_in_aue')->nullable();
            
            $table->text('servant_in')->nullable();
            $table->text('served_by')->nullable();
            $table->text('father_of_confession')->nullable();
            
            $table->boolean('regsiterd_in_youth_meeting')->nullable();

            $table->longText('sunday_school_details')->nullable();


            
            $table->string('school')->nullable();
            $table->string('qualification')->nullable();
            $table->string('qualification_description')->nullable();
            $table->text('specialization')->nullable();
            $table->text('education_details')->nullable();
            
            
            $table->string('company')->nullable();
            $table->string('domain')->nullable();
            $table->string('occupation')->nullable();
            $table->string('job_title')->nullable();
            
            $table->text('talents_hobbies')->nullable();

            
            $table->text('previous_services')->nullable();
            $table->text('services_want_to_join')->nullable();

            $table->text('phone_refers')->nullable();
            

            $table->string('status')->nullable();
            $table->string('edit_status')->nullable();

            $table->text('properties')->nullable();
            $table->timestamps();

            /* $table->foreign('family_uid')
                ->references('uid')
                ->on('families')
                ->onDelete('cascade') */;

            $table->foreign('church_id')
                ->references('id')
                ->on('churches')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
