<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLiturgiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liturgies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('church_id')->index();
            $table->boolean('active')->default(true);
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->date('date');
            $table->time('start');
            $table->time('end');
            $table->integer('seats');
            $table->timestamps();

            $table->foreign('church_id')
            ->references('id')
            ->on('churches')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liturgies');
    }
}
