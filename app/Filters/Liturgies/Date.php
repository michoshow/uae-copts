<?php
namespace App\Filters\Liturgies;

use App\Models\Liturgy;
use App\Filters\FiltersAbstrcat;

class Date extends FiltersAbstrcat
{
    public static function filter($liturgies , $filter){
        $dates = explode(" - " , $filter['value']);
        return  $liturgies->whereBetween("date" ,$dates)
        ;
    }

}