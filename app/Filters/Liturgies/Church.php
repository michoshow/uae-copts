<?php
namespace App\Filters\Liturgies;

use App\Filters\FiltersAbstrcat;

class Church extends FiltersAbstrcat
{
    public static function filter($liturgies , $filter){

        if(is_array($filter['value'])){
            return  $liturgies->whereIn("church_id" ,$filter['value']);
        }else{
            return  $liturgies->where("church_id",$filter['value']);
        }
    }

}