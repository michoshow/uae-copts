<?php

namespace App\Events;



use App\Models\Family;
use App\Models\Liturgy;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class Booked
{
    use Dispatchable, SerializesModels;


    public $bookings;
    public $token;
    public $liturgy;
    public $family;
    public $qrCode;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($bookings, $token, Liturgy $liturgy, Family $family, $qrCode)
    {
        $this->bookings = $bookings;
        $this->token    = $token;
        $this->liturgy  = $liturgy;
        $this->family  = $family;
        $this->qrCode  = $qrCode;
        
    }
   
}
