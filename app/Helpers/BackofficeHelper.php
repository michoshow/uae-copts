<?php 
namespace App\Helpers;

class BackofficeHelper
{
    public static function seatsStats($total , $rest){

        if($total == 0){
            return [
                'percentage' => 0,
                'color' => 'primary',
            ];
        }
        
        switch ($percentage = (($rest/$total) * 100)) {
            case $percentage > 70:
                $color = 'success';
                break;
            

            case $percentage > 50 && $percentage < 70:
                $color = 'warning';
                break;
            
            case $percentage <= 50:
                $color = 'danger';
                break;
            
            default:
                 $color = 'primary';
                break;
        };
        
        return [
            'percentage' => $percentage,
            'color' => $color,
        ];

    }
}
?>