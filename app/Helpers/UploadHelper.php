<?php
namespace App\Helpers;

use QrCode;
use App\Models\Liturgy;
use Storage;

class UploadHelper
{

    public static function upload($file , $path = null){
        if(!$path){
            $path = 'personal';
        }
        return Storage::put($path, $file);
    }

    
    public static function move($old , $folder , $new){

        Storage::makeDirectory($folder);
       
        return Storage::move( $old, $new);
    }

    public static function deleteFiles($files){
        
        return Storage::delete( $files);
        
    }

    
    public static function uploadQRCode(Liturgy $liturgy , $token){

        $base64_image = "data:image/jpeg;base64," .  base64_encode(QrCode::format('png')->size(300)->generate(route('public.user.booking.show' , [
            'liturgy'     => $liturgy,
            'token'       => $token,
        ])));

        if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
            $data = substr($base64_image, strpos($base64_image, ',') + 1);
            $data = base64_decode($data);

            if(Storage::put( 'qr-codes/' .  $liturgy->id  .'/' .$token.".png", $data)){
                return env('FTP_ROOT_URL') . "/qr-codes/$liturgy->id/$token.png";
            }
        }

    }

}