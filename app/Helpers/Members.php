<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
class Members
{
    public static function profileSections()
    {
        return [
            'personal' => [
                'title' => 'Personal Information',
                'icon'  => 'feather icon-user'
            ],
            
            'communications' => [
                'title' => 'Communication Details',
                'icon'  => 'feather icon-phone'
            ],
            
            'family' => [
                'title' => 'Family Details',
                'icon'  => 'feather icon-users'
            ],
            
            'uae' => [
                'title' => 'Status in UAE',
                'icon'  => 'feather icon-sun'
            ],

            'egypt' => [
                'title' => 'Status in Egypt',
                'icon'  => 'feather icon-sun'
            ],

            'church' => [
                'title' => 'Ecclesiastical Data',
                'icon'  => 'feather icon-plus'
            ],

            'sunday-school' => [
                'title' => 'Sunday School Data',
                'icon'  => 'feather icon-plus'
            ],
            'education' => [
                'title' => 'Education Details',
                'icon'  => 'feather icon-globe'
            ],

            'work' => [
                'title' => 'Work Details',
                'icon'  => 'feather icon-globe'
            ],

            'talent' => [
                'title' => 'Hobbies and Talents',
                'icon'  => 'feather icon-heart'
            ],

            'service' => [
                'title' => 'Service Details',
                'icon'  => 'feather icon-heart'
            ],

        ];
    }
}