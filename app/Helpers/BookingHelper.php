<?php 
namespace App\Helpers;

class BookingHelper
{

    public static function allowed($member , $liturgy)
    {
        // Age Check
        if(!self::ageCheck($member)){
            return false;
        }
        
        // The Liturgy is Bookable
        if(!self::bookable($liturgy)){
            return false;
        }
        
        // The member has No booking
        if(!$last = $member->bookings->last()){
            // First Booking Yeay!!
            return true;
        }

        return false;
    }

    protected static function ageCheck($member){
     
        $date = new \DateTime($member->date_of_birth);
        $now = new \DateTime();
        $interval = $now->diff($date);
        if($interval->y <  env('MIN_AGE')){
            return false;
        }
        return true;
    }
    
    public static function bookable($liturgy)
    {   

        if($liturgy->allowedSeats == 0){
            return false;
        }
        $liturgyDateTime = strtotime($liturgy->date . ' ' . $liturgy->start);
        $now = strtotime(date('Y-m-d h:i:s')); 
        
        if( $now > $liturgyDateTime ){
            return false;
        }

        return true;

    }


    public static function getBookingsList($bookings = null){
        $list = [];
        if(!$bookings){
            return $list;
        }
        
        foreach ($bookings as $booking) {
            $list[$booking->token] = $booking;
        }

        return $list;
    }

    public static function calendarURL($liturgy ,$platform = 'google'){
        $title = $liturgy->church->name;
        if($liturgy->title){
            $title .= ' - ' . $liturgy->title;
        }else{
            $title .= ' - Liturgy';
            
        }
        if($liturgy->description){
            $desc = $liturgy->description;
        }else{
            $desc = "Liturgy Booking";
        }

        $location = $liturgy->church->fullAddress;
        $startDate = $liturgy->date .' ' . $liturgy->start;
        $startDate =  date("Ymd\THis\Z", strtotime($startDate));
        
        $endDate = $liturgy->date .' ' . $liturgy->end;
        $endDate =  date("Ymd\THis\Z", strtotime($endDate));
        
        $timZone = urlencode('Asia/Dubai');
      
        
        switch ($platform) {
            case 'google':
                return "https://www.google.com/calendar/render?action=TEMPLATE&dates=$startDate/$endDate&location=$location&details=$desc&text=$title&ctz=$timZone#addcalendarlink";
                break;
            
            case 'office':
                return "https://outlook.live.com/owa/?path=/calendar/action/compose&rru=addevent&startdt=$startDate&enddt=$endDate&subject=$title&location=$location&allday=false&body=";
                break;
            
        }
      
    }
}