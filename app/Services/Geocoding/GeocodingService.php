<?php
namespace App\Services\Geocoding;
use  OpenCage\Geocoder\Geocoder;
class GeocodingService
{
    protected $geocoder;
    protected $lang;
    protected $countryCode;

    public function __construct($apiKey , $lang , $countryCode){
       $this->geocoder = new Geocoder($apiKey);
       $this->lang = $lang;
       $this->countryCode = $countryCode;
    }

    public function get($details){
        
        try {

            $result = $this->geocoder->geocode( $details['address'] .', ' . $details['area'] . ' ' . $details['city'] . ' ' . $details['emirate'] ,  ['language' => $this->lang , 'countrycode' => $this->countryCode] );
            
            if(count($result['results'])){
                return [
                    'lat' => $result['results'][0]['geometry']['lat'],
                    'long' => $result['results'][0]['geometry']['lng'],
                ];
            }else{
                return false;
            }            
        } catch (\Exception $th) {
            abort(500);
        }


        
    }
}
?>