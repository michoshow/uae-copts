<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Geocoding\GeocodingService;

class GeocodingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('geocoding' , function(){
            $api = env('OPENCAGEDATA_API');
            $lang = env('OPENCAGEDATA_LANGUAGE');
            $countryCode = env('OPENCAGEDATA_COUNTRY');
            return new GeocodingService($api, $lang , $countryCode);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
