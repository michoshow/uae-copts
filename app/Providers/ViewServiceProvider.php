<?php

namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;
use App\Http\View\Composer\AdminComposer;
use App\Http\View\Composer\LiturgiesComposer;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         // Using class based composers...
         View::composer(
            'panels.navbar', AdminComposer::class
        );

        View::composer(
            'front.dashboard.liturgies', LiturgiesComposer::class
        );
        
    }
}
