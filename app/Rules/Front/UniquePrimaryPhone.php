<?php

namespace App\Rules\Front;

use App\Models\Family;
use App\Models\Member;
use Illuminate\Contracts\Validation\Rule;

class UniquePrimaryPhone implements Rule
{
    protected $family;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($family = null)
    {
        $this->family = $family;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $value = str_replace(["-" , " " , "(" , ")"] , "" , $value);

        if($family = Family::where('phone' , $value )->first()){

            // Check if phone is used in the same family
            if($this->family && ($this->family->id == $family->id))
            {
                return true;
            }
            
            return false;
        }

        if($member = Member::where('phone1' , $value )->orWhere('phone2' , $value )->first()){
           
            if($this->family && $this->family->members->contains($member)){
               return true;
            } 

           return false;
        }

        return true;
      
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The primary phone is already used for another family';
    }
}
