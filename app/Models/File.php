<?php

namespace App\Models;

use App\Models\Member;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['original_name' , 'path'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
