<?php

namespace App\Models;

use App\Models\File;
use App\Models\Church;
use App\Models\Family;
use App\Models\Booking;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ["uid","first_name","middle_name","last_name","additional_name","full_name","date_of_birth","gender","phone1","phone2","whatsapp_number","whatsapp_group_number","marital_status","family_role","marriage_date","uae_status","egypt_status","deacon","deacon_degree","deacon_in_aue","servant_in","served_by","father_of_confession","regsiterd_in_youth_meeting","sunday_school_details","school","qualification","qualification_description","specialization","education_details","company","domain","occupation","job_title","talents_hobbies","previous_services","services_want_to_join","phone_refers","status","edit_status","properties","id_number_1","id_number_2","id_number_3" , "type" , "nationality" , "profile_image"];


    protected $casts = [
        /* "phone1"    => 'array',
        "phone2"    => 'array', */
        "uae_status"    => 'array',
        "egypt_status"  => 'array',
        "servant_in"    => 'array',
        "served_by" => 'array',
        "sunday_school_details" => 'array',
        "talents_hobbies"   => 'array',
        "previous_services" => 'array',
        "services_want_to_join" => 'array',
        "phone_refers"  => 'array',
    ];

    public function getRouteKeyName()
    {
        return 'uid';
    }

    public function getNameAttribute(){
        return $this->first_name . ' ' . $this->middle_name;
    }

    public function getAvatarAttribute(){

        if($this->profile_image !== null){
            return $this->profile_image;
        }
        $hash = md5(strtolower(trim($this->email)));
        return "http://www.gravatar.com/avatar/$hash?d=mp";
    }

    public function family(){
        return $this->belongsTo(Family::class , 'family_uid' , 'uid');
    }

    public function church(){
        return $this->belongsTo(Church::class , 'church_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
   
    public function getLastBookingAttribute()
    {
        return $this->bookings()->orderBy('id' , 'DESC')->first();
    }


    public function files()
    {
        return $this->hasMany(File::class);
    }
}
