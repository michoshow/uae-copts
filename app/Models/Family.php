<?php

namespace App\Models;

use App\Models\User;
use App\Models\Church;
use App\Models\Member;
use App\Models\Booking;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Family extends Model
{
    
    use SearchableTrait;

    protected $fillable = ["uid","email","phone","address","area","lat","long","status" , "emergerncy_contacts_in_country" , "emergerncy_contacts_out_country" , "landmarks" , "city", "emirate" , "country"];

    protected $casts = [
       'emergerncy_contacts_in_country'     => 'array',
       'emergerncy_contacts_out_country'    => 'array',
       'landmarks'                          => 'array',
    ];

    //protected $primaryKey = 'uid';

    public function getRouteKeyName()
    {
        return 'uid';
    }

   /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'families.phone'   => 10,
            'families.email'   => 10,
            'families.address' => 10,
            'families.area'    => 10,
            'families.uid'    => 10,
            'families.lat'     => 5,
            'families.long'    => 5,
        ]
    ];

    public function members(){
        return $this->hasMany(Member::class , 'family_uid' , 'uid');
    }
    public function bookings(){
        return $this->hasMany(Booking::class);
    }

    public function church(){
        return $this->belongsTo(Church::class , 'church_id' , 'id');
    }

    public function user(){
        return $this->belongsTo(User::class , 'user_id' , 'id');
    }
}
