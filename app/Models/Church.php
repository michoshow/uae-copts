<?php

namespace App\Models;

use App\Models\Family;
use App\Models\Member;
use Illuminate\Database\Eloquent\Model;

class Church extends Model
{
    protected $fillable = ["name","address","area","city","emirate","country" , "contacts"];

    protected $casts = [
        'contacts' => 'array'
    ];

    public $timestamps = false;

    public function getFullAddressAttribute(){
        return $this->address .',' . $this->area . ' ' . $this->emirate . ' ' . $this->country;
    }
    public function families(){
        return $this->hasMany(Family::class);
    }

    public function members(){
        return $this->hasMany(Member::class);
    }

    public function liturgies()
    {
        return $this->hasMany(Liturgy::class);
    }

}
