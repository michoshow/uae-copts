<?php

namespace App\Models;

use App\Models\Church;
use App\Models\Family;
use App\Models\Member;
use App\Models\Liturgy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Booking extends Model
{
    //
    public function scopeComing($query){
        return $query->where('date' , '>=' , date('Y-m-d'));
    }
    public function scopeArchived($query){
        return $query->where('date' , '<' , date('Y-m-d'));
    }

    public function family()
    {
        return $this->belongsTo(Family::class);
    }
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function liturgy()
    {
        return $this->belongsTo(Liturgy::class);
    }

    public function church()
    {
        return $this->belongsTo(Church::class);
    }

    public function getDetailsAttribute(){
        return $this->church->name . ' - ' . date( 'l jS F Y', strtotime($this->date)) ;
    }


}
