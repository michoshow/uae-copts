<?php

namespace App\Models;

use App\Models\Church;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Liturgy extends Model
{
    
    public function church(){
       return $this->belongsTo(Church::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function scopeComing($query){
        return $query->where('date' , '>=' , date('Y-m-d'));
    }
    
    public function scopeArchived($query){
        return $query->where('date' , '<' , date('Y-m-d'));
    }

    // Module model
    public function bookingsCount()
    {
        return $this->bookings()->selectRaw('count(id) as count');
    }
    
    public function scopeIsActive(Builder $builder){
        return $builder->where('active' , true);
    }
    
    public function getAllowedSeatsAttribute(){
        return $this->seats - $this->bookingsCount()->count();
    }

    public function getDetailsAttribute(){
        return $this->church->name . ' - ' . $this->title ;
    }


}
