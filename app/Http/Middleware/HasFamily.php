<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class HasFamily
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('user')->user()->family){
            return redirect(route('create.user.family' , Auth::guard('user')->user()));
        }

        if(!Auth::guard('user')->user()->family->members->count()){

            return redirect(route('create.family.member' , [
                'user'      => Auth::guard('user')->user(),
                'family'    => Auth::guard('user')->user()->family,
                'type'      => 'adult'
            ]));

        }



        return $next($request);
    }
}
