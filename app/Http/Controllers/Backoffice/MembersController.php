<?php

namespace App\Http\Controllers\Backoffice;

use Response;
use App\Models\Family;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $family = Family::where('uid' , $request->family_uid)->firstOrFail();

        $data = $request->all();
        $data['uid'] = random_int(9999,99999);
        $member = New Member();
        $member->fill($data);
        $member->family()->associate($request->family_uid);
        $member->church()->associate($request->church_id);
        if($member->save()){
            return redirect(route('families.edit' , $family ));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $id = $member->id;
        $name = $member->full_name;

        try{
            if( $member->delete() ){
                return Response::json([
                    'status'    => 'ok',
                    'deleted'   => $id,
                    'message'   => "$name Deleted Successfully"
                    ] , 200);
            }
        } catch (\Exception $e) {
            return abort("FAILED",500);
        }
    }
    
}
