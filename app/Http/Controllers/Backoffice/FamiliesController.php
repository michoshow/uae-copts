<?php

namespace App\Http\Controllers\Backoffice;

use Crypt;
use Response;
use Geocoding;
use App\Models\Family;
use App\Helpers\Members;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Http\Resources\Backoffice\FamiliesResource;

class FamiliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];
        return view('backoffice.families.index' , compact('pageConfigs'));
    }
    
    public function getFamiliesData(){
        
        $families = Family::with('members');

        return Datatables::of($families)

        ->addColumn('action' , function($family){
            $model = $family;
            $modelName = 'families';
            return view('backoffice.defaults.datatable.actions' , compact('model' , 'modelName'))->render();
        })
        ->editColumn('uid' , function($family){
            return view('backoffice.families.datatable.uid' , compact('family'))->render();
        })
        ->addColumn('members', function($family){
            $members = $family->members;
            return view('backoffice.families.datatable.members' , compact('members'))->render();
        })
        ->addColumn('phone', function($family){
            return view('backoffice.families.datatable.phone' , compact('family'))->render();
        })
        ->addColumn('family' , function($family){
            return $family->toArray();
        })
        ->setRowId('id')
        ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];
        return view('backoffice.families.create' , compact('pageConfigs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'address'   => 'required',
            'area'      => 'required',
        ]);
        $family = new Family();
        $family->address = $request->address;
        $family->area = $request->area;
        $family->city = $request->city;
        $family->emirate = $request->emirate;
        $family->landmarks = explode("," , $request->landmarks);
        $family->lat = $request->lat;
        $family->long = $request->long;
        $family->uid = random_int(9999,99999);

        $family->church()->associate($request->church);
        $family->save();
      
        return redirect( route('families.edit' , $family->uid) );
    }

    /**
     * Family's Add members page
     */
    public function addMembers(Family $family)
    {
        $family->load('members');
        $pageConfigs = [
            'pageHeader' => false
        ];
        
        $sections = Members::profileSections();
        
        return view('backoffice.families.members.add' , compact('family' , 'pageConfigs' , 'sections') );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Family $family)
    {
        $family->load('members');
        $pageConfigs = [
            'pageHeader' => false
        ];
        return view('backoffice.families.edit' , compact('family' , 'pageConfigs') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family)
    {
        $id = $family->id;
        $uid = $family->uid;

        try{
            if( $family->delete() ){
                return Response::json([
                    'status'    => 'ok',
                    'deleted'   => $id,
                    'message'   => "Family #$uid Deleted Successfully"
                    ] , 200);
            }
        } catch (\Exception $e) {
            return abort("FAILED",500);
        }
    }
    
    public function import()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];
        return view('backoffice.families.import' , compact('pageConfigs'));
    }

    public function upload(Request $request)
    {
        $path = $request->file('data_file')->getRealPath();
        $data = array_map('str_getcsv', file($path));

        if($request->data_type == 'families'){
            $this->uploadFamilies($data);
        }

    }

    protected function uploadFamilies($families){

        $map = [
            0 => 'uid',
            2 => 'street',
            3 => 'area',
            4 => 'lat', 
            5 => 'long', 
            7 => 'status', 
        ];
        foreach($families as $family ){
            if(is_numeric($family[0])){
                $data = [
                    'uid'       => $family[0],
                    'street'    => $family[2],
                    'area'      => $family[3],
                    'lat'       => $family[4],
                    'long'      => $family[5],
                    'status'    => $family[7],
                ];
                Family::create($data);
            }
        }
    }

    /** Retrive Coordinates from address */
    public function retriveCoordinates(Request $request){
        try {
            $coordinates = Geocoding::get($request->only('address' , 'area' , 'city' , 'emirate'));
            if($coordinates){
                return Response::json([
                    'status'        => 'ok',
                    'coordinates'   => $coordinates
                    ] , 200);
            }else{
                return Response::json([
                    'status'        => 'Not Found',
                    'coordinates'   => null
                ] , 404);
            }

        } catch (\Exception $th) {
            abort(500);        
        }
    }

    public function checkProprty(Request $request){
        
        $family = Family::where( $request->prop , 'like' , '%' . $request->value . '%' )->first();

        if($family){
            return Response::json([
                'status'        => 'ok',
                'family'        => $family->id,
                'url'           => route('families.edit' , $family),
                ] , 200);
        }
       
    }
}
