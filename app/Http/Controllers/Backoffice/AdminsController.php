<?php

namespace App\Http\Controllers\Backoffice;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Yajra\Datatables\Datatables;


class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.settings.administrators.index');
    }

    public function getAdminsData()
    {
        $admins = Admin::with('roles');
        return Datatables::of($admins)

        ->editColumn('name' , function($admin){
            return view('backoffice.settings.administrators.datatable.name' , compact('admin'))->render();
        })
        ->editColumn('active' , function ($admin) {
            return view('backoffice.settings.administrators.datatable.active' , compact('admin'))->render();
        })
        ->addColumn('role' , function ($admin) {
            $roles = $admin->roles->pluck('name');
            return view('backoffice.settings.administrators.datatable.roles' , compact('roles'))->render();
        })
        ->addColumn('action' ,function($admin){
            $model = $admin;
            $modelName = 'admins';
            return view('backoffice.defaults.datatable.actions' , compact('model' , 'modelName'))->render();
        })
        ->make(true);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('backoffice.settings.administrators.edit' , compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {

        $request->validate([
            'name'      => 'required|string',
            'email'     => 'required|email'
        ]);

        
        
        $data = $request->only(['name' , 'email', 'active']);
        
        if(!$request->has('active')){
            $data['active'] = 0;
        }

        if($request->password){
            $data['password'] = Hash::make($request->password);
        }
        
        $admin->update($data);

        if($request->role && !empty($request->role)){
            $admin->syncRoles($request->role);
        }
        return redirect( route('admins.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
