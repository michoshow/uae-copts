<?php

namespace App\Http\Controllers\Backoffice;

use App\Models\Church;
use App\Models\Liturgy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Response;


class LiturgiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->filter;
        if($filter == 'archived'){
            $pageTitle = 'Archived Liturgies';
        }else{
            $pageTitle = 'Coming Liturgies';
        }
        return view('backoffice.liturgies.index' , compact('filter' , 'pageTitle'));
    }


    public function getLiturgiesData($filter=null)
    {
        if($filter == 'coming' || !$filter){
            $liturgy = Liturgy::coming();
        }
        
        if($filter =='archived'){
            $liturgy = Liturgy::archived();
        }

        $liturgy = $liturgy->with('church');
        
        return Datatables::of($liturgy)

        ->editColumn('title' , function($liturgy){
            return view('backoffice.liturgies.datatable.title' , compact('liturgy'))->render();
        })
        ->editColumn('date' , function($liturgy){
            return view('backoffice.liturgies.datatable.date' , compact('liturgy'))->render();
        })
        ->editColumn('seats' , function($liturgy){
            return view('backoffice.liturgies.datatable.seats' , compact('liturgy'))->render();
        })
        ->addColumn('action' ,function($liturgy){
            $model = $liturgy;
            $modelName = 'liturgies';
            return view('backoffice.liturgies.datatable.actions' , compact('model' , 'modelName'))->render();
        })
        ->setRowId('id')
        ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];
        return view('backoffice.liturgies.create' , compact('pageConfigs'));
    }


    public function store(Request $request){
        $request->validate([
            'title'  => 'required',
            'date'   => 'required',
            'start'  => 'required',
            'end'    => 'required',
            'seats'  => 'required',
        ]);
        $church = Church::find($request->church);
        $liturgy = new Liturgy;
        $liturgy->title = $request->title;
        $liturgy->date  = $request->date;
        $liturgy->start = $request->start_submit;
        $liturgy->end   = $request->end_submit;
        $liturgy->seats   = $request->seats;
        $liturgy->church()->associate($church);
        
        $liturgy->save();
        toastr()->success('Data has been saved successfully!');
        return redirect(route('liturgies.index' , ['filter' => 'coming']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Liturgy $liturgy)
    {
       
        $id = $liturgy->id;
        $title = $liturgy->church->name .' - ' . $liturgy->title;

        try{
            if( $liturgy->delete() ){
                return Response::json([
                    'status'    => 'ok',
                    'deleted'   => $id,
                    'message'   => "$title Deleted Successfully"
                    ] , 200);
            }
        } catch (\Exception $e) {
            return abort("FAILED",500);
        }
    }

}
