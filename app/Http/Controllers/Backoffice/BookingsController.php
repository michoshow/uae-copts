<?php

namespace App\Http\Controllers\Backoffice;

use Response;
use App\Models\Booking;
use App\Models\Liturgy;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;


class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $filter = $request->filter;
        if($filter == 'archived'){
            $pageTitle = 'Archived Bookings';
        }else{
            $pageTitle = 'Coming Bookings';
        }
        if($request->has('liturgy')){
            $liturgy = Liturgy::find($request->liturgy);
            $pageTitle = 'Bookings - ' . $liturgy->details . ' ('  . date( 'l jS F Y', strtotime($liturgy->date)) . ')';
        }else{
            $liturgy = null;
        }
        
        return view('backoffice.bookings.index' , compact('pageTitle' , 'filter' , 'liturgy' ));
    }


    public function getBookingsData(Request $request)
    {
   

        if($request->filter == 'coming'){
            $booking = Booking::coming();
        }
        
        if($request->filter =='archived'){
            $booking = Booking::archived();
        }

        if($request->has('liturgy')){
            $booking->where('liturgy_id' , $request->liturgy);
        }
        $booking = $booking->with('liturgy' , 'member');

        return Datatables::of($booking)

        ->addColumn('member' , function($booking){
            $member = $booking->member;
          
            return view('backoffice.bookings.datatable.members' , compact('booking' , 'member'))->render();
        })
        ->addColumn('title' , function($booking){
            return view('backoffice.bookings.datatable.title' , compact('booking'))->render();
        })
        ->editColumn('date' , function($booking){
            return view('backoffice.bookings.datatable.date' , compact('booking'))->render();
        })
       
        ->addColumn('action' ,function($booking){
            $model = $booking;
            $modelName = 'bookings';
            return view('backoffice.defaults.datatable.actions' , compact('model' , 'modelName'))->render();
        })
        ->setRowId('id')
        ->make(true);
    }

    public function show(Booking $booking){

    }

}
