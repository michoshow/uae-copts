<?php

namespace App\Http\Controllers\Backoffice;

use App\Models\Church;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ChurchesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.churches.index');
    }


    public function getChurchesData()
    {
        $church = Church::get();

        return Datatables::of($church)

        ->editColumn('name' , function($church){
            return view('backoffice.churches.datatable.name' , compact('church'))->render();
        })
        ->addColumn('action' ,function($church){
            $model = $church;
            $modelName = 'churches';
            return view('backoffice.defaults.datatable.actions' , compact('model' , 'modelName'))->render();
        })
        ->setRowId('id')
        ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];
        return view('backoffice.churches.create' , compact('pageConfigs'));
    }

    public function store(Request $request){

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Church $church)
    {
        $pageConfigs = [
            'pageHeader' => false
        ];
        return view('backoffice.churches.edit' , compact('church' , 'pageConfigs') );
    }


    public function update(Request $request, Church $church)
    {
        $request->validate([
            'address'      => 'required|string',
            'emirate'     => 'required|string',
            'area'        => 'required|string'
        ]);
            
        $church->address    = $request->address;
        $church->emirate    = $request->emirate;
        $church->area       = $request->area;
        $church->contacts   = $request->contacts;
        if($church->save()){
            return redirect( route('churches.index'))->withSuccess('Updated Successfully');
        }else{
            return redirect()->back()->withErrors();
        }
    }

    
}
