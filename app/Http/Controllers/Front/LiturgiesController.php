<?php

namespace App\Http\Controllers\Front;

use App\Models\Liturgy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Session;

class LiturgiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $liturgies = Liturgy::isActive();

        if($request->has('filters')){
            $liturgies = $this->filter($liturgies , $request->filters);
        }else{
            
            if(Session::has('home.filters')){
                
                $liturgies = $this->filter($liturgies , session('home.filters'));
            }
        }
        
        $liturgies = $liturgies->orderBy('date' , 'asc')->with('church')->paginate();

        $template = $request->has('template')  ? $request->template : 'full';

        
        if($request->ajax()){
           
            $html = view('front.dashboard.liturgies-list-' . $template  , compact('liturgies'))->render(); 
            
            return Response::json([
                'status' => 'ok',
                'html'   => $html 
            ] , 200);
        }
        
        return view('front.dashboard.liturgies-list-' . $template , compact('liturgies'));
    }


    protected function filter($liturgies , $filters){
        $homeFilters = [];
            
        foreach($filters as $filter){
            
            if(isset($filter['value']) && !empty($filter['value'])){
                
                $homeFilters[$filter['filter']] = $filter['value'];
                
                $class = "App\\Filters\\Liturgies\\" . ucwords($filter['filter']);
                $liturgies = $class::filter($liturgies , $filter);
            }
        }

        Session::put('home.filters' , $filters);
        
        return $liturgies;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function filterLiturgies(Request $request){
        
    }
}
