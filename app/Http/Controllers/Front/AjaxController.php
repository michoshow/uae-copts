<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function excute(Request $request){
        if(!$request->action){
            return abort('No Action' , 500);
        }
        $params = explode("." , $request->action);
        if(count($params) == 2){
            $controller = 'App\\Http\\Controllers\\Front\\' . ucwords($params[0]). 'Controller';
            return app($controller)->{$params[1]}($request);
        }
    }
}
