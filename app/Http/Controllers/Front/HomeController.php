<?php

namespace App\Http\Controllers\Front;

use Auth;
use Session;
use App\Models\Church;
use App\Models\Liturgy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    function show()
    {
        $user = Auth::guard('user')->user()->load( [
            'family.members' ,
            'family.bookings' => function($q){
                $q->coming();
            },
        ]);
        
        $family = $user->family;
        $churches = Church::all();

        $filters = Session::has('home.filters') ? Session::get('home.filters') : [];
        return view('front.home' , compact('user' , 'churches' ,'family' , 'filters') );
    }
}
