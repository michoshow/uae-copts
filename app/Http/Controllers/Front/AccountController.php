<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;

class AccountController extends Controller
{
    public function show(Request $request){
      
        $family = Auth::guard('user')->user()->family->load('members');

        return view('front.account.show' , compact('family'));
        
    }

    public function changePassword(Request $request){
        
        $request->validate([
            'password' => 'required|min:6|confirmed'
        ]);

        if($user = Auth::guard('user')->user()){
            $user->password = Hash::make($request->password);
            if($user->save()){
                return redirect()->back()->withSuccess('Your password changed successfully!');
            }
        
        }

    }
}
