<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\UploadHelper;


class UploadController extends Controller
{
    public function upload(Request $request){
        
        if($file = $request->file('filepond')){
            $path = UploadHelper::upload($file);
            return $path;
        }
        if($files = $request->file('files')){
            $paths = [];
            foreach ($files as $file) {
                $paths[] = UploadHelper::upload($file);
            }
            return $paths;
        }
        return null;
    }


    public function destroy(Request $request){
        
    }
}
