<?php

namespace App\Http\Controllers\Front;

use Response;
use App\Models\User;
use App\Models\Church;
use App\Models\Family;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\Front\UniquePrimaryEmail;
use App\Rules\Front\UniquePrimaryPhone;
use App\Http\Requests\Front\CreateFamilyRequest;
use Auth;


class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd($request);

        $user = auth()->guard('user')->user();
       return view( 'front.family.create' , compact('user') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user, CreateFamilyRequest $request)
    {
       
        $validated = $request->validated();
        
        if(!$family = $user->family){
            
            $family = new Family();
            $family->address = $request->address;
            $family->emirate = $request->emirate;
            $family->area = $request->area;
            $family->phone = $request->phone;
            $family->email = $request->email;
            
            //$family->city = $request->city;
           // $family->landmarks = explode("," , $request->landmarks);
           
            $family->uid = random_int(9999,99999);

            $family->church()->associate($request->church);
            $family->user()->associate($user->id);
            $family->save();
            
        }
        return view('front.family.members.create-adult' , compact('family' , 'user'));

        //return redirect(route('show.user.family' , ['user' => $user , 'family' => $family]));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Family $family , Request $request)
    {
        $churches = Church::all();
        $family->load('members');
        return view('front.family.show' , compact('family' , 'user' , 'churches'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user,Family $family,Request $request)
    {

        if($family->user_id != Auth::guard('user')->user()->id){
            return abort('404');
        }
      

        $request->validate([
            'address'   => ['required'],
            //'area'      => ['required'],
            'emirate'   => ['required'],
            'church'    => ['required'],
            'phone'     => ['required', new UniquePrimaryPhone($family)],
            'email'     => ['required','email', new UniquePrimaryEmail($family)],
        ]);

        $family->address = $request->address;
        $family->emirate = $request->emirate;
        if($request->area){
            $family->area = $request->area;
        }
        $family->phone = $request->phone;
        $family->email = $request->email;
        $family->church()->associate($request->church);
        if($family->save()){
            return redirect()->back()->withSuccess('Updated Successfully!');
        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function uniquePhone(Request $request)
    {
        dd("sss");
        /* if($member = Member::where($request->attr , $request->value)->first()){
            return Response::json(false);
        } */
        return Response::json(false); 
    }
}
