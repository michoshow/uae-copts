<?php

namespace App\Http\Controllers\Front;


use Auth;
use QrCode;
use Session;
use Storage;
use Response;
use App\Events\Booked;
use App\Models\Member;
use App\Models\Booking;
use App\Models\Liturgy;
use Illuminate\Http\Request;
use App\Helpers\UploadHelper;
use App\Helpers\BookingHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Front\BookingRequest;

class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $family = Auth::guard('user')->user()->family;
        $filter = $request->filter;
            if($filter == 'archived'){
                $pageTitle = 'Archived Bookings';
                $bookings = $family->bookings()->archived()->get();
            }else{
                $pageTitle = 'Coming Bookings';
                $bookings = $family->bookings()->coming()->get();
            }
        return view('front.booking.index' , compact('pageTitle' , 'filter' , 'bookings' , 'family' ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Liturgy $liturgy , Request $request)
    {
        $family = Auth::guard('user')->user()->family->load('members' , 'members.bookings');
        $liturgy->load('church');
        return view('front.booking.create' , compact('liturgy' , 'family'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Liturgy $liturgy ,BookingRequest $request)
    {
        $validated = $request->validated();

        $token = md5(rand(1, 10) . microtime());
        $bookings   = [];
        $notAllowed = [];
        $family = Auth::guard('user')->user()->family;
        
        foreach ($request->bookers as $bookerId) {
            
            $member = Member::find($bookerId)->load('family');   
            
            if( BookingHelper::allowed($member , $liturgy) ){

                $booking = new Booking();
                $booking->date = $liturgy->date;
                $booking->start = $liturgy->start;
                $booking->end = $liturgy->end;
                $booking->token = $token;
            
                $booking->family()->associate($member->family);
                $booking->liturgy()->associate($liturgy);
                $booking->church()->associate($liturgy->church);
                $booking->member()->associate($member);
                $booking->save();
                $bookings[] = $booking;

            }else{
                $notAllowed[] = $member; 
            }
        }

        if(count($bookings)){
            
            // Upload The QR Code
            $qrCode = UploadHelper::uploadQRCode($liturgy , $token);
            
            Session::flash('success', 'Booked Successfully!');
            // Send Notification Emails
            event( new Booked($bookings , $token , $liturgy , $family , $qrCode ));

        }
        
        $family = Auth::guard('user')->user()->family->load('members' , 'members.bookings');
        $liturgy->load('church');
        
        if(count($bookings)){

            return  view('front.booking.booked' , compact( 'liturgy' , 'bookings' , 'family' , 'notAllowed' , 'token'));
            
        }else{

           return redirect(route('user.booking.show' , ['liturgy' => $liturgy , 'token' => $token]))->with();
        }
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Liturgy $liturgy , $token)
    {
        $family = Auth::guard('user')->user()->family->load('members' , 'members.bookings');
        $liturgy->load('church');

        $bookings = Booking::where([
            'token'     => $token,
            'family_id' => $family->id,
        ])->with('member')->get();

        if(!$bookings){
            return abort(404);
        }

        return  view('front.booking.show' , compact( 'liturgy' , 'bookings' , 'family' , 'token'));

    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showForPublic($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Liturgy $liturgy, Booking $booking ,Request $request)
    {
        if(Auth::guard('user')->user()->family->uid != $booking->family->uid ){
            return abort(419);
        }
        $token = $booking->token;

        $bookings = Booking::where('token' , $booking->token);
        $count = $bookings->count();

        if($request->has('member')){
            $bookings->where('member_id' , $request->member);
            $token = $request->member;
        }
        
        try{
            if( $bookings->delete() ){
                return Response::json([
                    'status'    => 'ok',
                    'deleted'   => $token,
                    'message'   => "Your booking deleted successfully!"
                    ] , 200);
            }
        } catch (\Exception $e) {
            return abort("FAILED",500);
        }
    }
}
