<?php

namespace App\Http\Controllers\Front;

use Auth;
use Response;
use App\Models\File;
use App\Models\Family;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Helpers\UploadHelper;
use App\Http\Controllers\Controller;


class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Family $family ,Request $request)
    {
        
        $user = Auth::guard('user')->user()->load('family.members');

        if(!$request->type){
            $type = 'adult';
            return view('front.family.members.create-' . $type , compact('family' , 'user' , 'type'));
        }
        $type = $request->type;
        return view('front.family.members.create-' . $type , compact('family' , 'user' , 'type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Family $family, Request $request)
    {

        
        $data = $request->except('files_list' , '_token' , 'uppyResult');
        $data['uid'] = random_int(9999,99999);

        if($request->file('profile_image')){
            $path = 'personal/' . $data['uid'] ;
            $profileImage = UploadHelper::upload($request->file('profile_image') , $path);
            $data['profile_image'] = env('FTP_ROOT_URL') . '/' . $profileImage;
        }
        
        
        $member = New Member();
        $member->fill($data);
        $member->family()->associate($family->uid);
        $member->church()->associate($request->church);
        if($member->save()){
            if( isset($request->files_list) ){
                $this->processFiles($request , $member);
            }
            return redirect(route('app.home'))->with('success', $member->first_name . ' Added Successfully!' );
        }
    }

    /**
     * Porcess a list of uploaded files
     */
    protected function processFiles($request , $member){
        $list = json_decode($request->files_list , true);
        $removed = [];        
        // Save Uploadd files in DB
        if(isset($list['removed'])){
            // Get the Removed files Paths
            $removed = array_column($list['removed'] , 'path');
        }
      
        if(count($list['added'])){
            foreach ($list['added'] as $file) {
                if(!in_array($file['path'] , $removed)){
                    $this->createFile($file , $member);
                }
            }
        }

        // Remove Deleted files from Storage
        if(count($list['removed'])){
            $this->deleteFile($removed);
        }
    }
    /**
     * Save a new File Entity in DB (the file was already uploaded)
     */
    protected function createFile($file , $member)
    {
        $tmp = explode("/" ,$file['path']);
        $newPath = 'personal/'.$member->uid.'/'.$tmp[count($tmp) - 1 ];
   
        $memberFile = new File();
        $memberFile->original_name = $file['originalName'];
        $memberFile->path = $newPath;
        $memberFile->member()->associate($member);
        $memberFile->save();
        
        UploadHelper::move( $file['path'] , 'personal/'. $member->uid , $newPath );

    }

    protected function deleteFile($files)
    {
        UploadHelper::deleteFiles( $files );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Family $family, Member $member, Request $request)
    {
        $user = Auth::guard('user')->user();
        $type = $request->type;
        if($user->family->id != $family->id){
            abort(404);
        }
        if($member->family->id != $user->family->id){
            abort(404);
        }

        return view('front.family.members.edit-' . $type , compact('family' , 'user' , 'type' , 'member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Family $family, Member $member, Request $request)
    {
        dd($request);
    }

    public function updateField(Member $member, Request $request){
        
        if(!$this->canEdit($member)){
            return abort(419);
        }
   

        $member->{$request->name} = $request->value;
        if($member->save()){
            return true;
        }

        return false;
        
    }


    protected function canEdit($member){
        if(Auth::guard('user')->user()->family->uid != $member->family->uid ){
            return false;
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family, Member $member, Request $request)
    {
        if(Auth::guard('user')->user()->family->uid != $member->family->uid ){
            return abort(419);
        }

        $id = $member->id;
        $name = $member->name;

        try{
            if( $member->delete() ){
                return Response::json([
                    'status'    => 'ok',
                    'deleted'   => $id,
                    'message'   => "$name Deleted Successfully"
                    ] , 200);
            }
        } catch (\Exception $e) {
            return abort("FAILED",500);
        }

    }


    public function isUniqueGlobally(Request $request){
        if($member = Member::where($request->attr , $request->value)->first()){
            return Response::json(false);
        }
        return Response::json(true); 
    }


    public function updateProfileImage(Request $request)
    {

        $request->validate([
            'profile_image' => 'required|image'
        ]);
        
        $member = Member::where('uid' , $request->member)->firstOrFail();
        if(Auth::guard('user')->user()->family->uid != $member->family->uid ){
            return abort(419);
        }   
        
        if($request->file('profile_image')){
            $path = 'personal/' . $request->member ;
            $profileImage = UploadHelper::upload($request->file('profile_image') , $path);
           $member->profile_image =  env('FTP_ROOT_URL') . '/' . $profileImage;
        }
        if($member->save()){
            return Response::json([
                'status'    => 'ok',
                'message'   => "Profile image updated Successfully"
            ] , 200);

        }

    }
}
