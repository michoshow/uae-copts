<?php

namespace App\Http\Requests\Front;

use App\Rules\Front\UniquePrimaryEmail;
use App\Rules\Front\UniquePrimaryPhone;
use Illuminate\Foundation\Http\FormRequest;

class CreateFamilyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'phone' => 'Primary phone',
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'   => ['required'],
            'area'      => ['required'],
            'emirate'   => ['required'],
            'church'    => ['required'],
            'email'     => ['required','email', new UniquePrimaryEmail(null)],
            'phone'     => ['required', new UniquePrimaryPhone(null)],
        ];
    }

    /**
 * Get the error messages for the defined validation rules.
 *
 * @return array
 */
public function messages()
{
    return [
        
    ];
}
}
