<?php
namespace App\Http\View\Composer;

use Illuminate\View\View;

class LiturgiesComposer{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $controller = 'App\\Http\\Controllers\\Front\\LiturgiesController';
        $liturgies = app($controller)->index(request());

        $view->with('liturgiesHTML', $liturgies );
    }

}