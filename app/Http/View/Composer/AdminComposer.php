<?php
namespace App\Http\View\Composer;

use Illuminate\View\View;

class AdminComposer{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('admin', auth()->guard('admin')->user());
    }

}