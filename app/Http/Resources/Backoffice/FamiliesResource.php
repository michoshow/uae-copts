<?php

namespace App\Http\Resources\Backoffice;

use Illuminate\Http\Resources\Json\JsonResource;

class FamiliesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                 'uid'      => $this->uid,
                 'email'    => $this->email,
                 'phone'    => $this->phone,
                 'members'  => $this->members->pluck('name')->implode(", "),
               ];
    }
}
