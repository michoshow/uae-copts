<?php

namespace App\Mail\Front;

use App\Models\Liturgy;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    public $bookings;
    public $token;
    public $liturgy;
    public $qrCode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bookings, $token, Liturgy $liturgy , $qrCode)
    {
        $this->bookings = $bookings;
        $this->token    = $token;
        $this->liturgy  = $liturgy;
        $this->qrCode  = $qrCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 
        'Booking Confirmation - ' . $this->liturgy->church->name . ' - ' . 
        date( 'l jS F Y', strtotime($this->liturgy->date)); 

        return $this
        ->subject($subject)
        ->view('front.booking.mail.confirmation');
    }
}
