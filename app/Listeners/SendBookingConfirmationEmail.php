<?php

namespace App\Listeners;

use App\Events\Booked;
use App\Mail\Front\BookingConfirmation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendBookingConfirmationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Booked  $event
     * @return void
     */
    public function handle(Booked $event)
    {
        $to = $event->family->email;
        Mail::to($to)->send(new BookingConfirmation($event->bookings, $event->token,$event->liturgy , $event->qrCode));
    }
}
