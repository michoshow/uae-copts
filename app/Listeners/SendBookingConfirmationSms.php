<?php

namespace App\Listeners;

use App\Events\Booked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendBookingConfirmationSms
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Booked  $event
     * @return void
     */
    public function handle(Booked $event)
    {
        //
    }
}
